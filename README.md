# DIVAID (DIVision of AtrIal Domains)

This bi-atrial division pipeline divides any bi-atrial geometry into clinically important segments according to predefined definitions.

<p float="left">
 <img src="images/example_anterior.png" width="400" />
 <img src="images/example_posterior.png" width="400" />
</p>



<!---
1. Standardization, 2. Ring extraction, 3. Division methods (top row: Laplace method, bottom row: Geometrical method), 4. Combined method, 5. Analysis of clinical data
-->

## 1. Installation

Clone repository

	git clone https://gitlab.kit.edu/kit/ibt-public/divaid.git

Create virtual environment for the main script of the pipeline

	cd divaid
	python3 -m venv venv_main
	source venv_main/bin/activate
	pip3 install -r req_main.txt
	deactivate

Install carputils and add path to carputils to venv_main. Alternatively, copy the carputils folder into venv_main/lib/python3.x/site-packages/ (https://git.opencarp.org/openCARP/carputils)

Create virtual environment for the remeshing procedure (PyMeshLab and PyMeshFix are only available for x86 and not for arm architecture)

* install Anaconda or Miniconda https://docs.anaconda.com/anaconda/install/index.html
(make sure to install the x86 (Intel) version, even on machines with arm architecture!)
* create conda environment with python 3.11

	```
	conda create -n venv_remeshing python=3.11
	```

* activate virtual environment

	```
	conda activate venv_remeshing
	```

* install requirements

	```
	cd divaid
	pip3 install -r req_remeshing.txt
	```

* deactivate conda environment

	```
	conda deactivate venv_remeshing
	```


Create virtual environment for the vein clipping procedure (for further information, see http://www.vmtk.org/download/)

* install Anaconda or Miniconda https://docs.anaconda.com/anaconda/install/index.html
(make sure to install the x86 (Intel) version, even on machines with arm architecture!)
* activate conda environment:

	```
	<path_to_anaconda>/anaconda3/bin/activate
	```

* create conda environment

	```	
	conda create -n venv_clipvein -c vmtk python=3.6 itk vtk vmtk
	```

* activate virtual environment

	```
	conda activate venv_clipvein
	```

* install matplotlib

	```
	conda install matplotlib==2.0.2
	```

* deactivate conda environment

	```
	conda deactivate venv_clipvein
	```

Activate environment for main script

	source venv_main/bin/activate
	cd src

Adapt the paths to your anaconda binary folder and to the virtual environments for remeshing and for the vein clipping procedure:
Run the script

	python3 pipeline_setup.py

Provide the following inputs when asked:
* `conda`: <path_to_conda>/bin/activate
* `name of remeshing venv`: <venv_remeshing>
* `name of clip vein venv`: <venv_clipvein>






## 2. Running the Pipeline
Example command:

	python3 main.py --mesh <path_to_mesh> --atrium <LA or RA> (e.g. python3 main.py --mesh ../data/LA --atrium LA)

<!--- --->
The first run may take exceptionally long to configure all environments!

Arguments:
* `--mesh`: path to .vtk file without file extension (polygonal data or unstructured grid with an open MV and/or TV orifice)
* `--atrium`: LA, RA or both depending on which atria the file contains
* `--start_step`: 1-4 allows to re-run the pipeline at any desired step if it has been run successfully until this step (e.g. to only re-run the division)
* `--subdivision`: applies subdivisions of large segments if set to 1

Seed placement:
* When the window with the atrium on a blue background appears, read the instructions in the console to place the seeds on the veins:
First, select valve (MV/TV) and press 'q', then select appendage (LAA/RAA) and additional veins (PVs/SVC/IVC/CS) and press 'q'.
* for the division, orifices of 4 (or 5) PVs, MV, TV, SVC, IVC and CS are required -> if these veins are closed, place seeds on the center of the tips of the veins to open them
* if the veins have merging branches or abrupt diameter changes along the veins, place the seed proximal of the merging branch/diameter change closest to the body


	
<!--- --->


<!---		-- no_LAA: set to 1 if no LAA is incorporated in the geometry (nevertheless, a seed for the LAA has to be guessed)
		-- no_RAA: set to 1 if no RAA is incorporated in the geometry (nevertheless, a seed for the RAA has to be guessed)
		-- no_clip: set to 1 if the veins have already been clipped
		-- clip_dist_LA: The veins will be re-appended at the end of the pipeline. The length of the veins can be specified with this parameter. Default is set to a 100mm. Therefore, the entire veins will be re-appended.
		-- clip_dist_RA: See clip_dist_LA
		-- skippoints: set to 0.01 if the veins are short, set to 0.1 or higher values, if segmentation faults occur during the vein clipping procedure
		-- no_analysis: set to 1 if the geometry does not contain any clinical data

--->

Conditions for automatic vein clipping are adapted from [1].







<!---

## 3. Running the Scripts as Standalone

### 3.1 Run Standardization Process:

Step 1: remeshing.py -> input file must be .vtk (can be polygonal data or unstructured grid)

	Example for standalone: python3 remeshing.py --mesh /Volumes/koala/Users/cg581/Documents/Atrial_segmentation_methods/Laplace/data/meshes/meanSSM/meanSSM --target_mesh_resolution 1 --clinical_data LGE-MRI


Step 2: clip_veins.py -> input file must be .vtk with the 4.2 data format as vtk 8.* can't read the 5.1 data format (inspired by Nunez-Garcia et al. [1])

	Change the environment to the one with the vmtk package

	Example for standalone: python2 clip_vein.py --mesh /Volumes/koala/Users/cg581/Documents/Atrial_segmentation_methods/Laplace/data/meshes/meanSSM/meanSSM --atrium both --skippoints 0.1






### 3.2 Run Ring Extraction:

Step 3: extract_rings.py -> input file must be .vtk (can be polygonal data or unstructured grid)

	Example for standalone: python3 extract_rings_both.py --mesh /Volumes/koala/Users/cg581/Documents/Atrial_segmentation_methods/Laplace/data/meshes/meanSSM/meanSSM --LAA 3877 --RAA 6931






### 3.3 Run Division:

Step 4.1: Laplace method: lap_main.py -> input file must be .vtk with polygonal data

	Example for standalone: python3 lap_main.py --mesh /Volumes/koala/Users/cg581/Documents/Atrial_segmentation_methods/Laplace/data/meshes/meanSSM/meanSSM --atrium both


Step 4.2: Geometrical method: geo_main.py -> input file must be .vtk with polygonal data

	python3 geo_main.py --mesh /Volumes/koala/Users/cg581/Documents/Atrial_segmentation_methods/Laplace/data/meshes/meanSSM/meanSSM --atrium both


Step 5: Combined method: can only be run in pipeline
	
	- computation and plot of the relative aereal portion of each region and the total surface area of the atrium
	- plot of the results of both division methods
	- user can change the division method for selected regions
	- if veins are cut during the process, they are re-appended

Step 6: Analysis of clinical data: can only be run in pipeline
	
	- visualization of healthy and diseased tissue, overlaid with region boundaries




## 4. Examples:

	- python3 main.py --../data/meanSSM --atrium both --skippoints 0.01 --no_analysis 1
	  (reduced skippoints factor since the vein length is very small, and no analysis since the shape model instances do not incorporate clinical data such as e.g. bipolar voltages)
	- python3 main.py --../data/Inst1 --atrium both --skippoints 0.01 --no_analysis 1

## 5. Troubleshooting
	
	- Segmentation fault during vein clipping procedure: Increase skippoints factor to 0.1 (or larger if needed)
	  For most cases, the default value of 0.04 leads to good results. However, for special cases such as short veins (reduced factor) or non-smooth surfaces, e.g. EAMs (increased factor), the skippoints factor needs to be adapted.
	- Veins are not cut properly: Try to place the seeds in the middle of the tubular structure at the tip of the veins (or proximal of the closest merging veins) OR cut the veins manually and set the flag no_clip to 1

## Clipping Parameters

![Clipping parameters](images/clipping_parameters.png)

--->


## Bibliography

[1] M. Nunez Garcia, G. Bernardino, F. Alarcon, et al., “Fast quasi-conformal regional flattening
of the left atrium,” IEEE Transactions on Visualization and Computer Graphics, vol. 26, pp.
2591–2602, 2020. doi:10.1109/tvcg.2020.2966702


## License

See [LICENSE](LICENSE)
