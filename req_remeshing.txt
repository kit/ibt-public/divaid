pymeshlab==2022.2.post4
pymeshfix==0.16.2
pyvista==0.42.3
vtk==9.2.6
argparse==1.4.0
scipy==1.11.3
numpy==1.26.0
pandas==2.1.1