vmtk==1.4.0
vtk==8.1.0
itk==4.13.0
h5py==2.8.0
numpy==1.11.3
matplotlib==2.0.2
cycler==0.11.0