#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Extract the feature edges from extracted endo surface
Then generate the rings

Input: extracted endo surface
Output: rings in vtk form; Ring points Id list
'''
import os
import numpy as np
from glob import glob
import pandas as pd
import vtk
from vtk.util import numpy_support
import scipy.spatial as spatial
from vtk.numpy_interface import dataset_adapter as dsa
import datetime
from sklearn.cluster import KMeans
import argparse
from scipy.spatial import cKDTree

vtk_version = vtk.vtkVersion.GetVTKSourceVersion().split()[-1].split('.')[0]

def parser():
    parser = argparse.ArgumentParser(description='Generate boundaries.')
    parser.add_argument('--mesh',
                        type=str,
                        default="",
                        help='path to mesh without extension')
    parser.add_argument('--LAA',
                        type=str,
                        default="",
                        help='LAA apex point index, leave empty if no LA')
    parser.add_argument('--RAA',
                        type=str,
                        default="",
                        help='RAA apex point index, leave empty if no RA')
    parser.add_argument('--LAA_base',
                        type=str,
                        default="",
                        help='LAA basis point index, leave empty if no LA')
    parser.add_argument('--RAA_base',
                        type=str,
                        default="",
                        help='RAA basis point index, leave empty if no RA')

    return parser.parse_args()

class Ring:
   def __init__(self, index, name, points_num, center_point, distance, polydata):
       self.id = index
       self.name = name
       self.np = points_num
       self.center = center_point
       self.ap_dist = distance
       self.vtk_polydata = polydata
      
def extract_rings(args):
    """Extracting Rings"""
    print('Extracting rings...')

    # define directories for input and outputs
    base_dir = f"{args.mesh}_division"
    extr_rings_dir = f"{base_dir}/stage3_extract_rings"
    outdir = extr_rings_dir
    # check if directory exists already
    if not os.path.exists(extr_rings_dir):
        # create directory
        os.makedirs(extr_rings_dir)

    # get filename of mesh
    filename = f"{base_dir}/stage2_clip_veins/{args.mesh.split('/')[-1]}_clipped.vtk"

    # read the mesh and check data format
    data_checker = vtk.vtkDataSetReader()
    data_checker.SetFileName(filename)
    data_checker.Update()

    if data_checker.IsFilePolyData():
        reader = vtk.vtkPolyDataReader()
    elif data_checker.IsFileUnstructuredGrid():
        reader = vtk.vtkUnstructuredGridReader()

    reader.SetFileName(filename)
    reader.Update()

    # extract surface from mesh
    geo_filter = vtk.vtkGeometryFilter()
    geo_filter.SetInputConnection(reader.GetOutputPort())
    geo_filter.Update()
    
    mesh_surf = geo_filter.GetOutput()
    
    # define dictionary to store xyz of centroids
    centroids = dict()

    
    # if both LAA and RAA ids are given
    if (args.LAA != "" and args.RAA != ""):

        # get point from the given ids and write on the centroids dictionary
        LA_ap_point = mesh_surf.GetPoint(int(args.LAA))
        RA_ap_point = mesh_surf.GetPoint(int(args.RAA))


        centroids["LAA"] = LA_ap_point
        centroids["RAA"] = RA_ap_point
        
        
        # get point from the given ids and write on the centroids dictionary
        if (args.LAA_base != "" and args.RAA_base != ""):
            LA_bs_point = mesh_surf.GetPoint(int(args.LAA_base))
            RA_bs_point = mesh_surf.GetPoint(int(args.RAA_base))

            centroids["LAA_base"] = LA_bs_point
            centroids["RAA_base"] = RA_bs_point
    
        # apply connectivity filter to surface to separate LA and RA
        connect = vtk.vtkConnectivityFilter()
        connect.SetInputConnection(geo_filter.GetOutputPort())
        connect.SetExtractionModeToAllRegions()
        connect.ColorRegionsOn()
        connect.Update()
        mesh_conn=connect.GetOutput()

        # get id data stored in "RegionId" array from connectivity filter and convert to numpy array (id_vec)
        mesh_conn.GetPointData().GetArray("RegionId").SetName("RegionID")
        id_vec = numpy_support.vtk_to_numpy(mesh_conn.GetPointData().GetArray("RegionID"))

        # double check ids as it can happen that the connectivity filter changes the ids
        loc = vtk.vtkPointLocator()
        loc.SetDataSet(mesh_conn)
        loc.BuildLocator()
        args.LAA = loc.FindClosestPoint(LA_ap_point)
        args.RAA = loc.FindClosestPoint(RA_ap_point)

        LA_tag = id_vec[int(args.LAA)]
        RA_tag = id_vec[int(args.RAA)]
        
        # extract LA from biatrial geometry
        thr = vtk.vtkThreshold()
        thr.SetInputData(mesh_conn)
        thr.SetLowerThreshold(LA_tag)
        thr.SetUpperThreshold(LA_tag)
        thr.Update()
        geo_filter = vtk.vtkGeometryFilter()
        geo_filter.SetInputConnection(thr.GetOutputPort())
        geo_filter.Update()
        
        # apply IdFilter in LA to generate ids
        idFilter = vtk.vtkIdFilter()
        idFilter.SetInputConnection(geo_filter.GetOutputPort())

        # IdFilter depends on vtk version
        if int(vtk_version) >= 9:
            idFilter.SetPointIdsArrayName('Ids')
            idFilter.SetCellIdsArrayName('Ids')
        else:
            idFilter.SetIdsArrayName('Ids')
        idFilter.Update()
        
        LA = idFilter.GetOutput()
    
        # Create LA geometry with Id data
        vtkWrite(LA, outdir+'/LA.vtk')
        
        loc = vtk.vtkPointLocator()
        loc.SetDataSet(LA)
        loc.BuildLocator()
        args.LAA = loc.FindClosestPoint(LA_ap_point)
        
        if args.LAA_base != "":
            loc = vtk.vtkPointLocator()
            loc.SetDataSet(LA)
            loc.BuildLocator()
            args.LAA_base = loc.FindClosestPoint(LA_bs_point)
        
        # Initialize boundary tag
        b_tag = np.zeros((LA.GetNumberOfPoints(),))

        # Identify rings and add ring points to boundary tag
        LA_rings = detect_and_mark_rings(LA, LA_ap_point)
        b_tag, centroids = mark_LA_rings(args, LA_rings, b_tag, centroids, outdir, LA)

        # Append marked LA ring points to geometry 
        dataSet = dsa.WrapDataObject(LA)
        dataSet.PointData.append(b_tag, 'boundary_tag')
        
        # Create LA with boundaries data
        vtkWrite(dataSet.VTKObject, outdir+'/LA_boundaries_tagged.vtk')

        # Extract RA from biatrial geometry
        thr.SetLowerThreshold(RA_tag)
        thr.SetUpperThreshold(RA_tag)
        thr.Update()
        geo_filter = vtk.vtkGeometryFilter()
        geo_filter.SetInputConnection(thr.GetOutputPort())
        geo_filter.Update()
        
        # IdFilter depends on vtk version 
        idFilter = vtk.vtkIdFilter()
        idFilter.SetInputConnection(geo_filter.GetOutputPort())
        if int(vtk_version) >= 9:
            idFilter.SetPointIdsArrayName('Ids')
            idFilter.SetCellIdsArrayName('Ids')
        else:
            idFilter.SetIdsArrayName('Ids')
        idFilter.Update()
        
        RA = idFilter.GetOutput()
        
        loc = vtk.vtkPointLocator()
        loc.SetDataSet(RA)
        loc.BuildLocator()
        args.RAA = loc.FindClosestPoint(RA_ap_point)
        
        if args.LAA_base != "":
            loc = vtk.vtkPointLocator()
            loc.SetDataSet(RA)
            loc.BuildLocator()
            args.RAA_base = loc.FindClosestPoint(RA_bs_point)
        
        # Create RA geometry with Id data
        vtkWrite(RA, outdir+'/RA.vtk')

        # Initialize boundary tag
        b_tag = np.zeros((RA.GetNumberOfPoints(),))

        # Identify rings and add ring points to boundary tag
        RA_rings = detect_and_mark_rings(RA, RA_ap_point)
        b_tag, centroids, RA_rings = mark_RA_rings(args, RA_rings, b_tag, centroids, outdir)
        cutting_plane_to_identify_tv_f_tv_s(RA, RA_rings, outdir)

        dataSet = dsa.WrapDataObject(RA)
        dataSet.PointData.append(b_tag, 'boundary_tag')
        
        # Create RA with boundaries data
        vtkWrite(dataSet.VTKObject, outdir+'/RA_boundaries_tagged.vtk')

        # Export PolyData of the MV, TV IVC and SVC
        vtkWrite([r.vtk_polydata for r in LA_rings if r.name == "MV"][0], outdir + '/MV.vtk')
        vtkWrite([r.vtk_polydata for r in LA_rings if r.name == "RSPV"][0], outdir + '/RSPV.vtk')
        vtkWrite([r.vtk_polydata for r in LA_rings if r.name == "RIPV"][0], outdir + '/RIPV.vtk')
        vtkWrite([r.vtk_polydata for r in LA_rings if r.name == "LSPV"][0], outdir + '/LSPV.vtk')
        vtkWrite([r.vtk_polydata for r in LA_rings if r.name == "LIPV"][0], outdir + '/LIPV.vtk')
        vtkWrite([r.vtk_polydata for r in RA_rings if r.name == "TV"][0], outdir + '/TV.vtk')
        vtkWrite([r.vtk_polydata for r in RA_rings if r.name == "SVC"][0], outdir + '/SVC.vtk')
        vtkWrite([r.vtk_polydata for r in RA_rings if r.name == "IVC"][0], outdir + '/IVC.vtk')
        vtkWrite([r.vtk_polydata for r in RA_rings if r.name == "CS"][0], outdir + '/CS.vtk')

    
    # Extract only LA rings
    elif args.RAA == "":
        LA_ap_point = mesh_surf.GetPoint(int(args.LAA))
        centroids["LAA"] = LA_ap_point
        idFilter = vtk.vtkIdFilter()
        idFilter.SetInputConnection(geo_filter.GetOutputPort())
        if int(vtk_version) >= 9:
            idFilter.SetPointIdsArrayName('Ids')
            idFilter.SetCellIdsArrayName('Ids')
        else:
            idFilter.SetIdsArrayName('Ids')
        idFilter.Update()
        LA = idFilter.GetOutput()
        vtkWrite(LA, outdir + '/LA.vtk')

        LA_rings = detect_and_mark_rings(LA, LA_ap_point)
        b_tag = np.zeros((LA.GetNumberOfPoints(),))
        b_tag, centroids = mark_LA_rings(args, LA_rings, b_tag, centroids, outdir, LA)

        dataSet = dsa.WrapDataObject(LA)
        dataSet.PointData.append(b_tag, 'boundary_tag')
        
        vtkWrite(dataSet.VTKObject, outdir+'/LA_boundaries_tagged.vtk')

        # Export PolyData of the MV, TV IVC and SVC
        vtkWrite([r.vtk_polydata for r in LA_rings if r.name == "MV"][0], outdir + '/MV.vtk')
        vtkWrite([r.vtk_polydata for r in LA_rings if r.name == "RSPV"][0], outdir + '/RSPV.vtk')
        vtkWrite([r.vtk_polydata for r in LA_rings if r.name == "RIPV"][0], outdir + '/RIPV.vtk')
        vtkWrite([r.vtk_polydata for r in LA_rings if r.name == "LSPV"][0], outdir + '/LSPV.vtk')
        vtkWrite([r.vtk_polydata for r in LA_rings if r.name == "LIPV"][0], outdir + '/LIPV.vtk')

    # Extract only RA rings
    elif args.LAA == "":
        RA_ap_point = mesh_surf.GetPoint(int(args.RAA))
        idFilter = vtk.vtkIdFilter()
        idFilter.SetInputConnection(geo_filter.GetOutputPort())
        if int(vtk_version) >= 9:
            idFilter.SetPointIdsArrayName('Ids')
            idFilter.SetCellIdsArrayName('Ids')
        else:
            idFilter.SetIdsArrayName('Ids')
        idFilter.Update()
        centroids["RAA"] = RA_ap_point
        RA = idFilter.GetOutput()
        vtkWrite(RA, outdir + '/RA.vtk')

        RA_rings = detect_and_mark_rings(RA, RA_ap_point)
        b_tag = np.zeros((RA.GetNumberOfPoints(),))
        b_tag, centroids, RA_rings = mark_RA_rings(args, RA_rings, b_tag, centroids, outdir)
        cutting_plane_to_identify_tv_f_tv_s(RA, RA_rings, outdir)

        dataSet = dsa.WrapDataObject(RA)
        dataSet.PointData.append(b_tag, 'boundary_tag')
        
        vtkWrite(dataSet.VTKObject, outdir+'/RA_boundaries_tagged.vtk')

        vtkWrite([r.vtk_polydata for r in RA_rings if r.name == "TV"][0], outdir + '/TV.vtk')
        vtkWrite([r.vtk_polydata for r in RA_rings if r.name == "SVC"][0], outdir + '/SVC.vtk')
        vtkWrite([r.vtk_polydata for r in RA_rings if r.name == "IVC"][0], outdir + '/IVC.vtk')
        vtkWrite([r.vtk_polydata for r in RA_rings if r.name == "CS"][0], outdir + '/CS.vtk')
    
    # Write centroids data frame as csv (if LA and RA are processed separately, get previously evaluated centers)
    if os.path.exists(outdir+"/rings_centroids.csv"):
        df1 = pd.read_csv(outdir+"/rings_centroids.csv")
        # check if columns already exist in df and delete them if yes
        for i in centroids:
            if i in df1.columns:
                df1 = df1.drop(i, axis=1)
        df2 = pd.DataFrame(centroids)
        df = df1.join(df2)
    else:
        df = pd.DataFrame(centroids)
    df.to_csv(outdir+"/rings_centroids.csv", float_format="%.2f", index=False)
    
    print("Extracting rings... Done!")
 
def detect_and_mark_rings(surf, ap_point):
    
    boundaryEdges = vtk.vtkFeatureEdges()
    boundaryEdges.SetInputData(surf)
    boundaryEdges.BoundaryEdgesOn()
    boundaryEdges.FeatureEdgesOff()
    boundaryEdges.ManifoldEdgesOff()
    boundaryEdges.NonManifoldEdgesOff()
    boundaryEdges.Update()
    
    "Splitting rings"
    
    connect = vtk.vtkConnectivityFilter()
    connect.SetInputData(boundaryEdges.GetOutput())
    connect.SetExtractionModeToAllRegions()
    connect.Update()
    num = connect.GetNumberOfExtractedRegions()
    
    connect.SetExtractionModeToSpecifiedRegions()
    
    rings = []
    
    for i in range(num):
        connect.AddSpecifiedRegion(i)
        connect.Update()
        surface = connect.GetOutput()

        # Clean unused points
        geo_filter = vtk.vtkGeometryFilter()
        geo_filter.SetInputData(surface)
        geo_filter.Update()
        surface = geo_filter.GetOutput()

        cln = vtk.vtkCleanPolyData()
        cln.SetInputData(surface)
        cln.Update()
        surface = cln.GetOutput()

        if num > 5 and cln.GetOutput().GetNumberOfPoints() < 25:
            continue
        
        ring_surf = vtk.vtkPolyData()
        ring_surf.DeepCopy(surface)
        
        centerOfMassFilter = vtk.vtkCenterOfMass()
        centerOfMassFilter.SetInputData(surface)
        centerOfMassFilter.SetUseScalarsAsWeights(False)
        centerOfMassFilter.Update()
        
        c_mass = centerOfMassFilter.GetCenter()
        
        ring = Ring(i,"", surface.GetNumberOfPoints(), c_mass, np.sqrt(np.sum((np.array(ap_point)- \
                    np.array(c_mass))**2, axis=0)), ring_surf)
    
        rings.append(ring)
        
        connect.DeleteSpecifiedRegion(i)
        connect.Update()
    
    return rings

def mark_LA_rings(args, rings, b_tag, centroids, outdir, LA):
    # get paths to directories
    base_dir = f"{args.mesh}_division"
    clip_veins_dir = f"{base_dir}/stage2_clip_veins"
    # if MV point exists (when used in pipeline), use the selected MV point for assignment (shortest distance)
    if os.path.exists(f"{clip_veins_dir}/LA/LA_mv_pt.txt"):
        MV_pt = np.loadtxt(f"{clip_veins_dir}/LA/LA_mv_pt.txt", skiprows=2, dtype=float)
        rings[np.argmin([np.sqrt(np.sum((np.array(r.center) - np.array(MV_pt))**2, axis=0)) for r in rings])].name = "MV"
    else:
        rings[np.argmax([r.np for r in rings])].name = "MV"
    pvs = [i for i in range(len(rings)) if rings[i].name!="MV"]
    
    estimator = KMeans(n_clusters=2)
    estimator.fit([r.center for r in rings if r.name!="MV"])
    label_pred = estimator.labels_

    min_ap_dist = np.argmin([r.ap_dist for r in [rings[i] for i in pvs]])
    label_LPV = label_pred[min_ap_dist]
    
    LPVs = [pvs[i] for i in np.where(label_pred == label_LPV)[0]]
    RPVs = [pvs[i] for i in np.where(label_pred != label_LPV)[0]]

    ''' not necessarily the smallest distance to the LAA
    LSPV_id = LPVs.index(pvs[min_ap_dist])
    '''
    
    
    #min_ap_dist_RPVs = np.argmin([r.ap_dist for r in [rings[i] for i in RPVs]])
    #RSPV_id = min_ap_dist_RPVs
    
    cutting_plane_to_identify_UAC(LPVs, RPVs, rings, LA, outdir)

    LSPV_id = cutting_plane_to_identify_SPV(LPVs, RPVs, rings, 'left')
    LSPV_id = LPVs.index(LSPV_id)

    RSPV_id = cutting_plane_to_identify_SPV(LPVs, RPVs, rings, 'right')
    RSPV_id = RPVs.index(RSPV_id)
    
    estimator = KMeans(n_clusters=2)
    estimator.fit([r.center for r in [rings[i] for i in LPVs]])
    LPV_lab = estimator.labels_
    LSPVs = [LPVs[i] for i in np.where(LPV_lab == LPV_lab[LSPV_id])[0]]
    LIPVs = [LPVs[i] for i in np.where(LPV_lab != LPV_lab[LSPV_id])[0]]
    
    estimator = KMeans(n_clusters=2)
    estimator.fit([r.center for r in [rings[i] for i in RPVs]])
    RPV_lab = estimator.labels_
    RSPVs = [RPVs[i] for i in np.where(RPV_lab == RPV_lab[RSPV_id])[0]]
    RIPVs = [RPVs[i] for i in np.where(RPV_lab != RPV_lab[RSPV_id])[0]]

    # check for a fifth PV
    RMPVs = []
    if len(RIPVs) > 1:
        RMPVs = list([RIPVs[np.argmax(np.linalg.norm(rings[RIPVs[i]].center - rings[RSPVs[i]].center) for i in range(len(RIPVs)))]])
        RIPVs = list([RIPVs[np.argmin(np.linalg.norm(rings[RIPVs[i]].center - rings[RSPVs[i]].center) for i in range(len(RIPVs)))]])
    
    LPV = []
    RPV = []
    
    for i in range(len(pvs)):
        if pvs[i] in LSPVs:
            rings[pvs[i]].name = "LSPV"
        elif pvs[i] in LIPVs:
            rings[pvs[i]].name = "LIPV"
        elif pvs[i] in RIPVs:
            rings[pvs[i]].name = "RIPV"
        elif pvs[i] in RSPVs:
            rings[pvs[i]].name = "RSPV"
        elif pvs[i] in RMPVs:
            rings[pvs[i]].name = "RMPV"
    
    for r in rings:
        id_vec = numpy_support.vtk_to_numpy(r.vtk_polydata.GetPointData().GetArray("Ids"))
        fname = outdir+'/ids_{}.vtx'.format(r.name)
        f = open(fname, 'w')
        f.write('{}\n'.format(len(id_vec)))
        f.write('extra\n')
        
        if r.name == "MV":
            b_tag[id_vec] = 1
        elif r.name == "LIPV":
            b_tag[id_vec] = 2
            LPV = LPV + list(id_vec)
        elif r.name == "LSPV":
            b_tag[id_vec] = 3
            LPV = LPV + list(id_vec)
        elif r.name == "RIPV":
            b_tag[id_vec] = 4
            RPV = RPV + list(id_vec)
        elif r.name == "RSPV":
            b_tag[id_vec] = 5
            RPV = RPV + list(id_vec)
            
        for i in id_vec:
            f.write('{}\n'.format(i))
        f.close()
        
        centroids[r.name] = r.center
     
    fname = outdir+'/ids_LAA.vtx'
    f = open(fname, 'w')
    f.write('{}\n'.format(1))
    f.write('extra\n')
    f.write('{}\n'.format(args.LAA))
    f.close()

    
    fname = outdir+'/ids_LPV.vtx'
    f = open(fname, 'w')
    f.write('{}\n'.format(len(LPV)))
    f.write('extra\n')
    for i in LPV:
        f.write('{}\n'.format(i))
    f.close()
    
    fname = outdir+'/ids_RPV.vtx'
    f = open(fname, 'w')
    f.write('{}\n'.format(len(RPV)))
    f.write('extra\n')
    for i in RPV:
        f.write('{}\n'.format(i))
    f.close()
    
    return b_tag, centroids

def mark_RA_rings(args, rings, b_tag, centroids, outdir):
    # get paths to directories
    base_dir = f"{args.mesh}_division"
    clip_veins_dir = f"{base_dir}/stage2_clip_veins"
    # if TV point exists (when used in pipeline), use the selected TV point for assignment (shortest distance)
    if os.path.exists(f"{clip_veins_dir}/RA/RA_tv_pt.txt"):
        TV_pt = np.loadtxt(f"{clip_veins_dir}/RA/RA_tv_pt.txt", skiprows=2, dtype=float)
        rings[np.argmin([np.sqrt(np.sum((np.array(r.center) - np.array(TV_pt)) ** 2, axis=0)) for r in rings])].name = "TV"
    # if TV point doesn't exist: assign largest dataset as TV or second largest dataset, if distance to RAA is smaller (don't confound IVC with TV)
    elif rings[np.argmax([r.np for r in rings])].ap_dist < rings[np.argsort([r.np for r in rings])[-2]].ap_dist:
        rings[np.argmax([r.np for r in rings])].name = "TV"
    else:
        rings[np.argsort([r.np for r in rings])[-2]].name = "TV"


    #rings[np.argmax([r.np for r in rings])].name = "TV"
    other = [i for i in range(len(rings)) if rings[i].name!="TV"]
    other_r = [rings[r] for r in other]


    # calculate smallest distances between all remaining orifices
    dist_oricfices = dict()
    for i in other:
        # calculate points of first dataset
        pts_dataset1 = vtk.util.numpy_support.vtk_to_numpy(rings[i].vtk_polydata.GetPoints().GetData())
        for j in other:
            if i != j:
                name = f"{min(i, j)}{max(i, j)}"
                if name not in list(dist_oricfices.keys()):
                    # calculate points of second dataset
                    pts_dataset2 = vtk.util.numpy_support.vtk_to_numpy(rings[j].vtk_polydata.GetPoints().GetData())
                    pt1, pt2 = closestid_between_datasets(pts_dataset1, pts_dataset2, input_type='pts')
                    dist_oricfices[name] = np.linalg.norm(pt1 - pt2)

    # assumption: there are 3 remaining orifices (SVC, IVC, CS)
    # if one orifice is far away from the others, use clustering, else: closest orifice to TV is CS
    if max(dist_oricfices.values()) > 2 * min(dist_oricfices.values()):
        # create cluster
        estimator = KMeans(n_clusters=2)
        estimator.fit([r.center for r in rings if r.name != "TV"])
        label_pred = estimator.labels_

        # 2 clusters: IVC and CS form cluster -> cluster which is equal to centroid is SVC
        dist = []
        for i in range(len(other)):
            # get minimal distance to cluster center
            dist.append(np.amin([np.linalg.norm(np.array(rings[other[i]].center) - estimator.cluster_centers_[j]) for j in range(2)]))
        label_SVC = label_pred[np.argmin(dist)]

        # IVC has more elements than CS
        SVC = other[np.where(label_pred == label_SVC)[0][0]]
        IVC_CS = [other[i] for i in np.where(label_pred != label_SVC)[0]]
        IVC_CS_r = [rings[r] for r in IVC_CS]
        IVC = IVC_CS[np.argmax([r.np for r in IVC_CS_r])]

    else:
        # orifice with smallest distance to TV is CS
        dist_tv = np.ones(len(other)) * np.inf
        # get points on TV orifice
        poly_tv = [r.vtk_polydata for r in rings if r.name == "TV"][0]
        pts_dataset1 = vtk.util.numpy_support.vtk_to_numpy(poly_tv.GetPoints().GetData())
        for i, ring_index in enumerate(other):
            # get points on orifice
            pts_dataset2 = vtk.util.numpy_support.vtk_to_numpy(rings[ring_index].vtk_polydata.GetPoints().GetData())
            # compute smallest distance between unassigned orifice and TV orifice
            pt1, pt2 = closestid_between_datasets(pts_dataset1, pts_dataset2, input_type="pts")
            # calculate distance
            dist_tv[i] = np.linalg.norm(pt1 - pt2)

        # orifice with smallest distance to TV orifice is CS
        CS = other[np.argmin(dist_tv)]

        # get remaining possible indices for SVC and IVC
        SVC_IVC = np.setdiff1d(np.array(other), CS)
        # get remaining rings
        SVC_IVC_r = [rings[r] for r in SVC_IVC]

        # SVC has smaller distance to RAA apex
        SVC = SVC_IVC[np.argmin([r.ap_dist for r in SVC_IVC_r])]

        # IVC is remaining ring
        IVC = np.setdiff1d(SVC_IVC, SVC)[0]
    
    rings[SVC].name = "SVC"
    rings[IVC].name = "IVC"
    if(len(other)>2):
        rings[list(set(other)-set([IVC,SVC]))[0]].name = "CS"
    
    for r in rings:
        id_vec = numpy_support.vtk_to_numpy(r.vtk_polydata.GetPointData().GetArray("Ids"))
        fname = outdir+'/ids_{}.vtx'.format(r.name)
        
        f = open(fname, 'w')
        f.write('{}\n'.format(len(id_vec)))
        f.write('extra\n')
        
        if r.name == "TV":
            b_tag[id_vec] = 6
        elif r.name == "SVC":
            b_tag[id_vec] = 7
        elif r.name == "IVC":
            b_tag[id_vec] = 8
        elif r.name == "CS":
            b_tag[id_vec] = 9
                
        for i in id_vec:
            f.write('{}\n'.format(i))
            
        f.close()
        
        centroids[r.name] = r.center
     
    fname = outdir+'/ids_RAA.vtx'
    f = open(fname, 'w')
    f.write('{}\n'.format(1))
    f.write('extra\n')
    f.write('{}\n'.format(args.RAA))
    f.close()
    
    return b_tag, centroids, rings

def vtkWrite(input_data, name):
    
    writer = vtk.vtkPolyDataWriter()
    writer.SetInputData(input_data)
    writer.SetFileName(name)
    writer.SetFileTypeToBinary()
    writer.Write()

def cutting_plane_to_identify_SPV(LPVs, RPVs, rings, side):
    LPVs_c = np.array([r.center for r in [rings[i] for i in LPVs]])
    lpv_mean = np.mean(LPVs_c, axis = 0)
    RPVs_c = np.array([r.center for r in [rings[i] for i in RPVs]])
    rpv_mean = np.mean(RPVs_c, axis = 0)
    mv_center = np.array([rings[i].center for i in range(len(rings)) if rings[i].name == "MV"][0])
    
    v1 = rpv_mean - mv_center
    v2 = lpv_mean - mv_center
    norm = np.cross(v1, v2)
    
    # # normalize vector
    norm = norm / np.linalg.norm(norm)

    plane = vtk.vtkPlane()
    plane.SetNormal(norm[0], norm[1], norm[2])
    plane.SetOrigin(mv_center[0], mv_center[1], mv_center[2])

    # check if LSPV or RSPV is to be identified
    # concatenate LPVs and RPVs to be able to index them
    PVs = list([LPVs, RPVs])
    if side == 'left':
        ind_lr = 0
    else:
        ind_lr = 1

    appendFilter = vtk.vtkAppendPolyData()
    for r in [rings[i] for i in PVs[ind_lr]]:
        tag_data = vtk.util.numpy_support.numpy_to_vtk(np.ones((r.np,))*r.id, deep=True, array_type=vtk.VTK_INT)
        tag_data.SetNumberOfComponents(1)
        tag_data.SetName("id")
        temp = vtk.vtkPolyData()
        temp.DeepCopy(r.vtk_polydata)
        temp.GetPointData().SetScalars(tag_data)
        appendFilter.AddInputData(temp)
    appendFilter.Update()
    
    meshExtractFilter = vtk.vtkExtractGeometry()
    meshExtractFilter.SetInputData(appendFilter.GetOutput())
    meshExtractFilter.SetImplicitFunction(plane)
    meshExtractFilter.Update()

    # count id tags as it may happen that some ids from both rings are in the dataset
    ids = vtk.util.numpy_support.vtk_to_numpy(meshExtractFilter.GetOutput().GetPointData().GetArray('id'))
    count0 = np.count_nonzero(ids == rings[PVs[ind_lr][0]].id)
    count1 = np.count_nonzero(ids == rings[PVs[ind_lr][1]].id)
    # in the case there is a 3rd PV
    if len(PVs[ind_lr]) > 2:
        # count id tags of 3rd PV
        count2 = np.count_nonzero(ids == rings[PVs[ind_lr][2]].id)
        counts = np.array([count0, count1, count2])
        # PV with most counts is assigned as SPV
        index = np.argmax(counts)
    else:
        counts = np.array([count0, count1])
        index = np.argmax(counts)
    SPV_id = int(PVs[ind_lr][index])

    
    return SPV_id

def cutting_plane_to_identify_UAC(LPVs, RPVs, rings, LA, outdir):
    LPVs_c = np.array([r.center for r in [rings[i] for i in LPVs]])
    lpv_mean = np.mean(LPVs_c, axis = 0)
    RPVs_c = np.array([r.center for r in [rings[i] for i in RPVs]])
    rpv_mean = np.mean(RPVs_c, axis = 0)
    mv_mean = rings[np.argmax([r.np for r in rings])].center
    
    v1 = rpv_mean - mv_mean
    v2 = lpv_mean - mv_mean
    norm = np.cross(v1, v2)
    
    # # normalize vector
    norm = norm / np.linalg.norm(norm)

    plane = vtk.vtkPlane()
    plane.SetNormal(norm[0], norm[1], norm[2])
    plane.SetOrigin(mv_mean[0], mv_mean[1], mv_mean[2])
    
    meshExtractFilter = vtk.vtkExtractGeometry()
    meshExtractFilter.SetInputData(LA)
    meshExtractFilter.SetImplicitFunction(plane)
    meshExtractFilter.Update()
    
    geo_filter = vtk.vtkGeometryFilter()
    geo_filter.SetInputData(meshExtractFilter.GetOutput())
    geo_filter.Update()
    surface = geo_filter.GetOutput()
    
    """
    here we will extract the feature edge 
    """
    boundaryEdges = vtk.vtkFeatureEdges()
    boundaryEdges.SetInputData(surface)
    boundaryEdges.BoundaryEdgesOn()
    boundaryEdges.FeatureEdgesOff()
    boundaryEdges.ManifoldEdgesOff()
    boundaryEdges.NonManifoldEdgesOff()
    boundaryEdges.Update()
    
    tree = cKDTree(vtk.util.numpy_support.vtk_to_numpy(boundaryEdges.GetOutput().GetPoints().GetData()))
    ids = vtk.util.numpy_support.vtk_to_numpy(boundaryEdges.GetOutput().GetPointData().GetArray('Ids'))
    MV_ring = [r for r in rings if r.name == "MV"]
    
    MV_ids = set(numpy_support.vtk_to_numpy(MV_ring[0].vtk_polydata.GetPointData().GetArray("Ids")))
    
    MV_ant = set(ids).intersection(MV_ids)
    MV_post = MV_ids - MV_ant
    
    fname = outdir+'/ids_MV_ant.vtx'
    f = open(fname, 'w')
    f.write('{}\n'.format(len(MV_ant)))
    f.write('extra\n')
    for i in MV_ant:
        f.write('{}\n'.format(i))
    f.close()
    
    fname = outdir+'/ids_MV_post.vtx'
    f = open(fname, 'w')
    f.write('{}\n'.format(len(MV_post)))
    f.write('extra\n')
    for i in MV_post:
        f.write('{}\n'.format(i))
    f.close()
    
    loc = vtk.vtkPointLocator()
    loc.SetDataSet(MV_ring[0].vtk_polydata)
    loc.BuildLocator()
    
    lpv_mv = loc.FindClosestPoint(lpv_mean)
    rpv_mv = loc.FindClosestPoint(rpv_mean)
    
    loc = vtk.vtkPointLocator()
    loc.SetDataSet(boundaryEdges.GetOutput())
    loc.BuildLocator()
    lpv_bb = loc.FindClosestPoint(lpv_mean)
    rpv_bb = loc.FindClosestPoint(rpv_mean)
    lpv_mv = loc.FindClosestPoint(MV_ring[0].vtk_polydata.GetPoint(lpv_mv))
    rpv_mv = loc.FindClosestPoint(MV_ring[0].vtk_polydata.GetPoint(rpv_mv))
    
    path = vtk.vtkDijkstraGraphGeodesicPath()
    path.SetInputData(boundaryEdges.GetOutput())
    path.SetStartVertex(lpv_bb)
    path.SetEndVertex(lpv_mv)
    path.Update()
    
    p = vtk.util.numpy_support.vtk_to_numpy(path.GetOutput().GetPoints().GetData())
    dd, ii = tree.query(p)
    mv_lpv = set(ids[ii])
    for r in rings:
        mv_lpv = mv_lpv - set(numpy_support.vtk_to_numpy(r.vtk_polydata.GetPointData().GetArray("Ids")))
    
    fname = outdir+'/ids_MV_LPV.vtx'
    f = open(fname, 'w')
    f.write('{}\n'.format(len(mv_lpv)))
    f.write('extra\n')
    for i in mv_lpv:
        f.write('{}\n'.format(i))
    f.close()
    
    path = vtk.vtkDijkstraGraphGeodesicPath()
    path.SetInputData(boundaryEdges.GetOutput())
    path.SetStartVertex(rpv_bb)
    path.SetEndVertex(rpv_mv)
    path.Update()
    
    p = vtk.util.numpy_support.vtk_to_numpy(path.GetOutput().GetPoints().GetData())
    dd, ii = tree.query(p)
    mv_rpv = set(ids[ii])
    for r in rings:
        mv_rpv = mv_rpv - set(numpy_support.vtk_to_numpy(r.vtk_polydata.GetPointData().GetArray("Ids")))
    
    fname = outdir+'/ids_MV_RPV.vtx'
    f = open(fname, 'w')
    f.write('{}\n'.format(len(mv_rpv)))
    f.write('extra\n')
    for i in mv_rpv:
        f.write('{}\n'.format(i))
    f.close()
    
    path = vtk.vtkDijkstraGraphGeodesicPath()
    path.SetInputData(boundaryEdges.GetOutput())
    path.SetStartVertex(lpv_bb)
    path.SetEndVertex(rpv_bb)
    path.Update()
    
    p = vtk.util.numpy_support.vtk_to_numpy(path.GetOutput().GetPoints().GetData())
    dd, ii = tree.query(p)
    rpv_lpv = set(ids[ii])
    for r in rings:
        rpv_lpv = rpv_lpv - set(numpy_support.vtk_to_numpy(r.vtk_polydata.GetPointData().GetArray("Ids")))
    
    fname = outdir+'/ids_RPV_LPV.vtx'
    f = open(fname, 'w')
    f.write('{}\n'.format(len(rpv_lpv)))
    f.write('extra\n')
    for i in rpv_lpv:
        f.write('{}\n'.format(i))
    f.close()
    

def cutting_plane_to_identify_tv_f_tv_s(model, rings, outdir):
    
    for r in rings:
        if r.name == "TV":
            tv_center = np.array(r.center)
            tv = r.vtk_polydata
        elif r.name == "SVC":
            svc_center = np.array(r.center)
            svc = r.vtk_polydata
        elif r.name == "IVC":
            ivc_center = np.array(r.center)
            ivc = r.vtk_polydata
            
    # calculate the norm vector
    v1 = tv_center - svc_center
    v2 = tv_center - ivc_center
    norm = np.cross(v1, v2)
    
    #normalize norm
    n = np.linalg.norm([norm], axis=1, keepdims=True)
    norm_1 = norm/n

    plane = vtk.vtkPlane()
    plane.SetNormal(norm_1[0][0], norm_1[0][1], norm_1[0][2])
    plane.SetOrigin(tv_center[0], tv_center[1], tv_center[2])
    
    geo_filter = vtk.vtkGeometryFilter()
    geo_filter.SetInputData(model)
    geo_filter.Update()
    surface = geo_filter.GetOutput()

    meshExtractFilter = vtk.vtkExtractGeometry()
    meshExtractFilter.SetInputData(surface)
    meshExtractFilter.SetImplicitFunction(plane)
    meshExtractFilter.Update()
    
    geo_filter = vtk.vtkGeometryFilter()
    geo_filter.SetInputData(meshExtractFilter.GetOutput())
    geo_filter.Update()
    surface = geo_filter.GetOutput()
    
    """
    here we will extract the feature edge 
    """
    boundaryEdges = vtk.vtkFeatureEdges()
    boundaryEdges.SetInputData(surface)
    boundaryEdges.BoundaryEdgesOn()
    boundaryEdges.FeatureEdgesOff()
    boundaryEdges.ManifoldEdgesOff()
    boundaryEdges.NonManifoldEdgesOff()
    boundaryEdges.Update()
    # points = boundaryEdges.GetOutput().GetPoints().GetData()
    
    gamma_top = boundaryEdges.GetOutput()

    """
    separate the tv into tv tv-f and tv-f
    """
    # calculate the norm vector
    v1 = svc_center - tv_center
    v2 = ivc_center - tv_center
    norm = np.cross(v2, v1)
    
    #normlize norm
    n = np.linalg.norm([norm], axis=1, keepdims=True)
    norm_1 = norm/n
    norm_2 = - norm_1

    plane = vtk.vtkPlane()
    plane.SetNormal(norm_1[0][0], norm_1[0][1], norm_1[0][2])
    plane.SetOrigin(tv_center[0], tv_center[1], tv_center[2])
    
    plane2 = vtk.vtkPlane()
    plane2.SetNormal(norm_2[0][0], norm_2[0][1], norm_2[0][2])
    plane2.SetOrigin(tv_center[0], tv_center[1], tv_center[2])
    
    meshExtractFilter = vtk.vtkExtractGeometry()
    meshExtractFilter.SetInputData(tv)
    meshExtractFilter.SetImplicitFunction(plane)
    meshExtractFilter.Update()
    
    meshExtractFilter2 = vtk.vtkExtractGeometry()
    meshExtractFilter2.SetInputData(tv)
    meshExtractFilter2.ExtractBoundaryCellsOn()
    meshExtractFilter2.SetImplicitFunction(plane2)
    meshExtractFilter2.Update()
    
    geo_filter = vtk.vtkGeometryFilter()
    geo_filter.SetInputData(meshExtractFilter.GetOutput())
    geo_filter.Update()
    tv_f = geo_filter.GetOutput()
    
    tv_f_ids = vtk.util.numpy_support.vtk_to_numpy(tv_f.GetPointData().GetArray("Ids"))
    fname = outdir+'/ids_TV_F.vtx'
    f = open(fname, 'w')
    f.write('{}\n'.format(len(tv_f_ids)))
    f.write('extra\n')
    for i in tv_f_ids:
        f.write('{}\n'.format(i))
    f.close()
    
    geo_filter2 = vtk.vtkGeometryFilter()
    geo_filter2.SetInputData(meshExtractFilter2.GetOutput())
    geo_filter2.Update()
    tv_s = geo_filter2.GetOutput()
    
    tv_s_ids = vtk.util.numpy_support.vtk_to_numpy(tv_s.GetPointData().GetArray("Ids"))
    fname = outdir+'/ids_TV_S.vtx'
    f = open(fname, 'w')
    f.write('{}\n'.format(len(tv_s_ids)))
    f.write('extra\n')
    for i in tv_s_ids:
        f.write('{}\n'.format(i))
    f.close()
    
    svc_points = svc.GetPoints().GetData()
    svc_points = vtk.util.numpy_support.vtk_to_numpy(svc_points)
    
    ivc_points = ivc.GetPoints().GetData()
    ivc_points = vtk.util.numpy_support.vtk_to_numpy(ivc_points)
    
    connect = vtk.vtkConnectivityFilter()
    connect.SetInputData(gamma_top)
    connect.SetExtractionModeToSpecifiedRegions()
    connect.Update()
    num = connect.GetNumberOfExtractedRegions()
    for i in range(num):
        connect.AddSpecifiedRegion(i)
        connect.Update()
        surface = connect.GetOutput()
        # Clean unused points
        cln = vtk.vtkCleanPolyData()
        cln.SetInputData(surface)
        cln.Update()
        surface = cln.GetOutput()
        points = surface.GetPoints().GetData()
        points = vtk.util.numpy_support.vtk_to_numpy(points)
        points = points.tolist()
    
        in_ivc = False
        in_svc = False
        # if there is point of group i in both svc and ivc then it is the "top_endo+epi" we need
        while in_ivc == False and in_svc == False:
            for var in points:
                if var in ivc_points:
                    in_ivc = True
                if var in svc_points:
                    in_svc = True
            if in_ivc and in_svc:
                top_endo_id = i
                break
            else:
                break
    
        # delete added region id
        connect.DeleteSpecifiedRegion(i)
        connect.Update()

    if 'top_endo_id' in locals():
        connect.AddSpecifiedRegion(top_endo_id)
        connect.Update()
        surface = connect.GetOutput()

        # Clean unused points
        cln = vtk.vtkCleanPolyData()
        cln.SetInputData(surface)
        cln.Update()

        top_cut = cln.GetOutput()

        pts_in_top = vtk.util.numpy_support.vtk_to_numpy(top_cut.GetPointData().GetArray("Ids"))
        pts_in_svc = vtk.util.numpy_support.vtk_to_numpy(svc.GetPointData().GetArray("Ids"))
        pts_in_ivc = vtk.util.numpy_support.vtk_to_numpy(ivc.GetPointData().GetArray("Ids"))

        to_delete = np.zeros((len(pts_in_top),), dtype=int)

        for i in range(len(pts_in_top)):
            if pts_in_top[i] in pts_in_svc or pts_in_top[i] in pts_in_ivc:
                to_delete[i] = 1

        meshNew = dsa.WrapDataObject(top_cut)
        meshNew.PointData.append(to_delete, "delete")

        thresh = vtk.vtkThreshold()
        thresh.SetInputData(meshNew.VTKObject)
        thresh.SetUpperThreshold(0)
        thresh.SetInputArrayToProcess(0, 0, 0, "vtkDataObject::FIELD_ASSOCIATION_POINTS", "delete")
        thresh.Update()

        geo_filter = vtk.vtkGeometryFilter()
        geo_filter.SetInputConnection(thresh.GetOutputPort())
        geo_filter.Update()

        mv_id = vtk.util.numpy_support.vtk_to_numpy(top_cut.GetPointData().GetArray("Ids"))[0]

        connect = vtk.vtkConnectivityFilter()
        connect.SetInputData(geo_filter.GetOutput())
        connect.SetExtractionModeToSpecifiedRegions()
        connect.Update()
        num = connect.GetNumberOfExtractedRegions()

        for i in range(num):
            connect.AddSpecifiedRegion(i)
            connect.Update()
            surface = connect.GetOutput()
            # Clean unused points
            cln = vtk.vtkCleanPolyData()
            cln.SetInputData(surface)
            cln.Update()
            surface = cln.GetOutput()

            pts_surf = vtk.util.numpy_support.vtk_to_numpy(surface.GetPointData().GetArray("Ids"))

            if mv_id not in pts_surf:
                found_id = i
                break

            # delete added region id
            connect.DeleteSpecifiedRegion(i)
            connect.Update()

        if 'found_id' in locals():
            connect.AddSpecifiedRegion(found_id)
            connect.Update()
            surface = connect.GetOutput()

            # Clean unused points
            cln = vtk.vtkCleanPolyData()
            cln.SetInputData(surface)
            cln.Update()

            top_endo = vtk.util.numpy_support.vtk_to_numpy(cln.GetOutput().GetPointData().GetArray("Ids"))
            fname = outdir+'/ids_TOP_ENDO.vtx'
            f = open(fname, 'w')
            f.write('{}\n'.format(len(top_endo)))
            f.write('extra\n')
            for i in top_endo:
                f.write('{}\n'.format(i))
            f.close()


def closestid_between_datasets(dataset1, dataset2, all_pts=[], input_type="ids"):
    if input_type == "ids":
        # compute coordinates of ids
        dataset1_pts = all_pts[dataset1]
        dataset2_pts = all_pts[dataset2]
    else:
        dataset1_pts = dataset1
        dataset2_pts = dataset2

    # compute distance matrix between dataset1 and dataset2
    dist_matr = np.linalg.norm(dataset1_pts[:, np.newaxis] - dataset2_pts, axis=-1)
    # get row and column index
    i, j = np.unravel_index(np.argmin(dist_matr), dist_matr.shape)

    if input_type == "ids":
        # convert row and column index to ids
        id_dataset1 = dataset1[i]
        id_dataset2 = dataset2[j]

        return id_dataset1, id_dataset2

    else:
        # get points
        pt_dataset1 = dataset1[i]
        pt_dataset2 = dataset2[j]

        return pt_dataset1, pt_dataset2


if __name__ == '__main__':
    args = parser()
    extract_rings(args)