import pymeshlab
import pymeshfix
import pyvista as pv
import vtk
import argparse
from scipy.spatial import cKDTree
from scipy.interpolate import LinearNDInterpolator
from scipy.interpolate import NearestNDInterpolator
from scipy.spatial import KDTree
from vtk.util import numpy_support
from vtk.numpy_interface import dataset_adapter as dsa
import os
import numpy as np
import pandas as pd
import shutil

vtk_version = vtk.vtkVersion.GetVTKSourceVersion().split()[-1].split('.')[0]

def parser():
    parser = argparse.ArgumentParser(description='Remeshing.')
    parser.add_argument('--mesh', type=str, default="", help='path to meshname')
    parser.add_argument('--meshfix', type=int, default=0, help='set to 1 to fix the mesh, 0 to only resample the mesh')
    parser.add_argument('--scale', type=int, default=1, help='normal unit is mm, set scaling factor if different')
    parser.add_argument('--size', type=float, default=10, help='patch radius in mesh units for curvature estimation')
    parser.add_argument('--target_mesh_resolution', type=float, default=1, help='target mesh resolution in mm')

    return parser.parse_args()



def remeshing(args):
    # get paths
    base_dir = f"{args.mesh}_division"
    remeshing_dir = f"{base_dir}/stage1_remeshing"

    # create new directory
    if not os.path.exists(remeshing_dir):
        os.makedirs(remeshing_dir)

    mesh_data = dict()

    if args.meshfix:
        reader = vtk.vtkPLYReader()
        reader.SetFileName('{}.ply'.format(args.mesh))
        reader.Update()

        boundaryEdges = vtk.vtkFeatureEdges()
        boundaryEdges.SetInputData(reader.GetOutput())
        boundaryEdges.BoundaryEdgesOn()
        boundaryEdges.FeatureEdgesOff()
        boundaryEdges.ManifoldEdgesOff()
        boundaryEdges.NonManifoldEdgesOff()
        boundaryEdges.Update()

        boundary_pts = vtk.util.numpy_support.vtk_to_numpy(boundaryEdges.GetOutput().GetPoints().GetData())

        # Clean the mesh from holes and self intersecting triangles
        meshin = pv.read('{}.ply'.format(args.mesh))
        meshfix = pymeshfix.MeshFix(meshin)
        meshfix.repair()
        vol = meshfix.mesh.volume

        pv.save_meshio('{}_meshfix.obj'.format(args.mesh), meshfix.mesh, "obj")

        reader = vtk.vtkOBJReader()
        reader.SetFileName('{}_meshfix.obj'.format(args.mesh))
        reader.Update()

        Mass = vtk.vtkMassProperties()
        Mass.SetInputData(reader.GetOutput())
        Mass.Update()

        print("Volume = ", Mass.GetVolume())
        print("Surface = ", Mass.GetSurfaceArea())

        bd_ids = find_elements_around_path_within_radius(reader.GetOutput(), boundary_pts, 1 * args.scale)

        tot_cells = set(list(range(reader.GetOutput().GetNumberOfCells())))
        cells_no_bd = tot_cells - bd_ids
        cell_ids_no_bd = vtk.vtkIdList()
        for i in cells_no_bd:
            cell_ids_no_bd.InsertNextId(i)
        extract = vtk.vtkExtractCells()
        extract.SetInputData(reader.GetOutput())
        extract.SetCellList(cell_ids_no_bd)
        extract.Update()

        geo_filter = vtk.vtkGeometryFilter()
        geo_filter.SetInputData(extract.GetOutput())
        geo_filter.Update()
        earth = geo_filter.GetOutput()

        cleaner = vtk.vtkCleanPolyData()
        cleaner.SetInputData(earth)
        cleaner.Update()

        connect = vtk.vtkConnectivityFilter()
        connect.SetInputConnection(cleaner.GetOutputPort())
        connect.SetExtractionModeToLargestRegion()
        connect.Update()

        cleaner = vtk.vtkCleanPolyData()
        cleaner.SetInputData(connect.GetOutput())
        cleaner.Update()

        writer = vtk.vtkOBJWriter()
        writer.SetInputData(cleaner.GetOutput())
        writer.SetFileName('{}_cleaned.obj'.format(args.mesh))
        writer.Write()

        mesh_data["vol"] = [vol]

        ms = pymeshlab.MeshSet()

        ms.load_new_mesh('{}_cleaned.obj'.format(args.mesh))

    else:
        # check data format of the mesh
        data_checker = vtk.vtkDataSetReader()
        data_checker.SetFileName('{}.vtk'.format(args.mesh))
        data_checker.Update()

        # choose reader to the corresponding file type
        if data_checker.IsFilePolyData():
            reader = vtk.vtkPolyDataReader()
        elif data_checker.IsFileUnstructuredGrid():
            reader = vtk.vtkUnstructuredGridReader()

        # read file
        reader.SetFileName(args.mesh + '.vtk')
        reader.Update()

        # get polydata
        geo_filter = vtk.vtkGeometryFilter()
        geo_filter.SetInputData(reader.GetOutput())
        #geo_filter.SetInputConnection(reader.GetOutputPort())
        geo_filter.Update()
        model = geo_filter.GetOutput()

        # convert it to an OBJ-file
        writer = vtk.vtkOBJWriter()
        writer.SetFileName(args.mesh + ".obj")
        writer.SetInputData(model)
        writer.Write()

        ms = pymeshlab.MeshSet()

        ms.load_new_mesh('{}.obj'.format(args.mesh))

        # delete file as it's no longer needed
        os.remove(args.mesh + ".obj")

    # compute the geometric measures of the current mesh
    # and save the results in the out_dict dictionary
    out_dict = ms.get_geometric_measures()

    # get the average edge length from the dictionary
    avg_edge_length = out_dict['avg_edge_length']

    tgt_edge_length = args.target_mesh_resolution * args.scale

    loc_tgt_edge_length = args.target_mesh_resolution * args.scale
    it = 1
    print("Current resolution: {} mm".format(avg_edge_length / args.scale))
    print("Target resolution: {} mm".format(tgt_edge_length / args.scale))
    while avg_edge_length > tgt_edge_length * 1.05 or avg_edge_length < tgt_edge_length * 0.95 or it < 3:

        a = pymeshlab.AbsoluteValue(loc_tgt_edge_length)

        ms.meshing_isotropic_explicit_remeshing(iterations=5, targetlen=a, adaptive=True)
        if it == 1 and args.meshfix:
            ms.laplacian_smooth()

        ms.apply_coord_hc_laplacian_smoothing()

        out_dict = ms.get_geometric_measures()

        avg_edge_length = out_dict['avg_edge_length']
        print("Current resolution: {} mm".format(avg_edge_length / args.scale))
        if avg_edge_length > tgt_edge_length * 1.05:
            loc_tgt_edge_length = tgt_edge_length * 0.95
            print("New target resolution: {} mm".format(loc_tgt_edge_length / args.scale))
        elif avg_edge_length < tgt_edge_length * 0.95:
            loc_tgt_edge_length = tgt_edge_length * 1.05
            print("New target resolution: {} mm".format(loc_tgt_edge_length / args.scale))
        it += 1

    mesh_data["surf"] = [out_dict['surface_area']]


    ## write data as .ply and afterwards read it to later save it as .vtk (42 version for clip_veins.py)
    ms.save_current_mesh('{}.ply'.format(args.mesh), save_vertex_color=False, save_vertex_normal=False, save_face_color=False, save_wedge_texcoord=False, save_wedge_normal=False)
    reader = vtk.vtkPLYReader()
    reader.SetFileName(args.mesh + '.ply')
    reader.Update()
    res_model = reader.GetOutput()

    # remove file as it's not longer needed
    os.remove(f"{args.mesh}.ply")


    ## if clinical data was acquired, the scalar values are transfered to the remeshed model with a nearest neighbor search and linear interpolation
    # get points of the original model
    pts = vtk.util.numpy_support.vtk_to_numpy(model.GetPoints().GetData())
    # get points of the remeshed model
    res_pts = vtk.util.numpy_support.vtk_to_numpy(res_model.GetPoints().GetData())

    ## get data arrays
    # get number of data arrays
    n_dat_arr = model.GetPointData().GetNumberOfArrays()
    # get data
    dat_arr = dict()
    for i in range(n_dat_arr):
        dat_arr[model.GetPointData().GetArrayName(i)] = vtk.util.numpy_support.vtk_to_numpy(model.GetPointData().GetArray(i))

    ## initialize empty array for remeshed model
    res_dat_arr = dict()
    for i in range(n_dat_arr):
        shape = np.shape(vtk.util.numpy_support.vtk_to_numpy(model.GetPointData().GetArray(i)))
        if len(shape) != 1:
            res_dat_arr[model.GetPointData().GetArrayName(i)] = np.zeros((len(res_pts), np.shape(vtk.util.numpy_support.vtk_to_numpy(model.GetPointData().GetArray(i)))[1]))
        else:
            res_dat_arr[model.GetPointData().GetArrayName(i)] = np.zeros(len(res_pts))


    ## if the original model has more points than the remeshed model: get the nearest neighbor of any point of the remeshed model (in the original model)
    ## if the original model has less points than the remeshed model: get the nearest neighbor af any point of the original model (in the remeshed model), transfer these values to the remeshed model and perform a linear interpolation for the remaining points
    if len(pts) > len(res_pts):
        # build a KDTree
        tree = KDTree(pts)
        # get the nearest neighbor of each point of the remeshed model in the original model
        dist, ind_i = tree.query(res_pts)

        # transfer data
        for i in range(n_dat_arr):
            res_dat_arr[model.GetPointData().GetArrayName(i)] = dat_arr[model.GetPointData().GetArrayName(i)] [ind_i]


    else:
        # build a KDTree
        tree = KDTree(res_pts)
        # get the closest point of each node of the original model in the remeshed model
        dist, ind_i = tree.query(pts)

        # transfer data and initialize interpolation
        res_ids = np.arange(0, len(res_pts))
        res_ids_unass = np.setdiff1d(res_ids, ind_i)


        for i in range(n_dat_arr):
            temp = dat_arr[model.GetPointData().GetArrayName(i)]

            # transfer data of nearest neighbors to remeshed model
            res_dat_arr[model.GetPointData().GetArrayName(i)][ind_i] = temp

            # get remaining values of the remeshed model by linear interpolation
            interp = LinearNDInterpolator(list(zip(res_pts[ind_i, 0], res_pts[ind_i, 1], res_pts[ind_i, 2])), temp)
            res_dat_arr[model.GetPointData().GetArrayName(i)] [res_ids_unass] = interp(res_pts[res_ids_unass])

            # if points are outside of the convex hull, use the nearest neighbor in the original model as scalar value
            nans = np.argwhere(np.isnan(res_dat_arr[model.GetPointData().GetArrayName(i)]))
            tree = KDTree(pts)
            dist, ind_j = tree.query(res_pts[nans])
            res_dat_arr[model.GetPointData().GetArrayName(i)] [nans] = dat_arr[model.GetPointData().GetArrayName(i)] [ind_j]


    # append data to remeshed vtk file
    meshNew = dsa.WrapDataObject(res_model)
    for i in range(n_dat_arr):
        meshNew.PointData.append(res_dat_arr[model.GetPointData().GetArrayName(i)], model.GetPointData().GetArrayName(i))
    res_model = meshNew.VTKObject

    # write the remeshed model (with the interpolated arrays)
    writer = vtk.vtkPolyDataWriter()
    writer.SetFileName(f"{remeshing_dir}/{args.mesh.split('/')[-1]}_remeshed.vtk")
    writer.SetInputData(res_model)
    writer.SetFileVersion(42)
    writer.Write()

    return


def find_elements_around_path_within_radius(mesh, points_data, radius):
    locator = vtk.vtkStaticPointLocator()
    locator.SetDataSet(mesh)
    locator.BuildLocator()

    mesh_id_list = vtk.vtkIdList()
    for i in range(len(points_data)):
        temp_result = vtk.vtkIdList()
        locator.FindPointsWithinRadius(radius, points_data[i], temp_result)
        for j in range(temp_result.GetNumberOfIds()):
            mesh_id_list.InsertNextId(temp_result.GetId(j))

    mesh_cell_id_list = vtk.vtkIdList()
    mesh_cell_temp_id_list = vtk.vtkIdList()
    for i in range(mesh_id_list.GetNumberOfIds()):
        mesh.GetPointCells(mesh_id_list.GetId(i), mesh_cell_temp_id_list)
        for j in range(mesh_cell_temp_id_list.GetNumberOfIds()):
            mesh_cell_id_list.InsertNextId(mesh_cell_temp_id_list.GetId(j))

    id_set = set()
    for i in range(mesh_cell_id_list.GetNumberOfIds()):
        id_set.add(mesh_cell_id_list.GetId(i))

    return id_set


if __name__ == '__main__':
    args = parser()
    remeshing(args)