import os
import argparse
import subprocess
import numpy as np
from stage21_review_clip_veins import review_clip_veins_main
from stage3_extract_rings import extract_rings
from stage4_division import run_division

def parser():
    parser = argparse.ArgumentParser(description='Atrial Division Pipeline')

    # overall arguments
    parser.add_argument('--atrium',
                        type=str,
                        choices=["LA", "RA", "biatrial"],
                        default="both",
                        help='indicate if the geometry is mono- or biatrial')

    # parameters required for stage1_remeshing.py
    parser.add_argument('--mesh',
                        type=str,
                        default="",
                        help="full path to mesh without extension")
    parser.add_argument('--target_mesh_resolution',
                        type=float,
                        default=1,
                        help='define target mesh resolution')
    parser.add_argument('--meshfix',
                        type=int,
                        default=0,
                        choices=[0, 1],
                        help='set to 1 to fix the mesh, 0 to only resample the mesh')
    parser.add_argument('--scale',
                        type=int,
                        default=1,
                        help='normal unit is mm, set scaling factor if different')

    # parameters required for stage4_division.py
    parser.add_argument('--subdivision',
                        type=int,
                        default=0,
                        choices=[0, 1],
                        help='set to 0 for no subdivisions, otherwise set to 1')

    # parameter to specify where to start the pipeline
    parser.add_argument('--start_step',
                        type=int,
                        default=1,
                        choices=[1, 2, 3, 4],
                        help='specify at which stage to start the pipeline')

    return parser.parse_args()


def run_main(args):

    # load file with information to setup venvs
    if os.path.exists("../venvs_paths.txt"):
        venvs_cmds = np.loadtxt("../venvs_paths.txt", dtype=str, delimiter=":")
    else:
        print("Please run 'pipeline_setup.py' first to setup the pipeline with your environment paths.")
        exit()

    ##########################
    # Stage 1: Remeshing
    ##########################
    print("\nStep 1: Remeshing\n")

    if args.start_step < 2:
        # another environment needs to be activated as PyMeshlab (for remeshing) and VMTK (which is essential for clipping
        # the veins) need older versions of numpy and VTK (and only run on x86-machines)
        conda_act_remeshing = venvs_cmds[np.where(venvs_cmds[:, 0] == "conda_act_remeshing")[0][0], 1][1:]
        scr_remeshing = f"python3 stage1_remeshing.py --mesh {args.mesh} --meshfix {args.meshfix} --scale {args.scale} " \
                        f"--target_mesh_resolution {args.target_mesh_resolution} "
        conda_deact_remeshing = venvs_cmds[np.where(venvs_cmds[:, 0] == "conda_deact_remeshing")[0][0], 1][1:]
        cmd_remeshing = conda_act_remeshing + scr_remeshing + conda_deact_remeshing


        # configure terminal from PyCharm
        os.environ["PATH"] = f"{venvs_cmds[np.where(venvs_cmds[:, 0] == 'os_environ')[0][0], 1][1:]}:" \
                             f"{os.environ['PATH']}"
        subprocess.run(cmd_remeshing, shell=True, check=True)


    ############################
    # Stage 2: Clip veins
    ############################
    print("\nStep 2: Clip veins\n")

    if args.start_step < 3:
        # another environment needs to be activated as PyMeshlab (for remeshing) and VMTK (which is essential for clipping
        # the veins) need older versions of numpy and VTK (and only run on Intel-Machines)
        # create a shell command with the activation of the environment and the command to run the script
        conda_act_clip_vein = venvs_cmds[np.where(venvs_cmds[:, 0] == "conda_act_clip_vein")[0][0], 1][1:]
        scr_clip_vein = f"python3 stage2_clip_veins.py --mesh {args.mesh} --atrium {args.atrium} "
        conda_deact_clip_vein = venvs_cmds[np.where(venvs_cmds[:, 0] == "conda_deact_clip_vein")[0][0], 1][1:]
        cmd_clip_vein = conda_act_clip_vein + scr_clip_vein + conda_deact_clip_vein

        # configure terminal from PyCharm
        os.environ["PATH"] = f"{venvs_cmds[np.where(venvs_cmds[:, 0] == 'os_environ')[0][1], 1][1:]}:" \
                             f"{os.environ['PATH']}"
        subprocess.run(cmd_clip_vein, shell=True, check=True)

        # visualize and edit vein clipping
        if args.atrium == "LA" or args.atrium == "biatrial":
            review_clip_veins_main(args, "LA")
        if args.atrium == "RA" or args.atrium == "biatrial":
            review_clip_veins_main(args, "RA")


    ##############################
    # Stage 3: Extract rings
    ##############################
    print("\nStage 3: Extract rings")

    if args.start_step < 4:
        # add arguments for LAA and RAA id
        args.LAA = ""
        args.RAA = ""
        if args.atrium == "LA" or args.atrium == "biatrial":
            args.LAA = load_laa_raa_id(args, "LAA")
        if args.atrium == "RA" or args.atrium == "biatrial":
            args.RAA = load_laa_raa_id(args, "RAA")

        extract_rings(args)


    ########################
    # Stage 4: Division
    ########################
    print("\nStage 4: Divide atria")

    if args.start_step < 5:
        run_division(args)


def load_laa_raa_id(args, appendage):
    # get atrium
    atrium = appendage[:-1]
    # get path to base directory
    base_dir = f"{args.mesh}_division"
    # get path to clip veins dir
    clip_veins_dir = f"{base_dir}/stage2_clip_veins/{atrium}/"
    # load id of appendage
    id_app = int(np.loadtxt(f"{clip_veins_dir}/{atrium}_id_{appendage}.txt", skiprows=2))

    return id_app


if __name__ == '__main__':
    args = parser()
    run_main(args)