import os
import shutil
import argparse
import vtk
import numpy as np
from vmtk import vmtkscripts
from vmtk import vmtkrenderer
from vtk.util.numpy_support import vtk_to_numpy
from vtk.numpy_interface import dataset_adapter as dsa

def parser():
    parser = argparse.ArgumentParser(description="Clip_veins")
    parser.add_argument('--mesh',
                        type=str,
                        default="",
                        help='path to meshname')
    parser.add_argument('--atrium',
                        type=str,
                        choices=["LA", "RA", "biatrial"],
                        default="biatrial",
                        help='indicate if the geometry is mono- or biatrial')
    parser.add_argument('--clspacing',
                        type=float,
                        default=0.4,
                        help='specify the distance between 2 consecutive points on the centerline')
    parser.add_argument('--skippoints',
                        type=float,
                        default=0.04,
                        help='factor of skipped points at distal end of a vein')
    parser.add_argument('--maxslope_dia',
                        type=float,
                        default=5,
                        help='threshold for the slope of the diameter change between 2 consecutive points')
    parser.add_argument('--highslope',
                        type=float,
                        default=1.2,
                        help='threshold to extract when the vein has an increasing diameter while coming closer to the body')

    return parser.parse_args()


def clip_veins_main(args):
    # get paths
    base_dir = f"{args.mesh}_division"
    clip_veins_dir = f"{base_dir}/stage2_clip_veins"

    # check if directory exists
    if not os.path.exists(f"{clip_veins_dir}"):
        # create new directory
        os.makedirs(f"{clip_veins_dir}")

    # read input file
    # check vtk format
    data_checker = vtk.vtkDataSetReader()
    data_checker.SetFileName(f"{base_dir}/stage1_remeshing/{args.mesh.split('/')[-1]}_remeshed.vtk")
    data_checker.Update()

    if data_checker.IsFilePolyData():
        reader = vtk.vtkPolyDataReader()
    elif data_checker.IsFileUnstructuredGrid():
        reader = vtk.vtkUnstructuredGridReader()

    reader.SetFileName(f"{base_dir}/stage1_remeshing/{args.mesh.split('/')[-1]}_remeshed.vtk")
    reader.Update()

    # generate ids of the model
    idFilter = vtk.vtkIdFilter()
    idFilter.SetInputConnection(reader.GetOutputPort())
    # IdFilter depends on vtk version
    if vtk.vtkVersion.GetVTKMajorVersion() >= 9:
        idFilter.SetPointIdsArrayName('clip_Ids')
    else:
        idFilter.SetIdsArrayName('clip_Ids')
    idFilter.Update()
    mesh = idFilter.GetOutput()

    # write mesh with ids
    write_vtk(mesh, f"{clip_veins_dir}/{args.mesh.split('/')[-1]}_clip_ids.vtk")

    # separate clipping for LA and RA
    if args.atrium == "LA" or args.atrium == "biatrial":
        # check if directory exists
        if os.path.exists(f"{clip_veins_dir}/LA"):
            # remove directory
            shutil.rmtree(f"{clip_veins_dir}/LA")

        # create new directory
        os.makedirs(f"{clip_veins_dir}/LA")

        # place seed points on the LA
        print("\nClipping LA:\n")
        print("Please place seeds in the following order:"
              "\nStart of centerline: MV (then press 'q')"
              "\nEnd of centerline: LAA and PVs if desired (then press 'q')\n")

        # get seeds and centerlines
        pts_seeds, pt_mv, poly_centerlines = get_seeds_centerlines(mesh)

        # prepare centerlines and get sections
        poly_centerlines_clipped, poly_centerlines_sections = prepare_centerlines(args, poly_centerlines, mesh)

        # get ids from clippoints with condition
        ids_clippoints = get_clippoints(args, poly_centerlines_sections)

        # save seed points, centerlines and clippoints
        for key in list(poly_centerlines_clipped.keys()):
            write_ids(pts_seeds[key], f"{clip_veins_dir}/LA/LA_seed_pt_{key}.txt")
            write_vtk(poly_centerlines_clipped[key], f"{clip_veins_dir}/LA/LA_centerline_{key}.vtk")
            write_ids(ids_clippoints[key], f"{clip_veins_dir}/LA/LA_clippoint_id_{key}.txt")

        # save mv center
        write_ids(pt_mv, f"{clip_veins_dir}/LA/LA_mv_pt.txt")


    if args.atrium == "RA" or args.atrium == "biatrial":
        # check if directory exists
        if os.path.exists(f"{clip_veins_dir}/RA"):
            # remove directory
            shutil.rmtree(f"{clip_veins_dir}/RA")

        # create new directory
        os.makedirs(f"{clip_veins_dir}/RA")

        # place seed points on the LA
        print("\nClipping RA:\n")
        print("Please place seeds in the following order:"
              "\nStart of centerline: TV (then press 'q')"
              "\nEnd of centerline: RAA and additional veins if desired (then press 'q')\n")

        # get seeds and centerlines
        pts_seeds, pt_tv, poly_centerlines = get_seeds_centerlines(mesh)

        # prepare centerlines and get sections
        poly_centerlines_clipped, poly_centerlines_sections = prepare_centerlines(args, poly_centerlines, mesh)

        # get ids from clippoints with condition
        ids_clippoints = get_clippoints(args, poly_centerlines_sections)

        # save centerlines and clippoints
        for key in list(poly_centerlines_clipped.keys()):
            write_ids(pts_seeds[key], f"{clip_veins_dir}/RA/RA_seed_pt_{key}.txt")
            write_vtk(poly_centerlines_clipped[key], f"{clip_veins_dir}/RA/RA_centerline_{key}.vtk")
            write_ids(ids_clippoints[key], f"{clip_veins_dir}/RA/RA_clippoint_id_{key}.txt")

        # save tv center
        write_ids(pt_tv, f"{clip_veins_dir}/RA/RA_tv_pt.txt")



def get_seeds_centerlines(mesh):
    clines = vmtkscripts.vmtkCenterlines()
    clines.Surface = mesh
    clines.SeedSelectorName = 'pickpoint'
    clines.Execute()

    # extract the selected seed points on the veins (targets)
    pts_seeds = vtk.util.numpy_support.vtk_to_numpy(clines.SeedSelector.PickedSeeds.GetPoints().GetData())
    # extract the selected seed points on the MV / TV (source)
    id_valve = clines.SeedSelector._SourceSeedIds.GetId(0)
    pt_valve = clines.SeedSelector._Surface.GetPoint(id_valve)

    # get polydata of centerlines
    poly_centerlines = clines.Centerlines

    # extract branches of centerlines
    poly_centerlines_branches = extract_branches(poly_centerlines)

    return pts_seeds, pt_valve, poly_centerlines_branches

def extract_branches(centerlines):
    # extract branches
    extractor = vmtkscripts.vmtkBranchExtractor()
    extractor.Centerlines = centerlines
    extractor.RadiusArrayName = 'MaximumInscribedSphereRadius'
    extractor.Execute()

    # get number of branches
    n_branches = np.max(vtk.util.numpy_support.vtk_to_numpy(extractor.Centerlines.GetCellData().GetArray("CenterlineIds"))) + 1

    # separate branches by id
    branches = dict()
    for i in range(0, n_branches):
        thresh = vtk.vtkThreshold()
        thresh.SetInputData(extractor.Centerlines)
        thresh.SetInputArrayToProcess(0, 0, 0, vtk.vtkDataObject.FIELD_ASSOCIATION_CELLS, 'CenterlineIds')
        if (vtk.vtkVersion.GetVTKMajorVersion() >= 9):
            thresh.SetLowerThreshold(i)
            thresh.SetUpperThreshold(i)
        else:
            thresh.ThresholdBetween(i, i)
        thresh.Update()
        geo_filter = vtk.vtkGeometryFilter()
        geo_filter.SetInputConnection(thresh.GetOutputPort())
        geo_filter.Update()

        branches[i] = geo_filter.GetOutput()

    return branches

def prepare_centerlines(args, poly_centerlines, mesh):
    poly_centerlines_sections = dict()
    poly_centerlines_clipped = dict()

    for index, centerline in poly_centerlines.items():
        # smoothing
        smoother = vmtkscripts.vmtkCenterlineSmoothing()
        smoother.Centerlines = centerline
        smoother.NumberOfSmoothingIterations = 100
        smoother.SmoothingFactor = 0.1
        smoother.Execute()

        # resampling
        resampler = vmtkscripts.vmtkCenterlineResampling()
        resampler.Centerlines = smoother.Centerlines
        resampler.Length = args.clspacing
        resampler.Execute()

        # skip points of the centerline at the distal end (target) of the veins
        n_skippoints = int(round(args.skippoints * resampler.Centerlines.GetNumberOfPoints()))

        # get number of nodes of shortened centerline
        n_nodes = resampler.Centerlines.GetNumberOfPoints() - n_skippoints

        # define points and line of the shortened centerline
        points = vtk.vtkPoints()
        polyline = vtk.vtkPolyLine()
        polyline.GetPointIds().SetNumberOfIds(n_nodes)

        # create new (shortened) centerline from points and swap directions of ids (index 0 is now in veins)
        for point_id in range(0, n_nodes):
            # add id to polyline
            polyline.GetPointIds().SetId(point_id, point_id)
            # compute point of id
            point = resampler.Centerlines.GetPoint(n_nodes - point_id)
            # add point to polyline
            points.InsertNextPoint(point)

        # define cell
        cells = vtk.vtkCellArray()
        cells.InsertNextCell(polyline)

        # create new centerline polydata from points and lines
        new_centerline = vtk.vtkPolyData()
        new_centerline.SetPoints(points)
        new_centerline.SetLines(cells)
        if not vtk.vtkVersion.GetVTKMajorVersion() > 5:
            new_centerline.Update()

        # resampling
        resampler = vmtkscripts.vmtkCenterlineResampling()
        resampler.Centerlines = new_centerline
        resampler.Length = args.clspacing
        resampler.Execute()

        poly_centerlines_clipped[index] = resampler.Centerlines

        # create sections
        sectioner = vmtkscripts.vmtkCenterlineSections()
        sectioner.Surface = mesh
        sectioner.Centerlines = resampler.Centerlines
        sectioner.Execute()

        poly_centerlines_sections[index] = sectioner.CenterlineSections

    return poly_centerlines_clipped, poly_centerlines_sections


def get_clippoints(args, sections):
    ids_clippoints_antrum = dict()
    # iterate through all centerlines
    for index, section in sections.items():
        # get arrays of the sections
        # binary array that indicates if the section is enclosed by the surface or not
        closed_section = vtk.util.numpy_support.vtk_to_numpy(section.GetCellData().GetArray('CenterlineSectionClosed'))
        # float array that contains the diameter of the maximum inscribed sphere
        max_dia = vtk.util.numpy_support.vtk_to_numpy(section.GetCellData().GetArray('CenterlineSectionMaxSize'))
        # float array that contains the area of the section
        area = vtk.util.numpy_support.vtk_to_numpy(section.GetCellData().GetArray('CenterlineSectionArea'))

        # get deltas
        delta_max_dia = max_dia[1:] - max_dia[:-1]
        delta_area = abs(area[1:] - area[:-1]) / args.clspacing

        # initialization
        id_clippoint_antrum = 0
        highcount = 0

        # iterate through all points until condition is satisfied
        for id in range(6, len(closed_section)):
            # check if section is closed
            if closed_section[id - 1]:
                maxslope_area = 10 * np.max(delta_area[id - 6 : id - 1]) / args.clspacing

                # compute change in diameter and area
                slope_dia = (max_dia[id] - max_dia[id - 1]) / args.clspacing
                slope_area = abs((area[id] - area[id - 1])) / args.clspacing

                # check if change is above defined threshold (slowly increasing)
                if slope_dia > args.highslope:
                    highcount += 1
                else:
                    highcount = 0

                # check if change is above high threshold (jump -> transition from vein to atrial body)
                if slope_dia > args.maxslope_dia:
                    break
                elif slope_area > maxslope_area:
                    break

            if highcount == 0:
                id_clippoint_antrum = id - 1
            else:
                id_clippoint_antrum = id - highcount


        ids_clippoints_antrum[index] = id_clippoint_antrum

    return ids_clippoints_antrum


def write_vtk(input, pathname):
    writer = vtk.vtkPolyDataWriter()
    writer.SetFileName(pathname)
    writer.SetInputData(input)
    writer.Write()


def write_ids(array, pathname):
    # convert to array
    array = np.array(array)
    f = open(pathname, 'w')

    if array.size == 0:
        f.write('0\nextra\n')
    elif array.size == 1:
        f.write('1\nextra\n')
        f.write(f'{array}\n')
    else:
        f.write(f'{len(array)}\nextra\n')
        for i in range(len(array)):
            f.write(f'{array[i]}\n')

    f.close()

def read_vtk(mesh, output="normal"):
    # check vtk format
    data_checker = vtk.vtkDataSetReader()
    data_checker.SetFileName(mesh)
    data_checker.Update()

    if data_checker.IsFilePolyData():
        reader = vtk.vtkPolyDataReader()
    elif data_checker.IsFileUnstructuredGrid():
        reader = vtk.vtkUnstructuredGridReader()
    else:
        print(f"File {mesh} cannot be read.")
        return

    # read vtk data
    reader.SetFileName(mesh)
    reader.Update()

    if output == "normal":
        return reader.GetOutput()
    elif output == "port":
        return reader.GetOutputPort()


def get_closest_id(input, point, clip_ids=True):
    loc = vtk.vtkPointLocator()
    loc.SetDataSet(input)
    loc.BuildLocator()

    if clip_ids == True:
        # get ids
        all_ids = vtk.util.numpy_support.vtk_to_numpy(input.GetPointData().GetArray("clip_Ids"))

        # get closest id
        closest_id = all_ids[int(loc.FindClosestPoint(point))]
    else:
        closest_id = int(loc.FindClosestPoint(point))

    return closest_id



if __name__ == '__main__':
    args = parser()
    clip_veins_main(args)