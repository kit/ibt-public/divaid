import argparse
import os.path

import vtk
import glob
import numpy as np
import pyvista as pv
from vtk.util.numpy_support import vtk_to_numpy
from vtk.numpy_interface import dataset_adapter as dsa

def parser():
    parser = argparse.ArgumentParser(description="Review_clip_veins")
    parser.add_argument('--mesh',
                        type=str,
                        default="",
                        help='path to meshname')
    parser.add_argument('--reviewed_atrium',
                        type=str,
                        default="LA",
                        choices=["LA", "RA"],
                        help='indicate which atrium is reviewed (independent of the displayed mesh)')

    return parser.parse_args()


def review_clip_veins_main(args, atrium):
    # get paths
    base_dir = f"{args.mesh}_division"
    clip_veins_dir = f"{base_dir}/stage2_clip_veins"

    # get mesh
    mesh = read_vtk(f"{clip_veins_dir}/{args.mesh.split('/')[-1]}_clip_ids.vtk")

    # get seed points
    seeds_paths_list = glob.glob(f"{clip_veins_dir}/{atrium}/{atrium}_seed_pt*.txt")
    seeds_pts = dict()
    for path in seeds_paths_list:
        # get index of seed
        index = path.split("_")[-1].split(".")[0]
        # get centerline
        seeds_pts[index] = np.loadtxt(path, skiprows=2, dtype=float)


    # get centerlines
    centerlines_paths_list = glob.glob(f"{clip_veins_dir}/{atrium}/{atrium}_centerline*.vtk")
    centerlines_poly = dict()
    for path in centerlines_paths_list:
        # get index of centerline
        index = path.split("_")[-1].split(".")[0]
        # don't process centerline to RAA
        if atrium == "RA" and index == "0":
            continue
        # get centerline
        centerlines_poly[index] = read_vtk(path)

    # get clippoints
    clippoints_paths_list = glob.glob(f"{clip_veins_dir}/{atrium}/{atrium}_clippoint*.txt")
    clippoints_ids = dict()
    for path in clippoints_paths_list:
        # get index of clippoint
        index = path.split("_")[-1].split(".")[0]
        # don't process centerline to RAA
        if atrium == "RA" and index == "0":
            continue
        # get id of clippoint
        clippoints_ids[index] = int(np.loadtxt(path, skiprows=2, dtype=int))

    # initialize flag to stop iteration
    all_clips_correct = []

    # intialize flag for automatic intersection check (only in first iteration)
    auto_intersection_check = 1

    # initialize dict with clip types (plane or path)
    clip_type = dict()
    for key in centerlines_poly.keys():
        clip_type[key] = 'plane'

    # initialize dict for path ids
    ids_paths = dict()

    # compute deleted ids
    ids_clipped = get_clipped_ids(mesh, centerlines_poly, clippoints_ids, clip_type, ids_paths)

    while all_clips_correct == []:
        # automatic intersection check (only in first iteration)
        # check if deleted ids share the same ids
        if auto_intersection_check == 1:
            for key1 in ids_clipped.keys():
                for key2 in ids_clipped.keys():
                    while key1 != key2 and len(np.intersect1d(ids_clipped[key1], ids_clipped[key2])) != 0\
                            and clippoints_ids[key1] > 0 and clippoints_ids[key2] > 0:
                        # shift back clippoint ids
                        clippoints_ids[key1] -= 1
                        clippoints_ids[key2] -= 1
                        # computed clipped ids
                        ids_clipped = get_clipped_ids(mesh, centerlines_poly, clippoints_ids, clip_type, ids_paths)

                        # shift index back even if there is no intersection anymore to obtain some mesh
                        # elements between orifices
                        if len(np.intersect1d(ids_clipped[key1], ids_clipped[key2])) == 0:
                            clippoints_ids[key1] -= 10
                            clippoints_ids[key2] -= 10
                            # computed clipped ids
                            ids_clipped = get_clipped_ids(mesh, centerlines_poly, clippoints_ids, clip_type, ids_paths)

        # automatic intersection check is only performed in first iteration
        auto_intersection_check = 0

        # get clipped mesh
        mesh_clipped = delete_ids_from_mesh(mesh, concatenate_dict_arrays(ids_clipped))

        # get clippoints (to plot them)
        clippoints_pts = dict()
        for key in clippoints_ids.keys():
            centerline = centerlines_poly[key]
            id = clippoints_ids[key]
            clippoints_pts[key] = vtk.util.numpy_support.vtk_to_numpy(centerline.GetPoints().GetData())[id]

        # show deleted ids
        p = pv.Plotter(window_size=(2200, 1600))

        # initalize list with vein that is to be edited
        edit_vein = []

        # callbacks depending on the number of veins
        class button_callbacks:
            def __init__(self, centerline_id):
                self.id = centerline_id

            def __call__(self, state):
                edit_vein.append(self.id)

        # plot clipped mesh
        p.add_mesh(mesh, show_edges=False, pickable=True, lighting=True,
                   interpolate_before_map=False, show_scalar_bar=False, opacity=0.3, color='C19A6B')
        p.add_mesh(mesh_clipped, show_edges=False, pickable=True, lighting=True,
                   interpolate_before_map=False, show_scalar_bar=False, opacity=1, color='C19A6B')

        # add labels of veins at clipping points
        for key, pt in clippoints_pts.items():
            p.add_point_labels(pt, [key], font_size=40)

        # initialize factor for position of buttons and text
        i_pos = 0
        # add buttons and text for editing the vein clips
        for i in sorted(ids_clipped.keys()):
            # callback buttons for automatic adaption of the clippoints with a plane
            callback1 = button_callbacks(f"a{i}")
            # callback buttons for manual adaption of the clippoints with a path
            callback2 = button_callbacks(f"m{i}")
            p.add_checkbox_button_widget(callback=callback1, value=True, position=[200 * i_pos + 400, 100],
                                         color_off='green', color_on='grey', border_size=0)
            p.add_checkbox_button_widget(callback=callback2, value=True, position=[200 * i_pos + 400, 20],
                                         color_off='green', color_on='grey', border_size=0)
            p.add_text(f"Vein {i}", position=[200 * i_pos + 400, 180])

            # update position index
            i_pos += 1

        # add text
        p.add_text("Edit clip with plane:", position=[20, 100])
        p.add_text("Edit clip with path:", position=[20, 20])
        p.add_text("Select checkbox to edit vein clipping automatically (with plane) or manually (with path) and "
                    "close the window.\nIf all veins are clipped correctly, just close the window.", position=[20, 260])

        # add title
        p.add_title("Review vein clipping")

        # set orientation
        p.camera_position = 'xy'

        p.show()

        # select which clip should be adapted -> manually or with plane or end clipping adaption
        if edit_vein == []:
            all_clips_correct.append(1)
        else:
            if edit_vein[0][0] == "a":
                # edit clip with plane
                ids_clipped, clippoints_ids, clip_type = edit_clip_with_plane(mesh, centerlines_poly, clippoints_ids,
                                                                              edit_vein, clip_type, ids_paths)
            elif edit_vein[0][0] == "m":
                # edit clip with path
                ids_clipped, clippoints_ids, clip_type, ids_paths = edit_clip_with_path(mesh, centerlines_poly,
                                                                                        clippoints_ids, edit_vein,
                                                                                        clip_type, ids_paths)

    # get all ids
    all_ids = vtk.util.numpy_support.vtk_to_numpy(mesh.GetPointData().GetArray("clip_Ids"))

    # for the regionalization, the veins should be clipped at the antrum (when the vein enters the atrium), however
    # a small part of the veins still belongs to the venoatrial junctions
    # initialize dict for clippoint ids at distal end of the vein
    clippoints_ids_dist = dict()

    # initialize dict for mesh and ids of clipped veins
    clipped_veins_mid_poly = dict()

    # clip veins at defined distance (default: clspacing = 0.4 -> 6 mm)
    clip_dist = 15
    for key in clippoints_ids.keys():
        # not for LAA or RAA
        if key == "0":
            continue

        # check if distance from clippoint id to the beginning of the centerline is sufficient
        if clippoints_ids[key] > clip_dist:
            # set distal clippoint
            clippoints_ids_dist[key] = clippoints_ids[key] - clip_dist

        elif clippoints_ids[key] >= 0:
            # set distal clippoint
            clippoints_ids_dist[key] = 0

        else:
            continue

        # save only part of the vein to re-append it or save entire vein
        if clippoints_ids_dist[key] != 0:
            # apply plane cut
            ids_clipped_dist = get_clipped_ids_plane(mesh, centerlines_poly[key], clippoints_ids_dist[key])

            # get ids of vein between antrum and distal end
            ids_clipped_mid = np.setdiff1d(ids_clipped[key], ids_clipped_dist)

        else:
            ids_clipped_mid = ids_clipped[key]

        # get mesh of middle part of the veins
        clipped_veins_mid_poly[key] = delete_ids_from_mesh(mesh, np.setdiff1d(all_ids, ids_clipped_mid), all_scalars=False)

        # write middle part of the veins
        write_vtk(clipped_veins_mid_poly[key], f"{clip_veins_dir}/{atrium}/{atrium}_vein_{key}.vtk")

    # save points from clippoints
    for key in clippoints_pts.keys():
        # write point
        write_ids(clippoints_pts[key], f"{clip_veins_dir}/{atrium}/{atrium}_clippoint_pt_{key}.txt")

    # save mesh with clipped veins (no LAA/RAA clip)
    ids_clipped_copy = ids_clipped.copy()
    if atrium == "LA":
        ids_clipped_copy["0"] = []
    # get clipped mesh
    mesh_clipped_final = delete_ids_from_mesh(mesh, concatenate_dict_arrays(ids_clipped_copy))
    write_vtk(mesh_clipped_final, f"{clip_veins_dir}/{args.mesh.split('/')[-1]}_clipped.vtk")

    # get id of closest point to appendage seed
    app_id = get_closest_id(mesh_clipped_final, seeds_pts["0"], clip_ids=False)
    # write id
    write_ids(app_id, f"{clip_veins_dir}/{atrium}/{atrium}_id_{atrium}A.txt")

    # also re-calculate the appendage seed of the LA after the RA is clipped because the id might change due to the
    # clipping
    if args.atrium == "biatrial" and atrium == "RA":
        # load LAA pt
        laa_pt = np.loadtxt(f"{clip_veins_dir}/LA/LA_seed_pt_0.txt", skiprows=2, dtype=float)
        # get id
        laa_id = get_closest_id(mesh_clipped_final, laa_pt, clip_ids=False)
        # write id
        write_ids(laa_id, f"{clip_veins_dir}/LA/LA_id_LAA.txt")

        # re-calculate ids of LAA path if exists
        if os.path.exists(f"{clip_veins_dir}/LA/LAA_path_ids.txt"):
            # load path ids
            ids_path_laa = np.loadtxt(f"{clip_veins_dir}/LA/LAA_path_ids.txt", skiprows=2, dtype=float)
            # get all ids of clipped mesh
            ids_all_clipped = vtk.util.numpy_support.vtk_to_numpy(mesh_clipped_final.GetPointData().GetArray("clip_Ids"))
            # update path ids for clipped mesh
            ids_path_laa = np.where(np.isin(ids_all_clipped, ids_path_laa))[0]
            # save path
            write_ids(ids_path_laa, f"{clip_veins_dir}/LA/LAA_path_ids.txt")

    # save LAA clippoint abd clipnormal
    if atrium == "LA":
        if clip_type["0"] == "plane":
            # get clippoint and clipnormal
            pt_clip_laa, n_laa = get_clippoint_clipnormal(centerlines_poly["0"], clippoints_ids["0"])
            laa_clippoint_clipnormal = np.array([pt_clip_laa, n_laa]).flatten()
            # save clippoint and clipnormal
            write_ids(laa_clippoint_clipnormal, f"{clip_veins_dir}/LA/LAA_clip_point_norm.txt")
        else:
            # get all ids of clipped mesh
            ids_all_clipped = vtk.util.numpy_support.vtk_to_numpy(mesh_clipped_final.GetPointData().GetArray("clip_Ids"))
            # update path ids for clipped mesh
            ids_path_laa = np.where(np.isin(ids_all_clipped, ids_paths["0"]))[0]
            # save path
            write_ids(ids_path_laa, f"{clip_veins_dir}/LA/LAA_path_ids.txt")









'''universal functions'''
def read_vtk(mesh):
    # check vtk format
    data_checker = vtk.vtkDataSetReader()
    data_checker.SetFileName(mesh)
    data_checker.Update()

    if data_checker.IsFilePolyData():
        reader = vtk.vtkPolyDataReader()
    elif data_checker.IsFileUnstructuredGrid():
        reader = vtk.vtkUnstructuredGridReader()

    # read vtk data
    reader.SetFileName(mesh)
    reader.Update()

    return reader.GetOutput()

def write_vtk(polydata, pathname):
    writer = vtk.vtkPolyDataWriter()
    writer.SetInputData(polydata)
    writer.SetFileName(pathname)
    writer.Write()


def write_ids(array, pathname):
    # convert to array
    array = np.array(array)
    f = open(pathname, 'w')

    if array.size == 0:
        f.write('0\nextra\n')
    elif array.size == 1:
        f.write('1\nextra\n')
        f.write(f'{array}\n')
    else:
        f.write(f'{len(array)}\nextra\n')
        for i in range(len(array)):
            f.write(f'{array[i]}\n')

    f.close()


def plane(origin, v1=np.zeros(3), v2=np.zeros(3), n=np.zeros(3)):
    if np.all(n == 0):
        # compute crossproduct
        n = np.cross(v1, v2)

    # normalize normal vector
    norm = np.linalg.norm(n, keepdims=True)
    n_norm = n / norm

    # define plane
    plane = vtk.vtkPlane()
    plane.SetNormal(n_norm[0], n_norm[1], n_norm[2])
    plane.SetOrigin(origin[0], origin[1], origin[2])

    return plane


def plane_cut(input, plane):
    # apply plane cut
    meshExtractFilter = vtk.vtkExtractGeometry()
    meshExtractFilter.SetInputData(input)
    meshExtractFilter.SetImplicitFunction(plane)
    meshExtractFilter.Update()

    # get polydata
    geo_filter = vtk.vtkGeometryFilter()
    geo_filter.SetInputData(meshExtractFilter.GetOutput())
    geo_filter.Update()

    return geo_filter.GetOutput()


def get_connected_region(input, point=np.zeros(3), mode='largest'):
    connect = vtk.vtkConnectivityFilter()
    connect.SetInputData(input)
    if mode == 'closest':
        connect.SetExtractionModeToClosestPointRegion()
        connect.SetClosestPoint(point)
    elif mode == 'largest':
        connect.SetExtractionModeToLargestRegion()
    connect.Update()

    # clean unused points
    cln = vtk.vtkCleanPolyData()
    cln.SetInputData(connect.GetOutput())
    cln.Update()

    return cln.GetOutput()


def get_closest_id(input, point, clip_ids=True):
    loc = vtk.vtkPointLocator()
    loc.SetDataSet(input)
    loc.BuildLocator()

    if clip_ids == True:
        # get ids
        all_ids = vtk.util.numpy_support.vtk_to_numpy(input.GetPointData().GetArray("clip_Ids"))

        # get closest id
        closest_id = all_ids[int(loc.FindClosestPoint(point))]
    else:
        closest_id = int(loc.FindClosestPoint(point))

    return closest_id


def get_clippoint_clipnormal(centerline_poly, clippoint_id):
    # get all points on centerline
    pts_centerline = vtk.util.numpy_support.vtk_to_numpy(centerline_poly.GetPoints().GetData())

    # get clippoint and successor on centerline
    pt_clip = pts_centerline[clippoint_id]
    pt_clip_suc = pts_centerline[clippoint_id + 1]

    # get normal vector (successor on centerline)
    n_clip = pt_clip_suc - pt_clip

    return pt_clip, n_clip


def concatenate_dict_arrays(dictionary):
    dict_list = []
    for i in list(dictionary.values()):
        dict_list.extend(i)

    dict_array = np.array(dict_list)

    return dict_array


def delete_ids_from_mesh(mesh, ids_to_delete, all_scalars=True):
    # initialize array that contains the ids that are to be deleted
    to_delete = np.zeros(len(vtk.util.numpy_support.vtk_to_numpy(mesh.GetPointData().GetArray("clip_Ids"))), dtype=int)
    to_delete[list(ids_to_delete)] = 1

    # remove ids of the path defining the region
    mesh = dsa.WrapDataObject(mesh)
    mesh.PointData.append(to_delete, "to_delete")
    thresh = vtk.vtkThreshold()
    thresh.SetInputData(mesh.VTKObject)
    thresh.ThresholdByLower(0)
    thresh.SetInputArrayToProcess(0, 0, 0, "vtkDataObject::FIELD_ASSOCIATION_POINTS", "to_delete")
    thresh.SetAllScalars(all_scalars)
    thresh.Update()

    geo_filter = vtk.vtkGeometryFilter()
    geo_filter.SetInputConnection(thresh.GetOutputPort())
    geo_filter.Update()

    return geo_filter.GetOutput()


def compute_spline_on_mesh(mesh, pts_selected):
    # create spline
    spline = pv.Spline(np.vstack([pts_selected, pts_selected[0]]), len(pts_selected) * 10)
    # get points on spline
    pts_spline = np.array(spline.points)
    # get ids of closest points on mesh
    ids_closest_mesh = []
    for pt in pts_spline:
        # get closest id
        closest_id = get_closest_id(mesh, pt)
        # append id
        ids_closest_mesh.append(closest_id)

    # add startnode to the end
    ids_closest_mesh.append(ids_closest_mesh[0])

    # compute paths (project spline onto mesh)
    ids_paths = []
    poly_paths = []
    for i, id in enumerate(ids_closest_mesh):
        # check if there is a following id (endnode)
        if i < len(ids_closest_mesh) - 1:
            # check if startnode and endnode are the same
            if ids_closest_mesh[i] != ids_closest_mesh[i + 1]:
                # compute path
                temp_poly, temp_ids = shortest_geodesic_path(mesh, id, ids_closest_mesh[i + 1])
                # append data
                ids_paths.append(temp_ids)
                poly_paths.append(temp_poly)

    # concatenate ids
    ids_paths_all = np.concatenate(ids_paths)
    # concatenate polydata
    poly_paths_all = append_polydata(poly_paths)

    return poly_paths_all, ids_paths_all


def shortest_geodesic_path(mesh, startnode, endnode):
    # get all ids
    ids = vtk.util.numpy_support.vtk_to_numpy(mesh.GetPointData().GetArray("clip_Ids"))

    # compute path with flipped startnode and endnode as the search always starts at the endnode
    dijkstra = vtk.vtkDijkstraGraphGeodesicPath()
    dijkstra.SetInputData(mesh)
    dijkstra.SetStartVertex(int(np.where(ids == endnode)[0][0]))
    dijkstra.SetEndVertex(int(np.where(ids == startnode)[0][0]))
    dijkstra.Update()

    path_ids = np.array([dijkstra.GetIdList().GetId(i) for i in range(dijkstra.GetIdList().GetNumberOfIds())])
    # internal ids may not be the same as the clip_ids (e.g. when some nodes on the mesh are temporarily removed)
    path_ids_entire_model = ids[path_ids]

    # get polydata
    path_poly = dijkstra.GetOutput()

    return path_poly, path_ids_entire_model

def append_polydata(list_polydata):
    # unite polydata
    append = vtk.vtkAppendPolyData()
    for polydata in list_polydata:
        append.AddInputData(polydata)
    append.Update()

    # clear duplicate points
    cleaner = vtk.vtkCleanPolyData()
    cleaner.SetInputConnection(append.GetOutputPort())
    cleaner.Update()

    return cleaner.GetOutput()



def get_clipped_ids_plane(mesh, centerline_poly, clippoint_id):
    # get clippoint and clipnormal
    pt_clip, n_clip = get_clippoint_clipnormal(centerline_poly, clippoint_id)

    # apply plane cut
    clipped_mesh = plane_cut(mesh, plane(pt_clip, n=n_clip))

    # get closest connected region to clipping point
    vein = get_connected_region(clipped_mesh, pt_clip, mode="closest")

    # get ids of clipped vein
    ids_to_delete = vtk.util.numpy_support.vtk_to_numpy(vein.GetPointData().GetArray("clip_Ids"))

    return ids_to_delete


def get_clipped_ids_path(mesh, path_ids):
    # get all ids of the mesh
    ids_all = vtk.util.numpy_support.vtk_to_numpy(mesh.GetPointData().GetArray("clip_Ids"))

    # get mesh with removed path
    mesh_separated = delete_ids_from_mesh(mesh, path_ids)

    # get largest connected region (not the vein but the atrial body)
    mesh_separated_largest = get_connected_region(mesh_separated, mode='largest')

    # get ids that are not on the vein
    ids_not_vein = vtk.util.numpy_support.vtk_to_numpy(mesh_separated_largest.GetPointData().GetArray("clip_Ids"))

    # get ids of clipped vein
    ids_to_delete = np.setdiff1d(ids_all, ids_not_vein)

    return ids_to_delete


def get_clipped_ids(mesh, centerlines_poly, clippoints_ids, clip_type, paths_ids):
    # initialize dictionaries for clip types
    if paths_ids is None:
        paths_ids = []
    ids_clipped_plane = dict()
    ids_clipped_path = dict()

    # iterate through all veins
    for key in centerlines_poly.keys():
        if clip_type[key] == "plane":
            ids_clipped_plane[key] = get_clipped_ids_plane(mesh, centerlines_poly[key], clippoints_ids[key])
        elif clip_type[key] == "path":
            ids_clipped_path[key] = get_clipped_ids_path(mesh, paths_ids[key])

    # merge dictionaries
    ids_clipped = ids_clipped_plane.copy()
    ids_clipped.update(ids_clipped_path)

    return ids_clipped


def edit_clip_with_plane(mesh, centerlines_poly, clippoints_ids, edit_vein, clip_type, ids_paths):
    # initalize empty dict for clipped ids
    ids_clipped = dict()

    # get index of edited vein
    vein_id = edit_vein[0][1]

    # show mesh, centerline and points on centerline labelled with ids
    p = pv.Plotter(window_size=(2200, 1600))

    def create_clip(clip_id):
        # assign new clippoint id to dict
        clippoints_ids[vein_id] = int(clip_id)
        # get ids to delete (clipped veins)
        temp_ids_clipped = get_clipped_ids(mesh, centerlines_poly, clippoints_ids, clip_type, ids_paths)
        ids_clipped.update(temp_ids_clipped)
        # remove ids from mesh
        mesh_clipped = delete_ids_from_mesh(mesh, concatenate_dict_arrays(ids_clipped))
        # create pv polydata
        pv_mesh_clipped = pv.PolyData(mesh_clipped)
        pv_mesh = pv.PolyData(mesh)
        # add mesh
        p.add_mesh(pv_mesh, name="mesh", show_edges=False, pickable=True, lighting=True,
                   interpolate_before_map=False, show_scalar_bar=False, opacity=0.3, color='C19A6B')
        p.add_mesh(pv_mesh_clipped, name="clipped_mesh", show_edges=False, pickable=True, lighting=True,
                   interpolate_before_map=False, show_scalar_bar=False, opacity=1, color='C19A6B')

        # add centerline
        pv_centerline = pv.PolyData(centerlines_poly[vein_id])
        p.add_mesh(pv_centerline, color='grey', line_width=3)

        # add points on centerline
        centerline_pts = vtk.util.numpy_support.vtk_to_numpy(centerlines_poly[vein_id].GetPoints().GetData())
        # plot every 10th point
        for label, pt in enumerate(centerline_pts[::10]):
            # add point as sphere
            p.add_mesh(pv.Sphere(radius=1, center=pt), color='black', pickable=False)



    # get ids of centerline
    n_ids = int(centerlines_poly[vein_id].GetNumberOfPoints())

    # add slider
    p.add_slider_widget(create_clip, [0, n_ids - 1], value=clippoints_ids[vein_id], title="Clippoint_Id",
                        pointa=(0.1, 0.1), pointb=(0.9,0.1))

    # add text
    p.add_text("Points show every 10th Id starting with 0 at the distal end of the vein.\n"
               "Move slider to select desired id and close the window to confirm the selection.",
               position=(0.1, 0.2), viewport=True)

    # add title
    p.add_title("Edit clip with plane perpendicular to centerline")

    # set orientation
    p.camera_position = 'xy'

    p.show()

    # update clip type
    clip_type[vein_id] = "plane"

    return ids_clipped, clippoints_ids, clip_type


def edit_clip_with_path(mesh, centerlines_poly, clippoints_ids, edit_vein, clip_type, ids_paths):
    # get index of edited vein
    vein_id = edit_vein[0][1]

    # get all points from mesh
    all_pts = vtk.util.numpy_support.vtk_to_numpy(mesh.GetPoints().GetData())

    # compute clipped mesh
    ids_clipped = get_clipped_ids(mesh, centerlines_poly, clippoints_ids, clip_type, ids_paths)
    # no cut of the vein that is processed
    ids_clipped_with_curr_vein = ids_clipped.copy()
    ids_clipped_with_curr_vein[vein_id] = []
    mesh_clipped = delete_ids_from_mesh(mesh, concatenate_dict_arrays(ids_clipped_with_curr_vein))

    # update clip type
    clip_type[vein_id] = "path"

    # initialize list with selected points and corresponding actors
    pts_selected = []
    actor_pts_selected = []

    # initialize actor for spline
    actor_polyline = []

    # initialize actor for clipped mesh
    actor_mesh = []

    p = pv.Plotter(window_size=(2200, 1600), notebook=False)

    def undo_callback():
        # remove last selected point
        if actor_pts_selected != []:
            # remove actor from mesh
            p.remove_actor(actor_pts_selected[-1])
            # remove actor from list
            actor_pts_selected.pop(-1)
            # remove point from list
            pts_selected.pop(-1)
        if actor_polyline != []:
            # remove actor from mesh
            p.remove_actor(actor_polyline)
            # remove actor from list
            actor_polyline.pop(-1)
        # compute spline with remaining points
        if len(pts_selected) > 1:
            temp_polyline, temp_ids = compute_spline_on_mesh(mesh, pts_selected)
            temp_actor = p.add_mesh(temp_polyline, color="blue", line_width=15)
            # add actor to list
            actor_polyline.append(temp_actor)
            # add ids to dict
            ids_paths[vein_id] = temp_ids

            # compute updated clipped ids
            ids_clipped_temp = get_clipped_ids(mesh, centerlines_poly, clippoints_ids, clip_type, ids_paths)

            # update global dict
            for key in ids_clipped_temp.keys():
                ids_clipped[key] = ids_clipped_temp[key]

    def visualize_clip(state):
        # visualize clip
        if state == 0:
            # get clipped mesh (with clip of current vein)
            mesh_clipped_preview = delete_ids_from_mesh(mesh, concatenate_dict_arrays(ids_clipped))

        # visualize entire vein
        else:
            # get clipped mesh (without clip of current vein)
            mesh_clipped_preview = delete_ids_from_mesh(mesh, concatenate_dict_arrays(ids_clipped_with_curr_vein))

        # remove previous actor
        p.remove_actor(actor_mesh)

        # add new actor
        actor_mesh.append(p.add_mesh(mesh_clipped_preview, show_edges=True, pickable=True, color='C19A6B'))

    def path_callback(pt):
        # plot point (sphere)
        if actor_pts_selected == []:
            # set different color for startnode
            pt_color = "orange"
        else:
            pt_color = "blue"
        temp_actor = p.add_mesh(pv.Sphere(radius=0.5, center=pt), color=pt_color, pickable=False)
        # add actor of new point to list
        actor_pts_selected.append(temp_actor)
        # add id to list
        pts_selected.append(pt)

        # create spline if more than 2 ids are selected
        if len(pts_selected) > 1:
            # remove previous actor first
            if actor_polyline != []:
                # remove actor from mesh
                p.remove_actor(actor_polyline)
                # remove actor from list
                actor_polyline.pop(-1)

            # compute polyline (spline)
            temp_polyline, temp_ids = compute_spline_on_mesh(mesh, pts_selected)
            temp_actor = p.add_mesh(temp_polyline, color="blue", line_width=15)
            # add actor to list
            actor_polyline.append(temp_actor)
            # add ids to dict
            ids_paths[vein_id] = temp_ids

            # compute updated clipped ids
            ids_clipped_temp = get_clipped_ids(mesh, centerlines_poly, clippoints_ids, clip_type, ids_paths)

            # update global dict
            for key in ids_clipped_temp.keys():
                ids_clipped[key] = ids_clipped_temp[key]

    # add mesh
    p.add_mesh(mesh, show_edges=True, pickable=True, color='C19A6B', opacity=0.3)
    actor_mesh.append(p.add_mesh(mesh_clipped, show_edges=True, pickable=True, color='C19A6B'))

    # add interactor for point picking on the mesh
    p.enable_point_picking(callback=path_callback,
                           show_message="Right click or press 'p' to select node under mouse."
                                        "\nPress 'u' to remove last selected point."
                                        "\nPress checkbox to preview results."
                                        "\nClose window if clip is correct.",
                           font_size=18, color='pink', point_size=50, show_point=False, left_clicking=False,
                           pickable_window=False)
    p.add_key_event("u", callback=undo_callback)

    # add checkbox to preview clip
    p.add_checkbox_button_widget(callback=visualize_clip, value=True, position=[2000, 100], color_off='C19A6B',
                                 color_on='grey', border_size=0)

    # set orientation
    p.camera_position = 'xy'

    p.show()

    # get center / mean of calculated path
    path_center = np.mean(all_pts[ids_paths[vein_id]], axis=0)
    # get closest clippoint id on centerline
    clippoints_ids[vein_id] = get_closest_id(centerlines_poly[vein_id], path_center, clip_ids=False)

    return ids_clipped, clippoints_ids, clip_type, ids_paths



if __name__ == '__main__':
    args = parser()
    review_clip_veins_main(args, args.reviewed_atrium)