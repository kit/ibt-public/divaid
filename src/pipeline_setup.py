# This file replaces the absolute path of the conda installation and the conda environments, which are activated from
# the main script to run the subprocesses of the remeshing and vein clipping procedure
import os

'''
# read main file line by line
with open("../venvs_paths.txt") as file:
    lines = file.readlines()
'''

# get path to conda and to conda venv
conda = input("\n\nPath to conda (e.g. /Users/.../anaconda3/bin/activate): ")
venv_remeshing = input("Name of remeshing venv (e.g. venv_remeshing): ")
venv_clip_vein = input("Name of vein clipping venv (e.g. venv_clipvein): ")

# initialize lines list
lines = []
# add path to conda
lines.append(f"Path to conda: {conda}\n")
# add name of remeshing venv
lines.append(f"Name of remeshing venv: {venv_remeshing}\n")
# add name of vein clipping venv
lines.append(f"Name of vein clipping venv: {venv_clip_vein}\n\n")

# add command to activate remeshing venv
lines.append(f'conda_act_remeshing: {conda} {venv_remeshing} && \n')
# add command to deactivate remeshing venv
lines.append(f'conda_deact_remeshing: && {"/".join(conda.split("/")[:-1])}/deactivate\n')
# set path for remeshing venv (only needed for PyCharm terminal)
lines.append(f'os_environ: {"/".join(conda.split("/")[:-2])}/envs/{venv_remeshing}/bin\n\n')


# add command to activate vein clipping venv
lines.append(f'conda_act_clip_vein: {conda} {venv_clip_vein} && \n')
# add command to deactivate vein clipping venv
lines.append(f'conda_deact_clip_vein: && {"/".join(conda.split("/")[:-1])}/deactivate\n')
# set path variable for vein clipping venv (only needed for PyCharm terminal)
lines.append(f'os_environ: {"/".join(conda.split("/")[:-2])}/envs/{venv_clip_vein}/bin\n\n')






'''
# get lines in which commands have to be replaced
line_conda_act_remeshing = [lines.index(i) for i in lines if ("conda_act_remeshing = " in i)][0]
line_conda_deact_remeshing = [lines.index(i) for i in lines if ("conda_deact_remeshing = " in i)][0]
line_conda_act_clip_vein = [lines.index(i) for i in lines if ("conda_act_clip_vein = " in i)][0]
line_conda_deact_clip_vein = [lines.index(i) for i in lines if ("conda_deact_clip_vein = " in i)][0]
line_os_environ = [lines.index(i) for i in lines if ("os.environ" in i)]

# add empty space
tab = "        "

# replace line with the path to conda and the remeshing environment

# activate remeshing conda venv
lines[line_conda_act_remeshing] = f'{tab}conda_act_remeshing = "{conda} {venv_remeshing} && "\n'
# deactivate remeshing conda venv
lines[line_conda_deact_remeshing] = f'{tab}conda_deact_remeshing = "&& {"/".join(conda.split("/")[:-1])}/deactivate"\n'
# set path variable for remeshing conda venv
lines[line_os_environ[0]] = f'{tab}os.environ["PATH"] = f"{"/".join(conda.split("/")[:-2])}/envs/{venv_remeshing}/bin:'
lines[line_os_environ[0]] += "{os.environ['PATH']}\"\n"

# activate clip vein conda venv
lines[line_conda_act_clip_vein] = f'{tab}conda_act_clip_vein = "{conda} {venv_clip_vein} && "\n'
# deactivate clip vein conda venv
lines[line_conda_deact_clip_vein] = f'{tab}conda_deact_clip_vein = "&& {"/".join(conda.split("/")[:-1])}/deactivate"\n'
# set path variable for clipvein conda venv
lines[line_os_environ[1]] = f'{tab}os.environ["PATH"] = f"{"/".join(conda.split("/")[:-2])}/envs/{venv_clip_vein}/bin:'
lines[line_os_environ[1]] += "{os.environ['PATH']}\"\n"
'''


# write main file line by line
with open("../venvs_paths.txt", 'w') as file:
    for line in lines:
        file.write(line)

print("\nPipeline setup completed.\n\n")