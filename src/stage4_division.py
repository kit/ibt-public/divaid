import os
import shutil
import argparse
import numpy as np
import vtk
import glob
import pyvista as pv
from vtk.numpy_interface import dataset_adapter as dsa
from matplotlib.colors import LinearSegmentedColormap
from matplotlib.colors import to_rgba

def parser():
    parser = argparse.ArgumentParser(description='Compute division.')
    parser.add_argument('--mesh',
                        type=str,
                        default="",
                        help='path to mesh without extension')
    parser.add_argument('--atrium',
                        type=str,
                        default="biatrial",
                        choices=["LA", "RA", "biatrial"],
                        help='indicate if the geometry is mono- or biatrial')
    parser.add_argument('--subdivision',
                        type=int,
                        default=0,
                        choices=[0, 1],
                        help='set to 0 for no subdivisions, otherwise set to 1')

    return parser.parse_args()


def run_division(args):
    if args.atrium == "LA" or args.atrium == "biatrial":
        ids_la_bound, ids_points = la_create_boundaries(args)
        mesh_final = la_division(args, ids_la_bound, ids_points)
        plot(args, "LA", mesh_final)

    if args.atrium == "RA" or args.atrium == "biatrial":
        ids_ra_bound, ids_points = ra_create_boundaries(args)
        mesh_final = ra_division(args, ids_ra_bound, ids_points)
        plot(args, "RA", mesh_final)


def la_create_boundaries(args):
    # get paths
    base_dir = f"{args.mesh}_division"
    clip_veins_dir = f"{base_dir}/stage2_clip_veins"
    extr_rings_dir = f"{base_dir}/stage3_extract_rings"
    div_dir = f"{base_dir}/stage4_division"

    # read mesh (of extr_rings directory)
    LA = read_vtk(f"{extr_rings_dir}/LA.vtk")

    # read polydata of MV ring
    mv_poly = read_vtk(f"{extr_rings_dir}/MV.vtk")

    # load centers
    table = np.loadtxt(f"{extr_rings_dir}/rings_centroids.csv", delimiter=",", dtype=str)
    lspv_center = np.asarray(table[1:4, int(np.where(table[0] == "LSPV")[0])], dtype=float)
    lipv_center = np.asarray(table[1:4, int(np.where(table[0] == "LIPV")[0])], dtype=float)
    rspv_center = np.asarray(table[1:4, int(np.where(table[0] == "RSPV")[0])], dtype=float)
    ripv_center = np.asarray(table[1:4, int(np.where(table[0] == "RIPV")[0])], dtype=float)
    mv_center = np.asarray(table[1:4, int(np.where(table[0] == "MV")[0])], dtype=float)

    # load ring-ids
    mv_ids = np.loadtxt(f"{extr_rings_dir}/ids_MV.vtx", skiprows=2, dtype=int)
    lspv_ids = np.loadtxt(f"{extr_rings_dir}/ids_LSPV.vtx", skiprows=2, dtype=int)
    lipv_ids = np.loadtxt(f"{extr_rings_dir}/ids_LIPV.vtx", skiprows=2, dtype=int)
    rspv_ids = np.loadtxt(f"{extr_rings_dir}/ids_RSPV.vtx", skiprows=2, dtype=int)
    ripv_ids = np.loadtxt(f"{extr_rings_dir}/ids_RIPV.vtx", skiprows=2, dtype=int)

    # get all ids
    all_ids = vtk.util.numpy_support.vtk_to_numpy(LA.GetPointData().GetArray("Ids"))

    # import LAA clip point and normal (if exists)
    if os.path.exists(f"{clip_veins_dir}/LA/LAA_clip_point_norm.txt"):
        temp = np.loadtxt(f"{clip_veins_dir}/LA/LAA_clip_point_norm.txt", skiprows=2, dtype=float)
        laa_clippoint = temp[0:3]
        laa_clipnormal = temp[3:6]

        # remove LAA from LA to assure that paths bypass that region
        # cut LAA with plane
        poly_plane_cut_laa = plane_cut(LA, plane(laa_clippoint, n=laa_clipnormal))
        # get poly of LAA
        poly_laa = get_closest_connected_region(poly_plane_cut_laa, laa_clippoint)
        # get ids of LAA
        ids_laa = vtk.util.numpy_support.vtk_to_numpy(poly_laa.GetPointData().GetArray("Ids"))
        # get LA polydata without LAA
        LA_no_laa = delete_ids_from_mesh(LA, ids_laa)

        # get boundary edges (path) of the LAA
        path_laa_poly = get_boundary_edges(poly_laa)
        # get ids of path
        path_laa_ids = vtk.util.numpy_support.vtk_to_numpy(path_laa_poly.GetPointData().GetArray("Ids"))

    else:
        path_laa_ids = np.loadtxt(f"{clip_veins_dir}/LA/LAA_path_ids.txt", skiprows=2, dtype=int)
        #path_laa_poly = read_vtk(f"{div_dir}/path_poly_la/path_laa.vtk")

        # remove path ids from LA
        LA_no_laa_path = delete_ids_from_mesh(LA, path_laa_ids)
        # remove LAA from LA
        LA_no_laa = get_largest_connected_region(LA_no_laa_path)
        # get ids of LAA
        ids_laa = np.setdiff1d(all_ids, vtk.util.numpy_support.vtk_to_numpy(LA_no_laa.GetPointData().GetArray("Ids")))

    # remove directory if exists
    if os.path.exists(f"{div_dir}/path_ids_la"):
        shutil.rmtree(f"{div_dir}/path_ids_la")
    if os.path.exists(f"{div_dir}/path_poly_la"):
        shutil.rmtree(f"{div_dir}/path_poly_la")
    # create new directories
    if not os.path.exists(div_dir):
        os.makedirs(div_dir)
    os.makedirs(f"{div_dir}/path_ids_la")
    os.makedirs(f"{div_dir}/path_poly_la")

    ########################################
    # Get global orientation (body axes) of the LA
    ########################################
    # x-axis (pointing from lateral to septal)
    # y-axis (pointing from posterior to anterior)
    # z-axis (pointing from inferior to superior)

    ''' x-axis '''
    # compute vector from lateral to septal
    lpv_mean = np.mean([lspv_center, lipv_center], axis=0)
    rpv_mean = np.mean([rspv_center, ripv_center], axis=0)
    #v_lat_sept = rpv_mean - lpv_mean
    v_lat_sept = rpv_mean - lipv_center

    # calculate normal vector of the plane going through lpv_mean, rpv_mean and mv_center (pointing anterior)
    #n_lpv_rpv_mv = np.cross(lpv_mean - mv_center, rpv_mean - mv_center)
    n_lpv_rpv_mv = np.cross(lipv_center - mv_center, rpv_mean - mv_center)
    # normalize vector
    n_lpv_rpv_mv = n_lpv_rpv_mv / np.linalg.norm(n_lpv_rpv_mv)

    # rotate lat-sept vector around normal vector to get anatomical orientation
    angle_lat_sept = - 15 * np.pi / 180

    # Rodrigues' rotation formula
    x = Rodrigues_rot_form(v_lat_sept, n_lpv_rpv_mv, angle_lat_sept)
    #x = v_lat_sept * np.cos(angle_lat_sept) + np.cross(n_lpv_rpv_mv, v_lat_sept) * np.sin(angle_lat_sept) \
        #+ n_lpv_rpv_mv * np.dot(n_lpv_rpv_mv, v_lat_sept) * (1-np.cos(angle_lat_sept))
    # normalize vector
    x = x / np.linalg.norm(x)


    ''' z-axis '''
    # compute cardiac axis
    pv_mean = np.mean([lpv_mean, rpv_mean], axis=0)
    v_card = pv_mean - mv_center

    # project cardiac axis onto plane perpendicular to x
    v_card_proj = v_card - np.dot(v_card, x) * x

    # rotate cardiac axis around x-axis
    angle_inf_sup = - 49.25 * np.pi / 180

    # Rodrigues' rotation formula
    z = Rodrigues_rot_form(v_card_proj, x, angle_inf_sup)
    #z = v_card_proj * np.cos(angle_inf_sup) + np.cross(x, v_card_proj) * np.sin(angle_inf_sup) \
        #+ x * np.dot(x, v_card_proj) * (1 - np.cos(angle_inf_sup))
    # normalize vector
    z = z / np.linalg.norm(z)


    ''' y-axis '''
    y = np.cross(z, x)


    ''' Calculate rotation matrix to transform points from given COS to new (body) COS '''
    # rotation matrix
    R = np.column_stack((x, y, z))
    # inverse rotation matrix
    R_inv = np.transpose(R)

    # get coordinates of all points
    all_pts = vtk.util.numpy_support.vtk_to_numpy(LA.GetPoints().GetData())



    ########################################
    # Calculate points A - D
    ########################################
    # get the most superior or most inferior aspects on the respective orifice
    ''' point A (most superior aspect of RSPV) '''
    id_a = get_id_on_orifice(all_pts, rspv_ids, R, "max", 2)

    ''' point B (most superior aspect fo the LSPV) '''
    id_b = get_id_on_orifice(all_pts, lspv_ids, R, "max", 2)

    ''' point C (most inferior aspect of the LIPV) '''
    id_c = get_id_on_orifice(all_pts, lipv_ids, R, "min", 2)

    ''' point D (most inferior aspect of the RIPV) '''
    id_d = get_id_on_orifice(all_pts, ripv_ids, R, "min", 2)


    ########################################
    # Calculate points E, F, J, K
    ########################################
    # fit plane in MV orifice using SVD (last of the right vectors is the normal vector)
    mv_pts = all_pts[mv_ids]
    svd_mv = np.linalg.svd(mv_pts - mv_center)
    n_mv = svd_mv[2][2]
    # normalize vector
    n_mv = n_mv / np.linalg.norm(n_mv)

    # make sure the normal vector points upwards (into LA -> closer to pv_mean)
    # calculate distance between MV center and mean of PVs
    d_pv_mv = np.linalg.norm(pv_mean - mv_center)
    # scale fitted normal vector with length between mean of PV and MV
    pt_test = mv_center + d_pv_mv * n_mv
    pt_test_neg = mv_center - d_pv_mv * n_mv
    # check if distance to pv_mean is smaller than with negative scaling
    if np.linalg.norm(pt_test - pv_mean) > np.linalg.norm(pt_test_neg - pv_mean):
        n_mv = - n_mv

    # project mean of inferior/superior vector onto MV plane
    v_mv_sup_proj = z - np.dot(z, n_mv) * n_mv

    # get ids by rotating (and clipping the unwanted part of the orifice) and intersecting the plane with the orifice

    ''' point E (9 o'clock) '''
    angle_mv_9 = - 90 * np.pi / 180
    id_e = get_id_on_valve(mv_poly, mv_center, v_mv_sup_proj, n_mv, angle_mv_9, all_pts)

    ''' point F (1 o'clock) '''
    angle_mv_1 = 30 * np.pi / 180
    id_f = get_id_on_valve(mv_poly, mv_center, v_mv_sup_proj, n_mv, angle_mv_1, all_pts)

    ''' point J (4 o'clock) '''
    angle_mv_4 = 120 * np.pi / 180
    id_j = get_id_on_valve(mv_poly, mv_center, v_mv_sup_proj, n_mv, angle_mv_4, all_pts)

    ''' point K (7 o'clock) '''
    angle_mv_7 = - 150 * np.pi / 180
    id_k = get_id_on_valve(mv_poly, mv_center, v_mv_sup_proj, n_mv, angle_mv_7, all_pts)


    ########################################
    # Calculate points on LAA boundary for left venoatrial junction and anterior wall
    ########################################
    # get LAA
    poly_laa = delete_ids_from_mesh(LA, np.setdiff1d(all_ids, ids_laa))
    # get boundary edges of LAA (path around LAA)
    poly_laa_bound = get_boundary_edges(poly_laa)

    # get closest point on LAA boundary to id_b
    id_laa_closest_id_b = closestid(poly_laa_bound, all_pts[id_b])
    # get closest point on LAA boundary to id_c
    id_laa_closest_id_c = closestid(poly_laa_bound, all_pts[id_c])
    # get closest point on LAA boundary to id_f
    id_laa_closest_id_f = closestid(poly_laa_bound, all_pts[id_f])


    ########################################
    # Compute supplementary paths to assure the boundary paths encircle the PVs correctly (between PVs and PVs to MV)
    ########################################
    # compute closest distance between RSPV and RIPV and get corresponding ids
    id_rspv_closest_to_ripv, id_ripv_closest_to_rspv = closestid_between_datasets(rspv_ids, ripv_ids, all_pts)
    # compute closest distance between LSPV and LIPV and get corresponding ids
    id_lspv_closest_to_lipv, id_lipv_closest_to_lspv = closestid_between_datasets(lspv_ids, lipv_ids, all_pts)

    # compute supplementary paths between RSPV and RIPV (LSPV and LIPV, respectively)y
    supp_path_rspv_ripv_poly, supp_path_rspv_ripv_ids = path(LA, id_rspv_closest_to_ripv, id_ripv_closest_to_rspv)
    supp_path_lspv_lipv_poly, supp_path_lspv_lipv_ids = path(LA, id_lspv_closest_to_lipv, id_lipv_closest_to_lspv)

    # compute mid of both supplementary paths
    id_mid_rpv = supp_path_rspv_ripv_ids[int(0.5 * len(supp_path_rspv_ripv_ids))]
    id_mid_lpv = supp_path_lspv_lipv_ids[int(0.5 * len(supp_path_lspv_lipv_ids))]

    # get closest points on MV
    id_mv_closest_to_rpv = closestid(mv_poly, all_pts[id_mid_rpv])
    id_mv_closest_to_lpv = closestid(mv_poly, all_pts[id_mid_lpv])

    # compute supplementary paths between RSPV and LIPV (RIPV and LSPV, respectively)
    supp_path_rspv_lipv_poly, supp_path_rspv_lipv_ids = path(LA, id_a, id_c)
    supp_path_ripv_lspv_poly, supp_path_ripv_lspv_ids = path(LA, id_d, id_b)
    # delete ids from mesh
    LA_for_pv_mv = delete_ids_from_mesh(LA, np.concatenate((ids_laa, supp_path_rspv_lipv_ids, supp_path_ripv_lspv_ids)))

    # compute supplementary paths between mid of RPVs (LPVs) and MV
    supp_path_rpv_mv_poly, supp_path_rpv_mv_ids = path(LA_for_pv_mv, id_mid_rpv, id_mv_closest_to_rpv)
    supp_path_lpv_mv_poly, supp_path_lpv_mv_ids = path(LA_for_pv_mv, id_mid_lpv, id_mv_closest_to_lpv)
    # compute supplementary path between mid of RPVs and mid of LPVs
    supp_path_rpv_lpv_poly, supp_path_rpv_lpv_ids = path(LA_no_laa, id_mid_rpv, id_mid_lpv)

    # delete ids from mesh to compute superior and inferior paths
    LA_for_sup = delete_ids_from_mesh(LA, np.concatenate((supp_path_rpv_mv_ids, supp_path_lpv_mv_ids, supp_path_rspv_ripv_ids, supp_path_lspv_lipv_ids)))
    LA_for_inf = delete_ids_from_mesh(LA, np.concatenate((supp_path_rspv_ripv_ids, supp_path_lspv_lipv_ids, supp_path_rpv_lpv_ids)))


    ########################################
    # Boundaries for LA: Right Venoatrial Junction
    ########################################
    # compute superior path
    path_rspv_ripv_sup_poly, path_rspv_ripv_sup_ids = path(LA_for_sup, id_a, id_d)
    # compute inferior path (from LIPV to LAA and from LAA to LSPV)
    path_rspv_ripv_inf_poly, path_rspv_ripv_inf_ids = path(LA_for_inf, id_a, id_d)


    ########################################
    # Boundaries for LA: Left Venoatrial Junction
    ########################################
    # compute superior path
    path_lspv_lipv_sup_poly, path_lspv_lipv_sup_ids = path(LA_for_sup, id_b, id_c)
    # compute inferior path
    # from LSPV to LAA
    path_lspv_laa_poly, path_lspv_laa_ids = path(LA_for_inf, id_laa_closest_id_b, id_b)
    # from LAA to LIPV
    path_laa_lipv_poly, path_laa_lipv_ids = path(LA_for_inf, id_c, id_laa_closest_id_c)
    # from superior point on LAA boundary to inferior point on LAA boundary
    # (there is only 1 path because it is computed on the boundary of the LAA)
    path_laa_left_veno_poly, path_laa_left_veno_ids = path(poly_laa_bound, id_laa_closest_id_c, id_laa_closest_id_b)
    # concatenate ids
    path_lspv_lipv_inf_ids = np.concatenate((path_lspv_laa_ids, path_laa_left_veno_ids, path_laa_lipv_ids))


    ########################################
    # Boundaries for LA: Left Posterior Wall
    ########################################
    # compute superior path
    path_lspv_rspv_poly, path_lspv_rspv_ids = path(LA, id_b, id_a)
    # compute inferior path
    path_lipv_ripv_poly, path_lipv_ripv_ids = path(LA, id_d, id_c)

    if args.subdivision == 1:
        # compute mid of RSPV-LSPV and RIPV-LIPV path
        id_mid_spv = path_lspv_rspv_ids[int(0.5 * len(path_lspv_rspv_ids))]
        id_mid_ipv = path_lipv_ripv_ids[int(0.5 * len(path_lipv_ripv_ids))]

        # compute intersection of supplementary path from mid LPV to mid RPV with left / right superior venoatrial junction paths
        id_mid_lpv_veno = np.intersect1d(supp_path_rpv_lpv_ids, path_lspv_lipv_sup_ids)[0]
        id_mid_rpv_veno = np.intersect1d(supp_path_rpv_lpv_ids, path_rspv_ripv_sup_ids)[0]

        # compute subdivisions
        supp_path_post_lat_sept_poly, supp_path_post_lat_sept_ids = path(LA, id_mid_spv, id_mid_ipv)
        supp_path_post_sup_inf_poly, supp_path_post_sup_inf_ids = path(LA, id_mid_lpv_veno, id_mid_rpv_veno)

        # split paths at intersection to get quarters
        id_sub_path_post_mid = np.intersect1d(supp_path_post_lat_sept_ids, supp_path_post_sup_inf_ids)
        # get index of that id in path array
        ind_id_sub_path_post_lat_sept = np.argwhere(supp_path_post_lat_sept_ids == id_sub_path_post_mid)[0][0]
        ind_id_sub_path_post_sup_inf = np.argwhere(supp_path_post_sup_inf_ids == id_sub_path_post_mid)[0][0]

        # split paths at index
        sub_path_post_lat_sept_sup_ids = supp_path_post_lat_sept_ids[ind_id_sub_path_post_lat_sept:]
        sub_path_post_lat_sept_inf_ids = supp_path_post_lat_sept_ids[:ind_id_sub_path_post_lat_sept + 1]
        sub_path_post_sup_inf_lat_ids = supp_path_post_sup_inf_ids[ind_id_sub_path_post_sup_inf:]
        sub_path_post_sup_inf_sept_ids = supp_path_post_sup_inf_ids[:ind_id_sub_path_post_sup_inf + 1]


    ########################################
    # Boundaries for LA: Left Anterior Wall
    ########################################
    # compute septal path
    path_rspv_mv_poly, path_rspv_mv_ids = path(LA, id_a, id_e)

    # compute lateral path
    # from MV (1 o'clock) to closest point on LAA boundary
    path_laa_mv_poly, path_laa_mv_ids = path(LA, id_f, id_laa_closest_id_f)
    # from superior point on LAA boundary to inferior point of LAA boundary
    path_laa_ant_poly, path_laa_ant_ids = path(poly_laa_bound, id_laa_closest_id_f, id_laa_closest_id_b)
    # concatenate ids
    path_lspv_mv_ids = np.concatenate((path_lspv_laa_ids, path_laa_ant_ids, path_laa_mv_ids))

    if args.subdivision == 1:
        # compute supplementary path on MV to get mid point
        supp_path_mv_ant_poly, supp_path_mv_ant_ids = path(mv_poly, id_e, id_f)

        # compute mid points
        id_mid_mv_ant = supp_path_mv_ant_ids[int(0.5 * len(supp_path_mv_ant_ids))]
        id_mid_lspv_mv = path_lspv_mv_ids[int(0.5 * len(path_rspv_mv_ids))]
        id_mid_rspv_mv = path_rspv_mv_ids[int(0.5 * len(path_rspv_mv_ids))]

        # compute subdivisions
        supp_path_ant_lat_sept_poly, supp_path_ant_lat_sept_ids = path(LA, id_mid_spv, id_mid_mv_ant)
        supp_path_ant_sup_inf_poly, supp_path_ant_sup_inf_ids = path(LA, id_mid_lspv_mv, id_mid_rspv_mv)

        # split paths at intersection to get quarters
        id_sub_path_ant_mid = np.intersect1d(supp_path_ant_lat_sept_ids, supp_path_ant_sup_inf_ids)
        # get index of that id in path array
        ind_id_sub_path_ant_lat_sept = np.argwhere(supp_path_ant_lat_sept_ids == id_sub_path_ant_mid)[0][0]
        ind_id_sub_path_ant_sup_inf = np.argwhere(supp_path_ant_sup_inf_ids == id_sub_path_ant_mid)[0][0]

        # split paths at index
        sub_path_ant_lat_sept_sup_ids = supp_path_ant_lat_sept_ids[ind_id_sub_path_ant_lat_sept:]
        sub_path_ant_lat_sept_inf_ids = supp_path_ant_lat_sept_ids[:ind_id_sub_path_ant_lat_sept + 1]
        sub_path_ant_sup_inf_lat_ids = supp_path_ant_sup_inf_ids[ind_id_sub_path_ant_sup_inf:]
        sub_path_ant_sup_inf_sept_ids = supp_path_ant_sup_inf_ids[:ind_id_sub_path_ant_sup_inf + 1]

    ########################################
    # Boundaries for LA: Lateral wall
    ########################################
    path_laa_lat_poly, path_laa_lat_ids = path(poly_laa_bound, id_laa_closest_id_c, id_laa_closest_id_f)


    ########################################
    # Boundaries for LA: Left Inferior Wall
    ########################################
    # compute lateral path
    path_lipv_mv_poly, path_lipv_mv_ids = path(LA, id_c, id_j)
    # compute septal path
    path_ripv_mv_poly, path_ripv_mv_ids = path(LA, id_d, id_k)

    if args.subdivision == 1:
        # compute supplementary path on MV to get mid point
        supp_path_mv_inf_poly, supp_path_mv_inf_ids = path(mv_poly, id_j, id_k)

        # compute mid points
        id_mid_mv_inf = supp_path_mv_inf_ids[int(0.5 * len(supp_path_mv_inf_ids))]
        id_mid_lipv_mv = path_lipv_mv_ids[int(0.5 * len(path_lipv_mv_ids))]
        id_mid_ripv_mv = path_ripv_mv_ids[int(0.5 * len(path_ripv_mv_ids))]

        # compute subdivisions
        supp_path_inf_lat_sept_poly, supp_path_inf_lat_sept_ids = path(LA, id_mid_ipv, id_mid_mv_inf)
        supp_path_inf_sup_inf_poly, supp_path_inf_sup_inf_ids = path(LA, id_mid_lipv_mv, id_mid_ripv_mv)

        # split paths at intersection to get quarters
        id_sub_path_inf_mid = np.intersect1d(supp_path_inf_lat_sept_ids, supp_path_inf_sup_inf_ids)
        # get index of that id in path array
        ind_id_sub_path_inf_lat_sept = np.argwhere(supp_path_inf_lat_sept_ids == id_sub_path_inf_mid)[0][0]
        ind_id_sub_path_inf_sup_inf = np.argwhere(supp_path_inf_sup_inf_ids == id_sub_path_inf_mid)[0][0]

        # split paths at index
        sub_path_inf_lat_sept_sup_ids = supp_path_inf_lat_sept_ids[ind_id_sub_path_inf_lat_sept:]
        sub_path_inf_lat_sept_inf_ids = supp_path_inf_lat_sept_ids[:ind_id_sub_path_inf_lat_sept + 1]
        sub_path_inf_sup_inf_lat_ids = supp_path_inf_sup_inf_ids[ind_id_sub_path_inf_sup_inf:]
        sub_path_inf_sup_inf_sept_ids = supp_path_inf_sup_inf_ids[:ind_id_sub_path_inf_sup_inf + 1]



    ########################################
    # Save all paths
    ########################################
    # add all paths to dict and save output
    paths = dict()
    for var_name, var_value in locals().items():
        if var_name.startswith("path") or var_name.startswith("supp") or var_name.startswith("sub"):
            # save poly data
            if var_name.endswith("poly"):
                # cut off ending and add it to the beginning
                temp_var_name = "_".join(var_name.split("_")[:-1])
                write_vtk(var_value, f"{div_dir}/path_poly_la/{temp_var_name}.vtk")
            # save ids
            elif var_name.endswith("ids"):
                # cut off ending and add it to the beginning
                temp_var_name = "_".join(var_name.split("_")[:-1])
                write_ids(var_value, f"{div_dir}/path_ids_la/{temp_var_name}.txt")
                if not var_name.startswith("supp"):
                    paths[var_name] = var_value

    # add all point ids to dict
    point_ids = dict()
    point_ids["a"] = id_a
    point_ids["b"] = id_b
    point_ids["c"] = id_c
    point_ids["d"] = id_d
    point_ids["e"] = id_e
    point_ids["f"] = id_f
    point_ids["j"] = id_j
    point_ids["k"] = id_k


    return paths, point_ids


def la_division(args, ids_la_bound, ids_points):
    # get paths
    filename = args.mesh.split("/")[-1]
    base_dir = f"{args.mesh}_division"
    clip_veins_dir = f"{base_dir}/stage2_clip_veins"
    extr_rings_dir = f"{base_dir}/stage3_extract_rings"
    div_dir = f"{base_dir}/stage4_division"

    # read mesh (of extract_rings directory)
    LA = read_vtk(f"{extr_rings_dir}/LA.vtk")

    # get all points
    all_pts = vtk.util.numpy_support.vtk_to_numpy(LA.GetPoints().GetData())

    # check if clipped veins exist that can be re-appended
    poly_veins_list = glob.glob(f"{clip_veins_dir}/LA/LA_vein*.vtk")
    veins_poly = dict()
    clippoints_pts = dict()
    veins_pts = dict()
    for path in poly_veins_list:
        # get index / key of seed
        key = path.split("_")[-1].split(".")[0]
        # get centerline
        veins_poly[key] = read_vtk(path)

        # get pathname for corresponding clippoints
        path_clippoint = f"{clip_veins_dir}/LA/LA_clippoint_pt_{key}.txt"
        # get corresponding clippoints
        clippoints_pts[key] = np.loadtxt(path_clippoint, skiprows=2, dtype=float)

        # get points from veins
        veins_pts[key] = vtk.util.numpy_support.vtk_to_numpy(veins_poly[key].GetPoints().GetData())

    # initialize array that keeps track of the added ids by appending the veins
    ids_added_by_veins = 0
    # initialize dict to store ids of veins
    ids_veins = dict()
    # assign ids to polydata from veins to be able to append them to the atrial body later
    for key in veins_poly.keys():
        # check for duplicate points in vein and atrial body
        ids_vein_duplicate_pts = []
        ids_atrium_duplicate_pts = []
        for i, pt in enumerate(veins_pts[key]):
            if any(np.all(np.isin(all_pts, pt), axis=1)):
                # get column (pt / id) in veins and atrium which is a duplicate of the atrial body
                ids_vein_duplicate_pts.append(i)
                ids_atrium_duplicate_pts.append(np.where(np.all(np.isin(all_pts, pt), axis=1))[0][0])

        # get number of points in vein
        n_pts_in_vein = veins_poly[key].GetNumberOfPoints()

        # add consecutive ids to array and if index is equal to a duplicate point, assign id of duplicate
        ids_veins[key] = []
        # initialize start id of ids in veins
        ids_veins_consecutive_id = LA.GetNumberOfPoints() + ids_added_by_veins
        for i in range(n_pts_in_vein):
            # if duplicate add id of atrial body
            if i in ids_vein_duplicate_pts:
                # get index in duplicate pts atrium array
                index_in_atrium_duplicate_pts = np.where(np.array(ids_vein_duplicate_pts) == i)[0][0]
                ids_veins[key].append(ids_atrium_duplicate_pts[index_in_atrium_duplicate_pts])
                continue

            # otherwise add consecutive id
            ids_veins[key].append(ids_veins_consecutive_id)
            # update index
            ids_veins_consecutive_id += 1

        ids_veins[key] = np.array(ids_veins[key])

        # update number of added points / ids by vein
        ids_added_by_veins += n_pts_in_vein - len(ids_vein_duplicate_pts)

        # add array with ids to vein
        geo_filter = vtk.vtkGeometryFilter()
        geo_filter.SetInputData(veins_poly[key])
        geo_filter.Update()

        idFilter = vtk.vtkIdFilter()
        idFilter.SetInputConnection(geo_filter.GetOutputPort())
        idFilter.SetPointIdsArrayName('Ids')
        idFilter.SetCellIdsArrayName('Ids')
        idFilter.Update()

        vein_dsa = dsa.WrapDataObject(idFilter.GetOutput())
        vein_dsa.PointData["Ids"][:] = ids_veins[key]

        # overwrite polydata
        veins_poly[key] = vein_dsa.VTKObject


    if len(list(veins_poly.keys())) > 0:
        # append polydata
        append = vtk.vtkAppendPolyData()
        append.AddInputData(LA)
        for polydata in veins_poly.values():
            append.AddInputData(polydata)
        append.Update()

        # clear duplicate points
        cleaner = vtk.vtkCleanPolyData()
        cleaner.SetInputConnection(append.GetOutputPort())
        cleaner.Update()

        # overwrite LA with re-appended veins
        LA = cleaner.GetOutput()

    # re-calculate all points and ids
    all_pts = vtk.util.numpy_support.vtk_to_numpy(LA.GetPoints().GetData())
    all_ids = vtk.util.numpy_support.vtk_to_numpy(LA.GetPointData().GetArray("Ids"))

    # concatenate boundary ids of each region
    ids_la_bound_region = dict()
    ids_la_bound_region["left_venoatrial_junction"] = np.concatenate((ids_la_bound["path_lspv_lipv_inf_ids"],
                                                                      ids_la_bound["path_lspv_lipv_sup_ids"]))
    ids_la_bound_region["right_venoatrial_junction"] = np.concatenate((ids_la_bound["path_rspv_ripv_inf_ids"],
                                                                       ids_la_bound["path_rspv_ripv_sup_ids"]))
    ids_la_bound_region["posterior_wall"] = np.concatenate((ids_la_bound["path_lspv_lipv_sup_ids"],
                                                            ids_la_bound["path_rspv_ripv_sup_ids"],
                                                            ids_la_bound["path_lspv_rspv_ids"],
                                                            ids_la_bound["path_lipv_ripv_ids"]))
    ids_la_bound_region["anterior_wall"] = np.concatenate((ids_la_bound["path_lspv_rspv_ids"],
                                                           ids_la_bound["path_lspv_mv_ids"],
                                                           ids_la_bound["path_rspv_mv_ids"]))
    ids_la_bound_region["laa"] = ids_la_bound["path_laa_ids"]
    ids_la_bound_region["lateral_wall"] = np.concatenate((ids_la_bound["path_lspv_lipv_inf_ids"],
                                                          ids_la_bound["path_lspv_mv_ids"],
                                                          ids_la_bound["path_lipv_mv_ids"]))
    ids_la_bound_region["inferior_wall"] = np.concatenate((ids_la_bound["path_lipv_ripv_ids"],
                                                           ids_la_bound["path_lipv_mv_ids"],
                                                           ids_la_bound["path_ripv_mv_ids"]))
    ids_la_bound_region["septal_wall"] = np.concatenate((ids_la_bound["path_rspv_ripv_inf_ids"],
                                                         ids_la_bound["path_rspv_mv_ids"],
                                                         ids_la_bound["path_ripv_mv_ids"]))


    # remove boundaries for each region, get largest connected region (negative of target region) and subtract ids
    # from all ids
    ids_la_regions = dict()
    for bound_name, bound_ids in ids_la_bound_region.items():
        # get LA with removed boundary ids
        LA_removed_bound = delete_ids_from_mesh(LA, bound_ids)
        # get connected region
        LA_connected = get_largest_connected_region(LA_removed_bound)
        # get ids of connected region
        con_ids = vtk.util.numpy_support.vtk_to_numpy(LA_connected.GetPointData().GetArray("Ids"))
        # add region ids to dict
        ids_la_regions[bound_name] = np.setdiff1d(all_ids, con_ids)

    # add veins to region
    # calculate mean of right and left venoatrial junction
    pt_mean_right_venoatrial_junction = np.mean(all_pts[ids_la_regions["right_venoatrial_junction"]], axis=0)
    pt_mean_left_venoatrial_junction = np.mean(all_pts[ids_la_regions["left_venoatrial_junction"]], axis=0)
    for key in ids_veins.keys():
        # calculate mean of added vein
        pt_mean_vein = np.mean(all_pts[ids_veins[key]], axis=0)
        # calculate distances to right and left venoatrial junction
        d_right_venoatrial_junction = np.linalg.norm(pt_mean_vein - pt_mean_right_venoatrial_junction)
        d_left_venoatrial_junction = np.linalg.norm(pt_mean_vein - pt_mean_left_venoatrial_junction)
        # assign ids to region depending on the distance to the right / left venoatrial junction
        if d_right_venoatrial_junction < d_left_venoatrial_junction:
            ids_la_regions["right_venoatrial_junction"] = np.concatenate((ids_la_regions["right_venoatrial_junction"],
                                                                          ids_veins[key]))
        else:
            ids_la_regions["left_venoatrial_junction"] = np.concatenate((ids_la_regions["left_venoatrial_junction"],
                                                                         ids_veins[key]))


    # assign boundary ids to one region and delete them from neighboring regions

    ids_la_regions["posterior_wall"] = np.setdiff1d(ids_la_regions["posterior_wall"],
                                                    np.concatenate((ids_la_regions["anterior_wall"],
                                                                    ids_la_regions["inferior_wall"])))
    ids_la_regions["left_venoatrial_junction"] = np.setdiff1d(ids_la_regions["left_venoatrial_junction"],
                                                              np.concatenate((ids_la_regions["posterior_wall"],
                                                                              ids_la_regions["anterior_wall"],
                                                                              ids_la_regions["inferior_wall"],
                                                                              ids_la_regions["septal_wall"],
                                                                              ids_la_regions["lateral_wall"],
                                                                              ids_la_regions["laa"])))
    ids_la_regions["right_venoatrial_junction"] = np.setdiff1d(ids_la_regions["right_venoatrial_junction"],
                                                               np.concatenate((ids_la_regions["posterior_wall"],
                                                                               ids_la_regions["anterior_wall"],
                                                                               ids_la_regions["inferior_wall"],
                                                                               ids_la_regions["septal_wall"],
                                                                               ids_la_regions["lateral_wall"],
                                                                               ids_la_regions["laa"])))
    ids_la_regions["anterior_wall"] = np.setdiff1d(ids_la_regions["anterior_wall"], ids_la_regions["laa"])
    ids_la_regions["inferior_wall"] = np.setdiff1d(ids_la_regions["inferior_wall"],
                                                   np.concatenate((ids_la_regions["lateral_wall"],
                                                                   ids_la_regions["septal_wall"])))
    ids_la_regions["septal_wall"] = np.setdiff1d(ids_la_regions["septal_wall"],
                                                 np.concatenate((ids_la_regions["anterior_wall"],
                                                                 ids_la_regions["posterior_wall"])))
    ids_la_regions["lateral_wall"] = np.setdiff1d(ids_la_regions["lateral_wall"],
                                                  np.concatenate((ids_la_regions["laa"],
                                                                  ids_la_regions["anterior_wall"],
                                                                  ids_la_regions["posterior_wall"])))

    # add subdivisions
    if args.subdivision == 1:
        # concatenate boundary ids for each subdivision (and add all ids that are not part of that region)
        ids_la_bound_subregion = dict()
        # posterior wall
        ids_la_bound_subregion["posterior_wall_left_sup"] = np.concatenate((
            ids_la_bound["sub_path_post_lat_sept_sup_ids"],
            ids_la_bound["sub_path_post_sup_inf_lat_ids"],
            np.setdiff1d(all_ids, ids_la_regions["posterior_wall"])))
        ids_la_bound_subregion["posterior_wall_left_inf"] = np.concatenate((
            ids_la_bound["sub_path_post_lat_sept_inf_ids"],
            ids_la_bound["sub_path_post_sup_inf_lat_ids"],
            np.setdiff1d(all_ids, ids_la_regions["posterior_wall"])))
        ids_la_bound_subregion["posterior_wall_right_sup"] = np.concatenate((
            ids_la_bound["sub_path_post_lat_sept_sup_ids"],
            ids_la_bound["sub_path_post_sup_inf_sept_ids"],
            np.setdiff1d(all_ids, ids_la_regions["posterior_wall"])))
        ids_la_bound_subregion["posterior_wall_right_inf"] = np.concatenate((
            ids_la_bound["sub_path_post_lat_sept_inf_ids"],
            ids_la_bound["sub_path_post_sup_inf_sept_ids"],
            np.setdiff1d(all_ids, ids_la_regions["posterior_wall"])))

        # anterior wall
        ids_la_bound_subregion["anterior_wall_left_sup"] = np.concatenate((
            ids_la_bound["sub_path_ant_lat_sept_sup_ids"],
            ids_la_bound["sub_path_ant_sup_inf_lat_ids"],
            np.setdiff1d(all_ids, ids_la_regions["anterior_wall"])))
        ids_la_bound_subregion["anterior_wall_left_inf"] = np.concatenate((
            ids_la_bound["sub_path_ant_lat_sept_inf_ids"],
            ids_la_bound["sub_path_ant_sup_inf_lat_ids"],
            np.setdiff1d(all_ids, ids_la_regions["anterior_wall"])))
        ids_la_bound_subregion["anterior_wall_right_sup"] = np.concatenate((
            ids_la_bound["sub_path_ant_lat_sept_sup_ids"],
            ids_la_bound["sub_path_ant_sup_inf_sept_ids"],
            np.setdiff1d(all_ids, ids_la_regions["anterior_wall"])))
        ids_la_bound_subregion["anterior_wall_right_inf"] = np.concatenate((
            ids_la_bound["sub_path_ant_lat_sept_inf_ids"],
            ids_la_bound["sub_path_ant_sup_inf_sept_ids"],
            np.setdiff1d(all_ids, ids_la_regions["anterior_wall"])))

        # inferior wall
        ids_la_bound_subregion["inferior_wall_left_sup"] = np.concatenate((
            ids_la_bound["sub_path_inf_lat_sept_sup_ids"],
            ids_la_bound["sub_path_inf_sup_inf_lat_ids"],
            np.setdiff1d(all_ids, ids_la_regions["inferior_wall"])))
        ids_la_bound_subregion["inferior_wall_left_inf"] = np.concatenate((
            ids_la_bound["sub_path_inf_lat_sept_inf_ids"],
            ids_la_bound["sub_path_inf_sup_inf_lat_ids"],
            np.setdiff1d(all_ids, ids_la_regions["inferior_wall"])))
        ids_la_bound_subregion["inferior_wall_right_sup"] = np.concatenate((
            ids_la_bound["sub_path_inf_lat_sept_sup_ids"],
            ids_la_bound["sub_path_inf_sup_inf_sept_ids"],
            np.setdiff1d(all_ids, ids_la_regions["inferior_wall"])))
        ids_la_bound_subregion["inferior_wall_right_inf"] = np.concatenate((
            ids_la_bound["sub_path_inf_lat_sept_inf_ids"],
            ids_la_bound["sub_path_inf_sup_inf_sept_ids"],
            np.setdiff1d(all_ids, ids_la_regions["inferior_wall"])))

        # remove all ids that are not in the region that is to be subdivided together with boundary ids separating the quarter
        # get largest connected region (negative of target region) and subtract these ids from all ids of that region
        # to obtain ids of subdivision
        ids_la_subregions = dict()
        for bound_name, bound_ids in ids_la_bound_subregion.items():
            # get LA with removed boundary ids
            LA_removed_bound = delete_ids_from_mesh(LA, bound_ids)
            # get connected region
            LA_connected = get_largest_connected_region(LA_removed_bound)
            # get ids of connected region
            con_ids = vtk.util.numpy_support.vtk_to_numpy(LA_connected.GetPointData().GetArray("Ids"))
            # get name of the entire region
            bound_name_main = "_".join(bound_name.split("_")[:2])
            # add region ids to dict
            ids_la_subregions[bound_name] = np.setdiff1d(ids_la_regions[bound_name_main], con_ids)

        # assign boundary ids to one region and delete them from neighboring regions
        # posterior wall
        ids_la_subregions["posterior_wall_right_sup"] = np.setdiff1d(ids_la_subregions["posterior_wall_right_sup"],
                                                                     ids_la_subregions["posterior_wall_left_sup"])
        ids_la_subregions["posterior_wall_left_inf"] = np.setdiff1d(ids_la_subregions["posterior_wall_left_inf"],
                                                                    ids_la_subregions["posterior_wall_left_sup"])
        ids_la_subregions["posterior_wall_right_inf"] = np.setdiff1d(ids_la_subregions["posterior_wall_right_inf"],
                                                                     np.concatenate((
                                                                         ids_la_subregions["posterior_wall_right_sup"],
                                                                         ids_la_subregions["posterior_wall_left_inf"],
                                                                         ids_la_subregions["posterior_wall_left_sup"])))

        # anterior wall
        ids_la_subregions["anterior_wall_right_sup"] = np.setdiff1d(ids_la_subregions["anterior_wall_right_sup"],
                                                                    ids_la_subregions["anterior_wall_left_sup"])
        ids_la_subregions["anterior_wall_left_inf"] = np.setdiff1d(ids_la_subregions["anterior_wall_left_inf"],
                                                                   ids_la_subregions["anterior_wall_left_sup"])
        ids_la_subregions["anterior_wall_right_inf"] = np.setdiff1d(ids_la_subregions["anterior_wall_right_inf"],
                                                                    np.concatenate((
                                                                        ids_la_subregions["anterior_wall_right_sup"],
                                                                        ids_la_subregions["anterior_wall_left_inf"],
                                                                        ids_la_subregions["anterior_wall_left_sup"])))

        # inferior wall
        ids_la_subregions["inferior_wall_right_sup"] = np.setdiff1d(ids_la_subregions["inferior_wall_right_sup"],
                                                                    ids_la_subregions["inferior_wall_left_sup"])
        ids_la_subregions["inferior_wall_left_inf"] = np.setdiff1d(ids_la_subregions["inferior_wall_left_inf"],
                                                                   ids_la_subregions["inferior_wall_left_sup"])
        ids_la_subregions["inferior_wall_right_inf"] = np.setdiff1d(ids_la_subregions["inferior_wall_right_inf"],
                                                                    np.concatenate((
                                                                        ids_la_subregions["inferior_wall_right_sup"],
                                                                        ids_la_subregions["inferior_wall_left_inf"],
                                                                        ids_la_subregions["inferior_wall_left_sup"])))



    # initialize array with region tags
    if args.subdivision == 1:
        region_ids = np.zeros((18, len(all_ids)), dtype=float)
        region_ids[0, ids_la_regions["left_venoatrial_junction"]] = 1
        region_ids[1, ids_la_regions["right_venoatrial_junction"]] = 2
        region_ids[2, ids_la_subregions["posterior_wall_left_sup"]] = 3.2
        region_ids[3, ids_la_subregions["posterior_wall_right_sup"]] = 3.4
        region_ids[4, ids_la_subregions["posterior_wall_left_inf"]] = 3.6
        region_ids[5, ids_la_subregions["posterior_wall_right_inf"]] = 3.8
        region_ids[6, ids_la_subregions["anterior_wall_left_sup"]] = 4.2
        region_ids[7, ids_la_subregions["anterior_wall_right_sup"]] = 4.4
        region_ids[8, ids_la_subregions["anterior_wall_left_inf"]] = 4.6
        region_ids[9, ids_la_subregions["anterior_wall_right_inf"]] = 4.8
        region_ids[10, ids_la_regions["laa"]] = 5
        region_ids[11, ids_la_regions["lateral_wall"]] = 6
        region_ids[12, ids_la_subregions["inferior_wall_left_sup"]] = 7.2
        region_ids[13, ids_la_subregions["inferior_wall_right_sup"]] = 7.4
        region_ids[14, ids_la_subregions["inferior_wall_left_inf"]] = 7.6
        region_ids[15, ids_la_subregions["inferior_wall_right_inf"]] = 7.8
        region_ids[16, ids_la_regions["septal_wall"]] = 8
        region_ids[17, :] = region_ids[:-1, :].sum(axis=0)

        # prepare output arrays
        mesh = dsa.WrapDataObject(LA)
        mesh.PointData.append(region_ids[0], "Left Venoatrial Junction")
        mesh.PointData.append(region_ids[1], "Right Venoatrial Junction")
        mesh.PointData.append(region_ids[2], "Posterior Wall (Left Superior)")
        mesh.PointData.append(region_ids[3], "Posterior Wall (Right Superior)")
        mesh.PointData.append(region_ids[4], "Posterior Wall (Left Inferior)")
        mesh.PointData.append(region_ids[5], "Posterior Wall (Right Inferior)")
        mesh.PointData.append(region_ids[6], "Anterior Wall (Left Superior)")
        mesh.PointData.append(region_ids[7], "Anterior Wall (Right Superior)")
        mesh.PointData.append(region_ids[8], "Anterior Wall (Left Inferior)")
        mesh.PointData.append(region_ids[9], "Anterior Wall (Right Inferior)")
        mesh.PointData.append(region_ids[10], "LAA")
        mesh.PointData.append(region_ids[11], "Lateral Wall")
        mesh.PointData.append(region_ids[12], "Inferior Wall (Left Superior)")
        mesh.PointData.append(region_ids[13], "Inferior Wall (Right Superior)")
        mesh.PointData.append(region_ids[14], "Inferior Wall (Left Inferior)")
        mesh.PointData.append(region_ids[15], "Inferior Wall (Right Inferior)")
        mesh.PointData.append(region_ids[16], "Septal Wall")
        mesh.PointData.append(region_ids[17], "Regions")

    else:
        region_ids = np.zeros((9, len(all_ids)), dtype=int)
        region_ids[0, ids_la_regions["left_venoatrial_junction"]] = 1
        region_ids[1, ids_la_regions["right_venoatrial_junction"]] = 2
        region_ids[2, ids_la_regions["posterior_wall"]] = 3
        region_ids[3, ids_la_regions["anterior_wall"]] = 4
        region_ids[4, ids_la_regions["laa"]] = 5
        region_ids[5, ids_la_regions["lateral_wall"]] = 6
        region_ids[6, ids_la_regions["inferior_wall"]] = 7
        region_ids[7, ids_la_regions["septal_wall"]] = 8
        region_ids[8, :] = region_ids[:-1,:].sum(axis=0)

        # prepare output arrays
        mesh = dsa.WrapDataObject(LA)
        mesh.PointData.append(region_ids[0], "Left Venoatrial Junction")
        mesh.PointData.append(region_ids[1], "Right Venoatrial Junction")
        mesh.PointData.append(region_ids[2], "Posterior Wall")
        mesh.PointData.append(region_ids[3], "Anterior Wall")
        mesh.PointData.append(region_ids[4], "LAA")
        mesh.PointData.append(region_ids[5], "Lateral Wall")
        mesh.PointData.append(region_ids[6], "Inferior Wall")
        mesh.PointData.append(region_ids[7], "Septal Wall")
        mesh.PointData.append(region_ids[8], "Regions")

    # remove arrays from mesh
    mesh_final = remove_arrays_from_model(mesh.VTKObject, ["to_delete"])

    # write output
    write_vtk(mesh_final, f"{div_dir}/{filename}_division.vtk")

    # inform user where to find the results
    print(f"\nResults are stored in {div_dir}/{filename}_division.vtk\n")

    return mesh_final


def ra_create_boundaries(args):
    # get paths
    base_dir = f"{args.mesh}_division"
    extr_rings_dir = f"{base_dir}/stage3_extract_rings"
    div_dir = f"{base_dir}/stage4_division"

    # read mesh (of extr_rings directory)
    RA = read_vtk(f"{extr_rings_dir}/RA.vtk")

    # read polydata of TV ring
    tv_poly = read_vtk(f"{extr_rings_dir}/TV.vtk")

    # get all points
    all_pts = vtk.util.numpy_support.vtk_to_numpy(RA.GetPoints().GetData())

    # load centers
    table = np.loadtxt(f"{extr_rings_dir}/rings_centroids.csv", delimiter=",", dtype=str)
    svc_center = np.asarray(table[1:4, int(np.where(table[0] == "SVC")[0])], dtype=float)
    ivc_center = np.asarray(table[1:4, int(np.where(table[0] == "IVC")[0])], dtype=float)
    tv_center = np.asarray(table[1:4, int(np.where(table[0] == "TV")[0])], dtype=float)
    cs_center = np.asarray(table[1:4, int(np.where(table[0] == "CS")[0])], dtype=float)

    # load ring-ids
    svc_ids = np.loadtxt(f"{extr_rings_dir}/ids_SVC.vtx", skiprows=2, dtype=int)
    ivc_ids = np.loadtxt(f"{extr_rings_dir}/ids_IVC.vtx", skiprows=2, dtype=int)
    tv_ids = np.loadtxt(f"{extr_rings_dir}/ids_TV.vtx", skiprows=2, dtype=int)
    cs_ids = np.loadtxt(f"{extr_rings_dir}/ids_CS.vtx", skiprows=2, dtype=int)

    # remove directory if exists
    if os.path.exists(f"{div_dir}/path_ids_ra"):
        shutil.rmtree(f"{div_dir}/path_ids_ra")
    if os.path.exists(f"{div_dir}/path_poly_ra"):
        shutil.rmtree(f"{div_dir}/path_poly_ra")
    # create new directories
    if not os.path.exists(div_dir):
        os.makedirs(div_dir)
    os.makedirs(f"{div_dir}/path_ids_ra")
    os.makedirs(f"{div_dir}/path_poly_ra")

    ########################################
    # Get global (body) orientation axes of the RA
    ########################################
    # x-axis (pointing from lateral to septal)
    # y-axis (pointing from posterior to anterior)
    # z-axis (pointing from inferior to superior)

    ''' z-axis '''
    # compute vector from IVC to SVC
    v_ivc_svc = svc_center - ivc_center

    # construct plane with vector normal to TV orifice and vector from TV to SVC
    # fit plane in TV orifice using SVD (last of the right vectors is the normal vector)
    tv_pts = all_pts[tv_ids]
    svd_tv = np.linalg.svd(tv_pts - tv_center)
    n_tv = svd_tv[2][2]
    # normalize vector
    n_tv = n_tv / np.linalg.norm(n_tv)

    # make sure the normal vector points into the RA (-> closer to SVC)
    # calculate distance between TV center and SVC center
    d_tv_svc = np.linalg.norm(svc_center - tv_center)
    # scale fitted normal vector with half of the length between SVC and TV
    pt_test = tv_center + 0.5 * d_tv_svc * n_tv
    pt_test_neg = tv_center - 0.5 * d_tv_svc * n_tv
    # check if distance to SVC is smaller than with negative scaling
    if np.linalg.norm(pt_test - svc_center) > np.linalg.norm(pt_test_neg - svc_center):
        n_tv = - n_tv

    # compute vector from TV to SVC
    v_tv_svc = svc_center - tv_center
    # get normal vector to normal of TV orifice and vector from TV to SVC
    n_tv_svc = np.cross(n_tv, v_tv_svc)
    # normalize vector
    n_tv_svc = n_tv_svc / np.linalg.norm(n_tv_svc)

    # rotate obtained normal vector by 10° around normal of TV
    anlge_svc_tv = 0 * np.pi / 180
    n_tv_svc_rot = Rodrigues_rot_form(n_tv_svc, n_tv, anlge_svc_tv)

    # project vector from IVC to SVC on this plane to obtain inferior/superior axis
    z = v_ivc_svc - np.dot(v_ivc_svc, n_tv_svc_rot) * n_tv_svc_rot
    z = z / np.linalg.norm(z)


    ''' y-axis '''
    # project TV-SVC vector on plane perpendicular to z
    v_tv_svc_proj = v_tv_svc - np.dot(v_tv_svc, z) * z

    # rotate vector around z-axis
    angle_ant_post = 120 * np.pi / 180
    y = Rodrigues_rot_form(v_tv_svc_proj, z, angle_ant_post)
    y = y / np.linalg.norm(y)


    ''' x-axis '''
    x = np.cross(y, z)


    ''' Calculate rotation matrix to transform points from given COS to new (body) COS '''
    # rotation matrix
    R = np.column_stack((x, y, z))
    # inverse rotation matrix
    R_inv = np.transpose(R)


    ########################################
    # Calculate points L - N
    ########################################
    # get points on SVC orifice
    ''' point L (connection of RAA basis with SVC orifice -> most anterior aspect of SVC)'''
    id_l = get_id_on_orifice(all_pts, svc_ids, R, "max", 1)

    ''' point M (most septal aspect of SVC) '''
    id_m = get_id_on_orifice(all_pts, svc_ids, R, "min", 0)

    ''' point N (most lateral aspect of SVC) '''
    id_n = get_id_on_orifice(all_pts, svc_ids, R, "max", 0)

    ########################################
    # Calculate points Q - T
    ########################################
    # project mean of inferior/superior vector onto TV plane
    v_tv_sup_proj = z - np.dot(z, n_tv) * n_tv

    # get ids by rotating (and clipping the unwanted part of the orifice) and intersecting the plane with the orifice

    ''' point Q (1 o'clock) '''
    angle_tv_1 = 30 * np.pi / 180
    id_q = get_id_on_valve(tv_poly, tv_center, v_tv_sup_proj, n_tv, angle_tv_1, all_pts)

    ''' point R (5 o'clock) '''
    angle_tv_4 = 150 * np.pi / 180
    id_r = get_id_on_valve(tv_poly, tv_center, v_tv_sup_proj, n_tv, angle_tv_4, all_pts)

    ''' point S (8 o'clock) '''
    angle_tv_7 = 240 * np.pi / 180
    id_s = get_id_on_valve(tv_poly, tv_center, v_tv_sup_proj, n_tv, angle_tv_7, all_pts)

    ''' point T (11 o'clock) '''
    angle_tv_10 = 330 * np.pi / 180
    id_t = get_id_on_valve(tv_poly, tv_center, v_tv_sup_proj, n_tv, angle_tv_10, all_pts)


    ########################################
    # Calculate points O - P
    ########################################
    # get points on IVC orifice
    ''' point O (most septal aspect of IVC) '''
    id_o = get_id_on_orifice(all_pts, ivc_ids, R, "min", 0)

    ''' point P (most lateral aspect of IVC) '''
    id_p = get_id_on_orifice(all_pts, ivc_ids, R, "max", 0)


    ########################################
    # Calculate points U and V
    ########################################
    # U In manuscript: at maximum curvature between RAA and RA
    # Here:
    # z-coordinate: at 1/6 between most superior point of the TV and the SVC
    # xy-coordinate: horizontal plane -> from septal to lateral: compute vector between neighboring points
    # -> last point that has a smaller angle than 40° to the normal vector of the TV (transition to lateral wall)

    # get all points in transformed COS
    all_pts_transf = np.dot(all_pts, R)

    ''' incorporate surface curvature '''
    # apply plane cut parallel to TV at mid between TV and SVC and get all points that are anterior of this plane
    RA_anterior_u = plane_cut(RA, plane(np.mean([svc_center, tv_center], axis=0), n=n_tv))
    # get ids of clipped mesh
    ids_anterior_u = vtk.util.numpy_support.vtk_to_numpy(RA_anterior_u.GetPointData().GetArray("Ids"))

    # get coordinates of points in transformed COS
    all_pts_transf = np.dot(all_pts, R)
    # get z-coordinate of SVC center
    z_svc_center_transf = np.dot(svc_center, R)[2]
    # get most superior point on TV
    id_tv_12 = get_id_on_orifice(all_pts, tv_ids, R, "max", 2)
    # get z-coordinate of most superior point on TV
    z_tv_12_transf = all_pts_transf[id_tv_12, 2]
    # get z-coordinate of point U
    z_u_transf = z_tv_12_transf + 1 / 6 * (z_svc_center_transf - z_tv_12_transf)

    # get all points in an z-interval of 2 mm and that are more anterior than the defined plane
    ids_pot_u = ids_anterior_u[np.where((all_pts_transf[ids_anterior_u, 2] <= z_u_transf + 1)
                                        & (all_pts_transf[ids_anterior_u, 2] >= z_u_transf - 1))[0]]

    # get transformed coordinates
    pts_pot_u_transf = all_pts_transf[ids_pot_u, :]

    # get ids of points that are more septal than SVC center
    ids_pot_start_u = ids_pot_u[np.where(pts_pot_u_transf[:, 0] <= np.dot(svc_center, R)[0])[0]]

    # get id of point that is most posterior (starting point)
    id_start_u = ids_pot_start_u[np.argmin(all_pts_transf[ids_pot_start_u, 1])]

    # copy array to proceed with
    ids_pot_u_iter = ids_pot_u.copy()

    # resample points with a distance of min 3 mm
    ids_pot_u_resampled = [id_start_u]

    while True:
        # get current point
        pt_curr_u = all_pts_transf[ids_pot_u_resampled[-1], :]

        # get distance to current point (in xy-plane)
        dist_u = np.linalg.norm(pt_curr_u[:-1] - all_pts_transf[ids_pot_u_iter, :-1], axis=1)

        # delete ids of points that are closer than 3mm
        indices_close_u = np.where(dist_u <= 3)[0]
        ids_pot_u_iter = np.delete(ids_pot_u_iter, indices_close_u)
        dist_u = np.delete(dist_u, indices_close_u)

        # exit loop if list is empty
        if len(ids_pot_u_iter) < 1:
            break

        # now, get id of closest point
        ids_pot_u_resampled.append(ids_pot_u_iter[np.argmin(dist_u)])

    # compute normals of vectors between consecutive points pointing towards the inside of the RA
    normals = []
    # compute angular offset between normals and normal of TV plane
    angular_offset = []
    # project normal of TV plane on xy-plane
    n_tv_proj = n_tv - np.dot(n_tv, z) * z
    n_tv_transf = np.dot(n_tv_proj, R)

    for id, id_next in zip(ids_pot_u_resampled[:-1], ids_pot_u_resampled[1:]):
        # compute vector of neighboring points (in xy-plane)
        v_temp = all_pts_transf[id_next, :-1] - all_pts_transf[id, :-1]

        # rotate vector around -90° of z-axis -> normal vector
        n_temp = Rodrigues_rot_form(np.append(v_temp, 0), np.array([0, 0, 1]), -90 * np.pi / 180)

        # compute angular offset
        angle_temp = np.arccos(np.dot(n_temp[:-1], n_tv_transf[:-1]) /
                               (np.linalg.norm(n_temp[:-1]) * np.linalg.norm(n_tv_transf[:-1]))) * 180 / np.pi

        # append values
        normals.append(n_temp)
        angular_offset.append(angle_temp)

    # fit 5th order polynomial
    indices_angular_offset = np.arange(len(angular_offset))
    fit_coeff_angle = np.polyfit(indices_angular_offset, angular_offset, 5)

    # compute angles for fit
    angular_offset_fit = []
    for i in indices_angular_offset:
        angular_offset_fit.append(get_function_value(fit_coeff_angle, i))

    # get id with minimal angle
    index_u = np.argmin(angular_offset_fit)
    id_u = ids_pot_u_resampled[index_u]

    # get first id after minimum with an angle >= 40°
    index_v = index_u + np.where(np.array(angular_offset_fit[index_u:]) >= 40)[0][0]
    id_v = ids_pot_u_resampled[index_v]

    # point W is calculated later at intersection of superior plane with path connecting most lateral aspect of IVC
    # with 7 o'clock position

    ########################################
    # Calculate point X
    ########################################
    ''' point X (most inferior aspect of CS) '''
    id_x = get_id_on_orifice(all_pts, cs_ids, R, "min", 2)


    ########################################
    # Compute supplementary paths
    ########################################
    ''' connect TV and CS to ensure that the superior border of the Koch triangle encircles the CS '''
    # get ids of closest points to each other
    id_tv_closest_to_cs, id_cs_closest_to_tv = closestid_between_datasets(tv_ids, np.setdiff1d(cs_ids, id_x), all_pts)

    # compute supplementary path
    supp_path_tv_cs_poly, supp_path_tv_cs_ids = path(RA, id_tv_closest_to_cs, id_cs_closest_to_tv)

    # delete ids from mesh to compute superior border of the Koch triangle
    RA_for_sup_koch = delete_ids_from_mesh(RA, supp_path_tv_cs_ids)

    ''' compute path on the superior aspects of the RAA from the RAA apex to the SVC to ensure the paths encircle the
    RAA septally or laterally, respectively '''
    # get ids of most anterior points in each z-interval between RAA and TV
    # get ids of most anterior points in each z-interval
    z_interv_tv_svc = np.arange(np.ceil(z_tv_12_transf), np.ceil(z_svc_center_transf), 1)
    ids_most_ant = []

    for z_lower in z_interv_tv_svc:
        # get ids in z-interval
        temp_id_in_interv = np.argwhere((all_pts_transf[:, 2] > z_lower) & (all_pts_transf[:, 2] < z_lower + 1))

        if len(temp_id_in_interv) > 0:
            # get row with most anterior value, subsequently get id
            temp_row_most_ant = np.argmax(all_pts_transf[temp_id_in_interv, 1])
            temp_id_most_ant = temp_id_in_interv[temp_row_most_ant, 0]
            ids_most_ant.append(temp_id_most_ant)

    # compute supplementary path on lateral wall (to assure a septal path)
    supp_path_raa_lat_poly, supp_path_raa_lat_ids = path(RA, ids_most_ant[-1], id_n)
    # compute supplementary path on septal wall (to assure a lateral path)
    supp_path_raa_sept_poly, supp_path_raa_sept_ids = path(RA, ids_most_ant[-1], id_tv_12)

    # delete ids from mesh to compute septal border (connect the RAA apex with the TV)
    RA_for_raa_sept = delete_ids_from_mesh(RA, np.setdiff1d(supp_path_raa_lat_ids, np.append(id_u, id_l)))
    # delete ids from mesh to compute lateral border (connect the RAA apex with the SVC)
    RA_for_raa_lat = delete_ids_from_mesh(RA, supp_path_raa_sept_ids)


    ########################################
    # Boundaries for RA: Septal Wall
    ########################################
    # compute supero-anterior boundary
    path_svc_tv_poly, path_svc_tv_ids = path(RA, id_m, id_q)
    # compute anterior boundary
    path_tv_cs_sup_poly, path_tv_cs_sup_ids = path(RA_for_sup_koch, id_q, id_x)
    # compute inferior boundary
    path_cs_ivc_poly, path_cs_ivc_ids = path(RA, id_x, id_o)
    # compute posterior boundary
    path_svc_ivc_sept_poly, path_svc_ivc_sept_ids = path(RA, id_m, id_o)


    ########################################
    # Boundaries for RA: Posterior Wall
    ########################################
    # compute lateral path
    path_svc_ivc_lat_poly, path_svc_ivc_lat_ids = path(RA, id_n, id_p)


    ########################################
    # Boundaries for RA: Anterior Wall
    ########################################
    # compute inferior path
    path_max_curv_tv_poly, path_max_curv_tv_ids = path(RA, id_v, id_t)


    ########################################
    # Boundaries for RA: Cavotricuspid Isthmus
    ########################################
    # compute lateral boundary
    path_tv_ivc_poly, path_tv_ivc_ids = path(RA, id_s, id_p)
    # compute superio-septal boundary
    path_tv_cs_inf_poly, path_tv_cs_inf_ids = path(RA, id_r, id_x)


    ########################################
    # Boundaries for RA: RAA
    ########################################
    # get point on 2/3 on the path from IVC to TV (closer to TV)
    id_w = path_tv_ivc_ids[int(2/3 * len(path_tv_ivc_ids))]
    ind_w = np.where(path_tv_ivc_ids == id_w)[0][0]

    # get supplementary points on ridge between lateral vestibule and lateral wall
    # get z-coordinates of points r and s
    z_u_transf = all_pts_transf[id_v, :][2]
    z_w_transf = all_pts_transf[id_w, :][2]

    # potential points: anterior of previously defined plane and lateral of center of TV orifice
    # apply plane cut perpendicular to TV in TV center get all points that are lateral of this plane
    RA_anterior_lateral_w = plane_cut(RA_anterior_u, plane(tv_center, v1=n_tv, v2=z))
    # get ids of clipped mesh
    ids_anterior_lateral_w = vtk.util.numpy_support.vtk_to_numpy(RA_anterior_lateral_w.GetPointData().GetArray("Ids"))

    # define fraction of z-difference between the superior point r and the inferior point s at which the supplementary
    # points are placed
    frac_z_w_help = [4/5, 2/5]
    sup_inf_w_help = ["sup", "inf"]

    # inizialize dict
    dict_help_w = dict()

    for sup_inf, frac in zip(sup_inf_w_help, frac_z_w_help):
        # get z-coordinates of supplementary points on 1/5 and 4/5 in z-direction
        dict_help_w[f"z_w_help_{sup_inf}_transf"] = z_w_transf + frac * (z_u_transf - z_w_transf)

        # get all points in a z-interval of 2 mm
        dict_help_w[f"ids_pot_w_{sup_inf}"] = ids_anterior_lateral_w[
            np.where((all_pts_transf[ids_anterior_lateral_w, 2] <= dict_help_w[f"z_w_help_{sup_inf}_transf"] + 1)
                     & (all_pts_transf[ids_anterior_lateral_w, 2] >= dict_help_w[f"z_w_help_{sup_inf}_transf"] - 1))[0]]

        # get id that is closest to TV orifice (starting point)
        dict_help_w[f"id_start_w_{sup_inf}"] = closestid_between_datasets(dict_help_w[f"ids_pot_w_{sup_inf}"],
                                                                          tv_ids, all_pts)[0]

        # copy array to proceed with
        ids_pot_w_iter = dict_help_w[f"ids_pot_w_{sup_inf}"].copy()

        # resample points with a distance of min 3 mm
        dict_help_w[f"ids_pot_w_{sup_inf}_resampled"] = [dict_help_w[f"id_start_w_{sup_inf}"]]

        while True:
            # get current point
            pt_curr_help_w = all_pts_transf[dict_help_w[f"ids_pot_w_{sup_inf}_resampled"][-1], :]

            # get distance to current point (in xy-plane)
            dist_help_w = np.linalg.norm(pt_curr_help_w[:-1] - all_pts_transf[ids_pot_w_iter, :-1], axis=1)

            # delete ids of points that are closer than 3 mm
            indices_close_help_w = np.where(dist_help_w <= 3)[0]
            ids_pot_w_iter = np.delete(ids_pot_w_iter, indices_close_help_w)
            dist_w = np.delete(dist_help_w, indices_close_help_w)

            # exit loop if list is empty
            if len(ids_pot_w_iter) < 1:
                break

            # now, get id of closest point
            dict_help_w[f"ids_pot_w_{sup_inf}_resampled"].append(ids_pot_w_iter[np.argmin(dist_w)])

        # compute normals of vectors between consecutive points pointing towards the inside of the RA
        dict_help_w[f"normals_{sup_inf}"] = []
        # compute angular offset between normals and normal of TV plane
        dict_help_w[f"angular_offset_{sup_inf}"] = []

        for id_current, id_next in zip(dict_help_w[f"ids_pot_w_{sup_inf}_resampled"][:-1],
                                       dict_help_w[f"ids_pot_w_{sup_inf}_resampled"][1:]):
            # compute vector of neighboring points (in xy-plane)
            v_temp = all_pts_transf[id_next, :-1] - all_pts_transf[id_current, :-1]

            # rotate vector around -90° of z-axis -> normal vector
            n_temp = Rodrigues_rot_form(np.append(v_temp, 0), np.array([0, 0, 1]), -90 * np.pi / 180)

            # compute angular offset
            angle_temp = np.arccos(np.dot(n_temp[:-1], n_tv_transf[:-1]) /
                                   (np.linalg.norm(n_temp[:-1]) * np.linalg.norm(n_tv_transf[:-1]))) * 180 / np.pi

            # append values
            dict_help_w[f"normals_{sup_inf}"].append(n_temp)
            dict_help_w[f"angular_offset_{sup_inf}"].append(angle_temp)

        # fit 5th order polynomial
        indices_angular_offset = np.arange(len(dict_help_w[f"angular_offset_{sup_inf}"]))
        dict_help_w[f"fit_coeff_angle_{sup_inf}"] = np.polyfit(indices_angular_offset,
                                                               dict_help_w[f"angular_offset_{sup_inf}"], 5)

        # compute angles for fit
        dict_help_w[f"angular_offset_fit_{sup_inf}"] = []
        for i in indices_angular_offset:
            dict_help_w[f"angular_offset_fit_{sup_inf}"].append(get_function_value
                                                                (dict_help_w[f"fit_coeff_angle_{sup_inf}"], i))

        # compute change (slope) between consecutive angles
        dict_help_w[f"slope_angular_offset_fit_{sup_inf}"] = np.array(dict_help_w[f"angular_offset_fit_{sup_inf}"])[1:] \
                                                             - np.array(dict_help_w[f"angular_offset_fit_{sup_inf}"])[:-1]

        # get index of first peak in slope
        predecessors = dict_help_w[f"slope_angular_offset_fit_{sup_inf}"][:-2]
        current_slope = dict_help_w[f"slope_angular_offset_fit_{sup_inf}"][1:-1]
        successors = dict_help_w[f"slope_angular_offset_fit_{sup_inf}"][2:]

        # check if slope has a peak
        if len(np.where((current_slope > predecessors) & (current_slope > successors))[0]) > 0:
            # get first index fulfilling the condition (+1 due to index shift and another +1 to get id from slope)
            dict_help_w[f"index_help_w_{sup_inf}"] = np.where((current_slope > predecessors) &
                                                              (current_slope > successors))[0][0] + 2
        else:
            dict_help_w[f"index_help_w_{sup_inf}"] = np.argmax(dict_help_w[f"slope_angular_offset_fit_{sup_inf}"]) + 1

        # get id of max slope
        if sup_inf == "sup":
            id_help_w_sup = dict_help_w[f"ids_pot_w_{sup_inf}_resampled"][dict_help_w[f"index_help_w_{sup_inf}"]]
        else:
            id_help_w_inf = dict_help_w[f"ids_pot_w_{sup_inf}_resampled"][dict_help_w[f"index_help_w_{sup_inf}"]]


    # compute anterior boundary
    path_svc_max_curv_poly1, path_svc_max_curv_ids1 = path(RA_for_raa_sept, id_u, id_l)
    path_svc_max_curv_poly2, path_svc_max_curv_ids2 = path(RA, id_v, id_u)
    path_svc_max_curv_ids = np.concatenate((path_svc_max_curv_ids1, path_svc_max_curv_ids2))
    # compute boundary towards lateral vestibule
    path_max_curv_inf_poly1, path_max_curv_inf_ids1 = path(RA, id_help_w_sup, id_v)
    path_max_curv_inf_poly2, path_max_curv_inf_ids2 = path(RA, id_help_w_inf, id_help_w_sup)
    path_max_curv_inf_poly3, path_max_curv_inf_ids3 = path(RA, id_w, id_help_w_inf)
    path_max_curv_inf_ids = np.concatenate((path_max_curv_inf_ids1, path_max_curv_inf_ids2, path_max_curv_inf_ids3))
    # compute inferior boundary (get segment from lateral CTI path)
    path_raa_inf_ivc_ids = path_tv_ivc_ids[:ind_w + 1]

    if args.subdivision == 1:
        # compute lateral subdivision
        sub_path_raa_sup_inf_poly, sub_path_raa_sup_inf_ids = path(RA_for_raa_lat, id_v, id_n)

    ########################################
    # Boundaries for RA: Lateral Vestibule
    ########################################
    # compute inferior boundary (get segment from lateral CTI path)
    path_inf_tv_ids = path_tv_ivc_ids[ind_w:]


    ########################################
    # Save all paths
    ########################################
    # add all paths to dict and save output
    paths = dict()
    poly_paths = dict()
    for var_name, var_value in locals().items():
        if var_name.startswith("path") or var_name.startswith("supp") or var_name.startswith("sub"):
            # save poly data
            if var_name.endswith("poly"):
                # cut off ending and add it to the beginning
                temp_var_name = "_".join(var_name.split("_")[:-1])
                write_vtk(var_value, f"{div_dir}/path_poly_ra/{temp_var_name}.vtk")
                poly_paths[var_name] = var_value
            # save ids
            elif var_name.endswith("ids"):
                # cut off ending and add it to the beginning
                temp_var_name = "_".join(var_name.split("_")[:-1])
                write_ids(var_value, f"{div_dir}/path_ids_ra/{temp_var_name}.txt")
                if not var_name.startswith("supp"):
                    paths[var_name] = var_value

    # add all point ids to dict
    point_ids = dict()
    point_ids["l"] = id_l
    point_ids["m"] = id_m
    point_ids["n"] = id_n
    point_ids["o"] = id_o
    point_ids["p"] = id_p
    point_ids["q"] = id_q
    point_ids["r"] = id_r
    point_ids["s"] = id_s
    point_ids["t"] = id_t
    point_ids["u"] = id_u
    point_ids["v"] = id_v
    point_ids["w"] = id_w
    point_ids["x"] = id_x


    return paths, point_ids


def ra_division(args, ids_ra_bound, ids_points):
    # get paths
    filename = args.mesh.split("/")[-1]
    base_dir = f"{args.mesh}_division"
    extr_rings_dir = f"{base_dir}/stage3_extract_rings"
    div_dir = f"{base_dir}/stage4_division"

    # read mesh (of extract_rings directory)
    RA = read_vtk(f"{extr_rings_dir}/RA.vtk")

    # concatenate boundary ids of each region
    ids_ra_bound_region = dict()
    ids_ra_bound_region["septal_wall"] = np.concatenate((ids_ra_bound["path_svc_tv_ids"],
                                                         ids_ra_bound["path_tv_cs_sup_ids"],
                                                         ids_ra_bound["path_cs_ivc_ids"],
                                                         ids_ra_bound["path_svc_ivc_sept_ids"]))
    ids_ra_bound_region["posterior_wall"] = np.concatenate((ids_ra_bound["path_svc_ivc_sept_ids"],
                                                            ids_ra_bound["path_svc_ivc_lat_ids"]))
    ids_ra_bound_region["raa"] = np.concatenate((ids_ra_bound["path_svc_max_curv_ids"],
                                                 ids_ra_bound["path_max_curv_inf_ids"],
                                                 ids_ra_bound["path_raa_inf_ivc_ids"],
                                                 ids_ra_bound["path_svc_ivc_lat_ids"]))
    ids_ra_bound_region["vestibule"] = np.concatenate((ids_ra_bound["path_max_curv_tv_ids"],
                                                       ids_ra_bound["path_max_curv_inf_ids"],
                                                       ids_ra_bound["path_inf_tv_ids"]))
    ids_ra_bound_region["anterior_wall"] = np.concatenate((ids_ra_bound["path_svc_tv_ids"],
                                                           ids_ra_bound["path_svc_max_curv_ids"],
                                                           ids_ra_bound["path_max_curv_tv_ids"]))
    ids_ra_bound_region["cavotricuspid_isthmus"] = np.concatenate((ids_ra_bound["path_tv_cs_inf_ids"],
                                                                   ids_ra_bound["path_cs_ivc_ids"],
                                                                   ids_ra_bound["path_tv_ivc_ids"]))
    ids_ra_bound_region["koch_triangle"] = np.concatenate((ids_ra_bound["path_tv_cs_sup_ids"],
                                                           ids_ra_bound["path_tv_cs_inf_ids"]))


    # get all ids of the mesh
    all_ids = vtk.util.numpy_support.vtk_to_numpy(RA.GetPointData().GetArray("Ids"))
    # get all points of the mesh
    all_pts = vtk.util.numpy_support.vtk_to_numpy(RA.GetPoints().GetData())

    # remove boundaries for each region, get largest connected region (negative of target region) and subtract ids
    # from all ids
    ids_ra_regions = dict()
    for bound_name, bound_ids in ids_ra_bound_region.items():
        # get RA with removed boundary ids
        RA_removed_bound = delete_ids_from_mesh(RA, bound_ids)
        # get connected region
        if bound_name == "raa":
            # the RAA region might be larger than the rest of the RA (therefore use closest region to CS)
            RA_connected = get_closest_connected_region(RA_removed_bound, all_pts[ids_points["x"]])
        else:
            # get largest connected region
            RA_connected = get_largest_connected_region(RA_removed_bound)
        # get ids of connected region
        con_ids = vtk.util.numpy_support.vtk_to_numpy(RA_connected.GetPointData().GetArray("Ids"))
        # add region ids to dict
        ids_ra_regions[bound_name] = np.setdiff1d(all_ids, con_ids)


    # assign boundary ids to one region and delete them from neighboring regions
    ids_ra_regions["posterior_wall"] = np.setdiff1d(ids_ra_regions["posterior_wall"],
                                                    np.concatenate((ids_ra_regions["septal_wall"],
                                                                    ids_ra_regions["raa"],
                                                                    ids_ra_regions["cavotricuspid_isthmus"])))
    ids_ra_regions["raa"] = np.setdiff1d(ids_ra_regions["raa"],
                                         np.concatenate((ids_ra_regions["anterior_wall"],
                                                         ids_ra_regions["cavotricuspid_isthmus"],
                                                         ids_ra_regions["vestibule"])))
    ids_ra_regions["vestibule"] = np.setdiff1d(ids_ra_regions["vestibule"], ids_ra_regions["cavotricuspid_isthmus"])
    ids_ra_regions["anterior_wall"] = np.setdiff1d(ids_ra_regions["anterior_wall"],
                                                   np.concatenate((ids_ra_regions["septal_wall"],
                                                                   ids_ra_regions["vestibule"])))
    ids_ra_regions["cavotricuspid_isthmus"] = np.setdiff1d(ids_ra_regions["cavotricuspid_isthmus"],
                                                           ids_ra_regions["septal_wall"])
    ids_ra_regions["koch_triangle"] = np.setdiff1d(ids_ra_regions["koch_triangle"],
                                                   np.concatenate((ids_ra_regions["septal_wall"],
                                                                   ids_ra_regions["cavotricuspid_isthmus"],
                                                                   ids_ra_regions["anterior_wall"])))


    # add subdivision
    if args.subdivision == 1:
        # concatenate boundary ids for each subdivision (and add all ids that are not part of that region)
        ids_ra_bound_subregion = dict()
        # RAA
        ids_ra_bound_subregion["raa_sup"] = np.concatenate((ids_ra_bound["path_svc_max_curv_ids"],
                                                            ids_ra_bound["sub_path_raa_sup_inf_ids"],
                                                            np.setdiff1d(all_ids, ids_ra_regions["raa"])))
        ids_ra_bound_subregion["raa_inf"] = np.concatenate((ids_ra_bound["sub_path_raa_sup_inf_ids"],
                                                            ids_ra_bound["path_max_curv_inf_ids"],
                                                            ids_ra_bound["path_raa_inf_ivc_ids"],
                                                            ids_ra_bound["path_svc_ivc_lat_ids"]))

        # remove all ids that are not in the region that is to be subdivided together with ids separating the subregions
        # get largest connected region (negative of target region) and subtract these ids from all ids of that region
        # to obtain ids of subdivision
        ids_ra_subregions = dict()
        for bound_name, bound_ids in ids_ra_bound_subregion.items():
            # get LA with removed boundary ids
            RA_removed_bound = delete_ids_from_mesh(RA, bound_ids)
            # get connected region
            RA_connected = get_closest_connected_region(RA_removed_bound, all_pts[ids_points["x"]])
            # get ids of connected region
            con_ids = vtk.util.numpy_support.vtk_to_numpy(RA_connected.GetPointData().GetArray("Ids"))
            # get name of the entire region
            if "_" in bound_name:
                bound_name_main = "_".join(bound_name.split("_")[:1])
            else:
                bound_name_main = bound_name
            # add region ids to dict
            ids_ra_subregions[bound_name] = np.setdiff1d(ids_ra_regions[bound_name_main], con_ids)

        # assign boundary ids to one region and delete them from neighboring regions
        ids_ra_subregions["raa_inf"] = np.setdiff1d(ids_ra_subregions["raa_inf"], ids_ra_subregions["raa_sup"])


    # initialize array with region tags
    if args.subdivision == 1:
        region_ids = np.zeros((9, len(all_ids)), dtype=float)
        region_ids[0, ids_ra_regions["septal_wall"]] = 9
        region_ids[1, ids_ra_regions["posterior_wall"]] = 10
        region_ids[2, ids_ra_subregions["raa_sup"]] = 11.3
        region_ids[3, ids_ra_subregions["raa_inf"]] = 11.6
        region_ids[4, ids_ra_regions["vestibule"]] = 12
        region_ids[5, ids_ra_regions["anterior_wall"]] = 13
        region_ids[6, ids_ra_regions["cavotricuspid_isthmus"]] = 14
        region_ids[7, ids_ra_regions["koch_triangle"]] = 15
        region_ids[8, :] = region_ids[:-1, :].sum(axis=0)

        # prepare output arrays
        mesh = dsa.WrapDataObject(RA)
        mesh.PointData.append(region_ids[0], "Septal Wall")
        mesh.PointData.append(region_ids[1], "Posterior Wall")
        mesh.PointData.append(region_ids[2], "RAA (Superior)")
        mesh.PointData.append(region_ids[3], "RAA (Inferior)")
        mesh.PointData.append(region_ids[4], "Lateral Vestibule")
        mesh.PointData.append(region_ids[5], "Anterior Wall")
        mesh.PointData.append(region_ids[6], "Cavotricuspid Isthmus")
        mesh.PointData.append(region_ids[7], "Koch Triangle")
        mesh.PointData.append(region_ids[8], "Regions")

    else:
        region_ids = np.zeros((8, len(all_ids)), dtype=int)
        region_ids[0, ids_ra_regions["septal_wall"]] = 9
        region_ids[1, ids_ra_regions["posterior_wall"]] = 10
        region_ids[2, ids_ra_regions["raa"]] = 11
        region_ids[3, ids_ra_regions["vestibule"]] = 12
        region_ids[4, ids_ra_regions["anterior_wall"]] = 13
        region_ids[5, ids_ra_regions["cavotricuspid_isthmus"]] = 14
        region_ids[6, ids_ra_regions["koch_triangle"]] = 15
        region_ids[7, :] = region_ids[:-1, :].sum(axis=0)

        # prepare output arrays
        mesh = dsa.WrapDataObject(RA)
        mesh.PointData.append(region_ids[0], "Septal Wall")
        mesh.PointData.append(region_ids[1], "Posterior Wall")
        mesh.PointData.append(region_ids[2], "RAA")
        mesh.PointData.append(region_ids[3], "Lateral Vestibule")
        mesh.PointData.append(region_ids[4], "Anterior Wall")
        mesh.PointData.append(region_ids[5], "Cavotricuspid Isthmus")
        mesh.PointData.append(region_ids[6], "Koch Triangle")
        mesh.PointData.append(region_ids[7], "Regions")

    # remove arrays from mesh
    mesh_final = remove_arrays_from_model(mesh.VTKObject, ["to_delete"])

    # write output
    write_vtk(mesh_final, f"{div_dir}/{filename}_division.vtk")

    # inform user where to find the results
    print(f"\nResults are stored in {div_dir}/{filename}_division.vtk\n")

    return mesh_final


def plot(args, atrium, mesh):
    # define arguments for scalar bar
    sargs = dict(vertical=True, position_x=0.95, position_y=0.1, height=0.8, width=0.1, label_font_size=40, n_labels=0, title="")

    # define custom colors
    colors_la = ['#B3B3B3', '#8CA0CB', "#E5C494", "#A6D855", "#FFD92F", '#FC8D62', '#E789C3', '#66C2A6']
    colors_ra = ['#66C2A6', '#E5C494', '#FFD92F', '#FC8D62', '#A6D855', '#E789C3', '#B3B3B3']

    # define dictionary with colors for subdivision
    colors_subdivision_la = {
        3: ["#E5C494", "#B7986A", "#8B6F43", "#60481E"],
        4: ["#A6D855", "#79AB28", "#4D8100", "#215800"],
        7: ["#E789C3", "#BF659E", "#99417A", "#741D58"]
    }
    colors_subdivision_ra = {
        3: ["#FFD92F", "#FFEC97"]
    }

    # define annotations
    if atrium == "LA":
        # get RGBA colors
        colors_rgba = hex_to_rgba(colors_la)

        # define limit of shown value range
        clim = [1, 9]

        # add annotations
        annotations = {
            1.5: "Left venoatrial junction",
            2.5: "Right venoatrial junction",
            5.5: "LAA",
            6.5: "Lateral wall",
            8.5: "Septal wall"
        }

        if args.subdivision == 1:
            # add annotations
            annotations[3.2] = "Posterior wall (left superior)"
            annotations[3.4] = "Posterior wall (right superior)"
            annotations[3.6] = "Posterior wall (left inferior)"
            annotations[3.8] = "Posterior wall (right inferior)"
            annotations[4.2] = "Anterior wall (left superior)"
            annotations[4.4] = "Anterior wall (right superior)"
            annotations[4.6] = "Anterior wall (left inferior)"
            annotations[4.8] = "Anterior wall (right inferior)"
            annotations[7.2] = "Inferior wall (left superior)"
            annotations[7.4] = "Inferior wall (right superior)"
            annotations[7.6] = "Inferior wall (left inferior)"
            annotations[7.8] = "Inferior wall (right inferior)"

            # define shades of main color for subdivision
            for key in list(colors_subdivision_la.keys()):
                # iterate over 4 subdivisions
                for i in np.arange(0, 4):
                    # assign color
                    colors_rgba[(key - 1) * 32 + i * 8: (key - 1) * 32 + (i + 1) * 8, :] = to_rgba(
                        colors_subdivision_la[key][i])


        else:
            # add annotations
            annotations[3.5] = "Posterior wall"
            annotations[4.5] = "Anterior wall"
            annotations[7.5] = "Inferior wall"

    else:
        # get RGBA colors
        colors_rgba = hex_to_rgba(colors_ra)

        # define limit of shown value range
        clim = [9, 16]

        # add annotations
        annotations = {
            9.5: "Septal wall",
            10.5: "Posterior wall",
            12.5: "Lateral vestibule",
            13.5: "Anterior wall",
            14.5: "Cavotricuspid isthmus",
            15.5: "Koch triangle"
        }

        if args.subdivision == 1:
            annotations[11.3] = "RAA (superior)"
            annotations[11.6] = "RAA (inferior) / lateral wall"

            # define shades of main color for subdivision
            for key in list(colors_subdivision_ra.keys()):
                # iterate over 2 subdivisions
                for i in np.arange(0, 2):
                    # assign color
                    colors_rgba[(key - 1) * 36 + i * 18: (key - 1) * 36 + (i + 1) * 18, :] = to_rgba(
                        colors_subdivision_ra[key][i])

        else:
            annotations[11.5] = "RAA"


    # create colormap
    cmap = LinearSegmentedColormap.from_list("Regionalization", colors_rgba)


    p = pv.Plotter(window_size=(2200, 1600), notebook=False)
    pv_mesh = pv.PolyData(mesh)
    pv_mesh.set_active_scalars("Regions")
    p.add_mesh(pv_mesh, show_edges=False, cmap=cmap, clim=clim, scalar_bar_args=sargs,
               interpolate_before_map=False, annotations=annotations)
    p.camera_position = 'xy'
    p.show()








def read_vtk(mesh):
    # read vtk data
    reader = vtk.vtkPolyDataReader()
    reader.SetFileName(mesh)
    reader.Update()

    return reader.GetOutput()

def write_vtk(input, pathname):
    writer = vtk.vtkPolyDataWriter()
    writer.SetFileName(pathname)
    writer.SetInputData(input)
    writer.Write()

def write_ids(array, pathname):
    fname = pathname
    f = open(fname, 'w')

    if array.size == 0:
        f.write('0\n')
        f.write('extra\n')
    elif array.size == 1:
        f.write('1\n')
        f.write('extra\n')
        f.write(f'{array}\n')
    else:
        f.write(f'{len(array)}\n')
        f.write('extra\n')
        for i in range(len(array)):
            f.write(f'{array[i]}\n')


def get_id_on_orifice(all_pts, orifice_ids, R, minmax, axis):
    # get coordinates of all points on the orifice
    orifice_pts = all_pts[orifice_ids]
    # compute coordinates in the transformed COS
    orifice_pts_transf = []
    for pt in orifice_pts:
        #temp = np.dot(R, pt)
        temp = np.dot(pt, R)
        orifice_pts_transf.append(temp)
    # convert list to array
    orifice_pts_transf = np.array(orifice_pts_transf)
    # get row index of the point on the orifice with the greatest/smallest value along specified axis
    if minmax == "max":
        row_target = np.argmax(orifice_pts_transf[:, axis])
    elif minmax == "min":
        row_target = np.argmin(orifice_pts_transf[:, axis])
    # get id of that point
    id_target = orifice_ids[row_target]

    return id_target


def get_id_on_valve(mv_poly, mv_center, v, rot_axis, angle, all_pts):
    # get rotation angle for normal vector of plane for intersection with MV orifice (-90°)
    angle_n_inters = angle - 90 * np.pi / 180

    # get rotation angle for normal vector of plane for cutting the opposite part of the MV orifice (-180°)
    angle_n_cut = angle - 180 * np.pi / 180

    # get normal vector for intersecting plane
    n_inters = Rodrigues_rot_form(v, rot_axis, angle_n_inters)

    # get normal vector for cutting plane
    n_cut = Rodrigues_rot_form(v, rot_axis, angle_n_cut)

    # apply plane cut
    mv_cut_poly = plane_cut(mv_poly, plane(mv_center, n=n_cut))

    # apply intersection
    inters_poly = plane_slice(mv_cut_poly, plane(mv_center, n=n_inters))

    # get ids
    inters_ids = vtk.util.numpy_support.vtk_to_numpy(inters_poly.GetPointData().GetArray("Ids"))

    # select point which is closest to plane
    d = np.inf
    for id in inters_ids:
        # calculate distance
        d_temp = dist_pt_to_plane(all_pts[id], n_inters, mv_center)
        if d_temp < d:
            # update new minimal distance and closest id
            d = d_temp
            id_target = id

    return id_target


def plane(origin, v1=np.zeros(3), v2=np.zeros(3), n=np.zeros(3)):
    if np.all(n == 0):
        # compute crossproduct
        n = np.cross(v1, v2)

    # normalize normal vector
    norm = np.linalg.norm(n, keepdims=True)
    n_norm = n / norm

    # define plane
    plane = vtk.vtkPlane()
    plane.SetNormal(n_norm[0], n_norm[1], n_norm[2])
    plane.SetOrigin(origin[0], origin[1], origin[2])

    return plane


def plane_cut(input, plane):
    meshExtractFilter = vtk.vtkExtractGeometry()
    meshExtractFilter.SetInputData(input)
    meshExtractFilter.SetImplicitFunction(plane)
    meshExtractFilter.Update()

    geo_filter = vtk.vtkGeometryFilter()
    geo_filter.SetInputData(meshExtractFilter.GetOutput())
    geo_filter.Update()

    return geo_filter.GetOutput()


def plane_slice(input, plane):
    meshExtractFilter = vtk.vtkExtractGeometry()
    meshExtractFilter.SetInputData(input)
    meshExtractFilter.SetImplicitFunction(plane)
    meshExtractFilter.SetExtractOnlyBoundaryCells(True)
    meshExtractFilter.SetExtractBoundaryCells(True)
    meshExtractFilter.Update()

    geo_filter = vtk.vtkGeometryFilter()
    geo_filter.SetInputData(meshExtractFilter.GetOutput())
    geo_filter.Update()

    return geo_filter.GetOutput()


def path(input, startnode, endnode):
    # get all ids
    ids = vtk.util.numpy_support.vtk_to_numpy(input.GetPointData().GetArray("Ids"))

    path = vtk.vtkDijkstraGraphGeodesicPath()
    path.SetInputData(input)
    path.SetStartVertex(np.where(ids == startnode)[0][0])
    path.SetEndVertex(np.where(ids == endnode)[0][0])
    path.Update()
    # get id list of path nodes
    path_id_list = np.array([path.GetIdList().GetId(i) for i in range(path.GetIdList().GetNumberOfIds())])
    # get the corresponding ids of the path nodes (id list may not be the same as ids for an incomplete / cut mesh)
    path_ids = ids[path_id_list]

    return path.GetOutput(), path_ids


def closestid(input, point):
    # build locator
    loc = vtk.vtkPointLocator()
    loc.SetDataSet(input)
    loc.BuildLocator()

    # get all ids
    ids = vtk.util.numpy_support.vtk_to_numpy(input.GetPointData().GetArray("Ids"))

    # ids and point id may not be the same for an incomplete / cut mesh
    id_closest = ids[int(loc.FindClosestPoint(point))]

    return id_closest


def closestid_between_datasets(dataset1_ids, dataset2_ids, all_pts):
    # compute coordinates of ids
    dataset1_pts = all_pts[dataset1_ids]
    dataset2_pts = all_pts[dataset2_ids]

    # compute distance matrix between dataset1 and dataset2
    dist_matr = np.linalg.norm(dataset1_pts[:, np.newaxis] - dataset2_pts, axis=-1)
    # get row and column index
    i, j = np.unravel_index(np.argmin(dist_matr), dist_matr.shape)
    # convert row and column index to ids
    id_dataset1 = dataset1_ids[i]
    id_dataset2 = dataset2_ids[j]

    return id_dataset1, id_dataset2


def Rodrigues_rot_form(v, rot_axis, angle):
    # Rodrigues' rotation formula to rotate a vector around another vector (rotation axis) by a defined angle
    v_rot = v * np.cos(angle) + np.cross(rot_axis, v) * np.sin(angle) \
        + rot_axis * np.dot(rot_axis, v) * (1-np.cos(angle))

    return v_rot


def dist_pt_to_plane(pt, n, pt_on_plane):
    # calculate vector from any point on the plane to target point
    v_to_pt = pt - pt_on_plane

    # calculate the projection of the vector onto the plane's normal
    proj = np.dot(v_to_pt, n) / np.linalg.norm(n)

    # calculate distance
    d = np.abs(proj)

    return d


def delete_ids_from_mesh(mesh, ids_to_delete):
    # initialize array that contains the ids that are to be deleted
    to_delete = np.zeros(len(vtk.util.numpy_support.vtk_to_numpy(mesh.GetPointData().GetArray("Ids"))), dtype=int)
    to_delete[ids_to_delete] = 1

    # remove ids of the path defining the region
    mesh = dsa.WrapDataObject(mesh)
    mesh.PointData.append(to_delete, "to_delete")
    thresh = vtk.vtkThreshold()
    thresh.SetInputData(mesh.VTKObject)
    thresh.SetUpperThreshold(0)
    thresh.SetInputArrayToProcess(0, 0, 0, "vtkDataObject::FIELD_ASSOCIATION_POINTS", "to_delete")
    thresh.Update()

    geo_filter = vtk.vtkGeometryFilter()
    geo_filter.SetInputConnection(thresh.GetOutputPort())
    geo_filter.Update()

    return geo_filter.GetOutput()


def get_largest_connected_region(input):
    connect = vtk.vtkConnectivityFilter()
    connect.SetInputData(input)
    connect.SetExtractionModeToLargestRegion()
    connect.Update()
    cln = vtk.vtkCleanPolyData()
    cln.SetInputData(connect.GetOutput())
    cln.Update()

    return cln.GetOutput()


def get_closest_connected_region(input, pt):
    connect = vtk.vtkConnectivityFilter()
    connect.SetInputData(input)
    connect.SetExtractionModeToClosestPointRegion()
    connect.SetClosestPoint(pt)
    connect.Update()
    cln = vtk.vtkCleanPolyData()
    cln.SetInputData(connect.GetOutput())
    cln.Update()

    return cln.GetOutput()


def get_boundary_edges(input):
    boundaryEdges = vtk.vtkFeatureEdges()
    boundaryEdges.SetInputData(input)
    boundaryEdges.BoundaryEdgesOn()
    boundaryEdges.FeatureEdgesOff()
    boundaryEdges.ManifoldEdgesOff()
    boundaryEdges.NonManifoldEdgesOff()
    boundaryEdges.Update()

    return boundaryEdges.GetOutput()


def get_function_value(coeff, x_val):
    # compute value of a function based on coefficients
    y_val = 0
    for i in range(len(coeff)):
        y_val += coeff[i] * x_val ** (len(coeff) - 1 - i)
    return y_val


def get_derivative_coeff(coeff):
    # compute coefficients after derivation
    der_coeff = []
    for i in range(len(coeff)):
        temp_coeff = (len(coeff) - 1 - i) * coeff[i]
        # drop last coefficient
        if temp_coeff == 0:
            continue
        der_coeff.append(temp_coeff)

    return der_coeff


def get_curvature(coeff, x_val):
    # calculate maximum curvature of fitted curve in given interval
    # kappa = abs(y'') / ((1 + (y')^2)^(3/2))

    # get coefficients of first derivative
    coeff_der = get_derivative_coeff(coeff)

    # get coefficients of second derivative
    coeff_der2 = get_derivative_coeff(coeff_der)

    k = abs(get_function_value(coeff_der2, x_val)) / ((1 + get_function_value(coeff_der, x_val)**2)**(3/2))

    return k


def remove_arrays_from_model(mesh, list_array_ids=[], list_array_cells=[]):
    # remove arrays from the model
    for i in range(mesh.GetPointData().GetNumberOfArrays()):
        name = mesh.GetPointData().GetArrayName(i)
        if name in list_array_ids:
            mesh.GetPointData().RemoveArray(name)
    for i in range(mesh.GetCellData().GetNumberOfArrays()):
        name = mesh.GetCellData().GetArrayName(i)
        if name not in list_array_cells:
            mesh.GetCellData().RemoveArray(name)

    return mesh

def hex_to_rgba(colors):
    # get intervals for each color
    interval = int(256 / len(colors))

    # get RGBA colors
    colors_rgba = np.zeros((256, 4), dtype=float)
    for i, col in enumerate(colors):
        colors_rgba[i * interval:, :] = to_rgba(col)

    return colors_rgba


if __name__ == '__main__':
    args = parser()
    run_division(args)